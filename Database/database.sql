/*
MySQL Backup
Source Server Version: 10.2.8
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `businesses`
-- ----------------------------
DROP TABLE IF EXISTS `businesses`;
CREATE TABLE `businesses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(40) NOT NULL,
  `business` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `cardealer`
-- ----------------------------
DROP TABLE IF EXISTS `cardealer`;
CREATE TABLE `cardealer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehiclehash` text NOT NULL,
  `name` text NOT NULL,
  `nameconcess` text NOT NULL,
  `poids` int(10) NOT NULL,
  `price` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `housing`
-- ----------------------------
DROP TABLE IF EXISTS `housing`;
CREATE TABLE `housing` (
  `ID` varchar(50) NOT NULL,
  `Owner` varchar(50) NOT NULL,
  `Inventory` text CHARACTER SET utf8 NOT NULL,
  `Type` int(2) NOT NULL,
  `Position` text NOT NULL,
  `Price` int(10) NOT NULL,
  `Locked` tinyint(1) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Furnitures` text NOT NULL,
  `Money` int(10) NOT NULL,
  `Weapons` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `name` varchar(9999) DEFAULT NULL,
  `owner` varchar(9999) DEFAULT NULL,
  `member` varchar(9999) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `money` int(20) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `bank` int(20) NOT NULL,
  `jailed` tinyint(1) NOT NULL DEFAULT 0,
  `inventory` text DEFAULT NULL,
  `location` text DEFAULT NULL,
  `staffRank` int(1) NOT NULL DEFAULT 0,
  `characters` text DEFAULT NULL,
  `hunger` smallint(3) NOT NULL DEFAULT 100,
  `thirst` smallint(3) NOT NULL DEFAULT 100,
  `health` smallint(3) NOT NULL DEFAULT 100,
  `name` varchar(50) DEFAULT '',
  `password` text DEFAULT NULL,
  `faction` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `inPound` tinyint(1) NOT NULL,
  `pid` varchar(20) NOT NULL,
  `name` varchar(64) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `primaryColor` varchar(20) NOT NULL,
  `secondaryColor` varchar(20) NOT NULL,
  `inventory` text DEFAULT NULL,
  `fuel` double NOT NULL DEFAULT 1,
  `location` text DEFAULT NULL,
  `mods` text DEFAULT NULL,
  `neon` varchar(64) DEFAULT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `parkingid` int(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`,`active`,`inPound`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `whitelist`
-- ----------------------------
DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE `whitelist` (
  `id_socialclub` varchar(70) NOT NULL,
  `password` text NOT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id_socialclub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
