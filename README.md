# Server RP GT-MP

French GT-MP GameMode for RolePlaying (C#)

## Built With

* [GT-MP](https://gt-mp.net/)

## Authors

* [Jonathan Pourrez](https://bitbucket.org/Djoe45/) & [kiene_f](https://bitbucket.org/kiene_f)
