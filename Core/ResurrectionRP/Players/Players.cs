﻿using GrandTheftMultiplayer.Server.Elements;
using System.Collections.Generic;
using System.Data;

namespace ResurrectionRP
{
    class Players
    {
        public static List<Player> players = new List<Player>();

        public static Player add(Client client, DataRow info)
        {
            Player player = new Player(client, info);
            players.Add(player);

            return player;
        }

        public static void delete(Client client)
        {
            Player player = players.Find(x => x.client == client) ?? null;
            if (player != null)
                players.Remove(player);
        }

        public static void delete(string socialClubName)
        {
            Player player = players.Find(x => x.socialClubName == socialClubName) ?? null;
            if (player != null)
                players.Remove(player);
        }

        public static void delete(Player player)
        {
            if (players.Contains(player))
                players.Remove(player);
        }

        public static Player getPlayerBySCN(string socialClubName)
        {
            return players.Find(x => x.socialClubName == socialClubName) ?? null;
        }

        public static Player getPlayerByName(string name)
        {
            return players.Find(x => x.name == name) ?? null;
        }

        public static Player getPlayerByClient(Client player)
        {
            return players.Find(x => x.client == player) ?? null;
        }
    }
}
