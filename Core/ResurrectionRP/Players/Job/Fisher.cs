﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Items;

namespace ResurrectionRP.Player.Job
{
    public class Fisher : Job
    {
        private Player player;
        private Location spawn;
        private Vector3 farmSpawn;
        private string _boatModel;
        private Marker marker;

        public VehicleHandler boat { get; set; }
        public bool notification = false;
        private int _boatColor;

        public Fisher(){ }

        public override void delete()
        {
            player.inventory.delete(Inventory.itemByID(Enums.Items.Canne), 1);
            boat.vehicle.delete();
            Vehicles.vehicles.Remove(boat);
            CharacterCreator.ApplyCharacter(player.client);
        }

        public Fisher(Player player, Location spawn, Vector3 farmSpawn, string boatModel, int boatColor)
        {
            this.player = player;
            this.spawn = spawn;
            this.farmSpawn = farmSpawn;
            _boatModel = boatModel;
            this.canFarm = false;
            _boatColor = boatColor;

            player.client.triggerEvent(Enums.Events.setWaypoint, farmSpawn.X, farmSpawn.Y);
            player.client.triggerEvent(Enums.Events.createMarker, 1, farmSpawn - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(200.0f, 200.0f, 10f), 100, 255, 255, 255);
            CylinderColShape farmColShape = API.createCylinderColShape(farmSpawn - new Vector3(0f, 0f, 1f), 200.0f, 10.0f);
            player.inventory.add(Inventory.itemByID(Enums.Items.Canne), 1);
            this.boat = new VehicleHandler(player.socialClubName, API.vehicleNameToModel(_boatModel), spawn.pos.toVector3(), spawn.rot.toVector3(), primaryColor: _boatColor, secondaryColor: _boatColor, locked: false, engineStatus: true, spawnVeh: true);

            farmColShape.onEntityEnterColShape += (shape, entity) =>
            {
                Client client = API.getPlayerFromHandle(entity);
                if (client == null || client != player.client)
                    return;
                canFarm = true;
                if (notification == false)
                {
                    player.client.sendNotification("Server", "Utiliser votre canne à pêche pour commencer à pêcher.");
                    notification = true;
                }
            };

            farmColShape.onEntityExitColShape += (shape, entity) =>
            {
                Client client = API.getPlayerFromHandle(entity);
                if (client == null || client != player.client)
                    return;
                canFarm = false;
            };

            if (player.character.Gender == 0)
            {
                API.setPlayerClothes(player.client, 11, 188, 0);
                API.setPlayerClothes(player.client, 4, 36, 2);
                API.setPlayerClothes(player.client, 6, 12, 0);
                API.setPlayerClothes(player.client, 8, 2, 0);
            } else
            {
                API.setPlayerClothes(player.client, 11, 190, 0);
                API.setPlayerClothes(player.client, 4, 35, 2);
                API.setPlayerClothes(player.client, 6, 68, 0);
                API.setPlayerClothes(player.client, 8, 2, 0);
            }
        }
    }
}
