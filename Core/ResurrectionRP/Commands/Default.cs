﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Businesses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class Default : Script
    {
        [Command("vehicle")]
        public async Task vehicleAsync(Client sender, VehicleHash vehicle, int color1, int color2)
        {
            VehicleHandler vh = new VehicleHandler(sender.socialClubName, vehicle, sender.position, sender.rotation, color1, secondaryColor: color2, client: sender, engineStatus: true, spawnVeh: true);
            API.setPlayerIntoVehicle(sender, vh.vehicle, -1);
            await vh.save();
        }

        [Command("introduction")]
        public void introduction(Client sender)
        {
            sender.triggerEvent(Events.startIntroduction);
        }

        [Command("add")]
        public void add(Client client)
        {
            Players.getPlayerByClient(client).inventory.add(client, new Item(Enums.Items.Pain, "Pain", "C'est du pain !"), 5);
        }

        [Command("marketOwner")]
        public void marketOwner(Client sender)
        {
            if (Businesses.Businesses.businesses[0].owner == null)
                Businesses.Businesses.businesses[0].owner = sender.socialClubName;
            else
                Businesses.Businesses.businesses[0].owner = null;
        }

        [Command("clothes")]
        public void clothes(Client sender, int a, int b, int c)
        {
            API.shared.setPlayerClothes(sender, a, b, c);
        }

        [Command("fisher")]
        public void fisher(Client sender)
        {
            List<Location> spawns = new List<Location>();
            spawns.Add(Location.fromVector3(new Vector3(-1600.967, 5260.864, -0.4743322), new Vector3(0, 0, 16.31103)));
            List<Vector3> farmSpawns = new List<Vector3>();
            farmSpawns.Add(new Vector3(-772.3212, 6188.515, 0.5212269));
            farmSpawns.Add(new Vector3(-1961.724, 5534.534, 0.2315986));
            farmSpawns.Add(new Vector3(-2108.969, 5343.768, -0.05694753));
            farmSpawns.Add(new Vector3(-2053.907, 4832.303, 1.018692));
            Fisher fisher = new Fisher(new Vector3(-1594.178, 5192.865, 4.310092), spawns, farmSpawns, "Tug", 0);
            API.setEntityPosition(sender, new Vector3(-1594.178, 5192.865, 4.310092));
        }

        [Command("fisher2")]
        public void fisher2(Client sender)
        {
            List<Location> spawns = new List<Location>();
            spawns.Add(Location.fromVector3(new Vector3(1328.338, 4269.186, 30.04752), new Vector3(1.589967, 0.2459596, 176.3435)));
            spawns.Add(Location.fromVector3(new Vector3(1340.969, 4268.389, 30.02968), new Vector3(2.061017, -0.4724084, 172.7197)));
            List<Vector3> farmSpawns = new List<Vector3>();
            farmSpawns.Add(new Vector3(978.7129, 4221.285, 29.87996));
            farmSpawns.Add(new Vector3(402.6023, 4240.635, 30.48716));
            farmSpawns.Add(new Vector3(-23.204, 4295.153, 30.21893));
            Fisher fisher = new Fisher(new Vector3(1309.423, 4361.983, 41.54271), spawns, farmSpawns, "Dinghy2", 111);
            API.setEntityPosition(sender, new Vector3(1309.423, 4361.983, 41.54271));
        }

        [Command("getAllVehicles")]
        public async Task getAllVehiclesAsync(Client sender)
        {
            await Vehicles.loadAllVehiclesActive();
        }

        [Command("tp")]
        public void setposition(Client client, string x, string y, string z)
        {
            string X = x.Replace(",", "").Replace(".", ",");
            string Y = y.Replace(",", "").Replace(".", ",");
            string Z = z.Replace(",", "").Replace(".", ",");
            API.setEntityPosition(client, (new Vector3(float.Parse(X), float.Parse(Y), float.Parse(Z))));
        }


        [Command("coords")]
        public void coords(Client player, string coordName)
        {
            Vector3 playerPosGet = null;
            Vector3 playerRotGet = null;
            if (player.isInVehicle)
            {
                playerPosGet = player.vehicle.position;
                playerRotGet = player.vehicle.rotation;
            } else
            {
                playerPosGet = API.getEntityPosition(player);
                playerRotGet = API.getEntityRotation(player);
            }

            string pPosX = (playerPosGet.X.ToString().Replace(',', '.') + ", ");
            string pPosY = (playerPosGet.Y.ToString().Replace(',', '.') + ", ");
            string pPosZ = (playerPosGet.Z.ToString().Replace(',', '.'));
            
            string pRotX = (playerRotGet.X.ToString().Replace(',', '.') + ", ");
            string pRotY = (playerRotGet.Y.ToString().Replace(',', '.') + ", ");
            string pRotZ = (playerRotGet.Z.ToString().Replace(',', '.'));

            API.sendChatMessageToPlayer(player, "Your position is: ~y~" + playerPosGet, "~w~Your rotation is: ~y~" + playerRotGet);
            StreamWriter coordsFile;
            if (!File.Exists("SavedCoords.txt"))
            {
                coordsFile = new StreamWriter("SavedCoords.txt");
            } else
            {
                coordsFile = File.AppendText("SavedCoords.txt");
            }
            API.sendChatMessageToPlayer(player, "~r~Coordinates have been saved!");
            coordsFile.WriteLine("| " + coordName + " | " + "Saved Coordenates: " + pPosX + pPosY + pPosZ + " Saved Rotation: " + pRotX + pRotY + pRotZ);
            coordsFile.Close();
        }
        /*
        [Command("test")]
        public void test(Client client)
        {
            Players.getPlayerByClient(client).addfactionLicence(FactionType.LSPD, 5);
        }
        
        [Command("test")]
        public void test(Client client)
        {
            new HeartManager(client);
        }
        */
        [Command("AddInEMS")]
        public void test(Client client)
        {
            Players.getPlayerByClient(client).addfactionLicence(FactionType.EMS, 5);
        }

        [Command("discord", GreedyArg = true) ]
        public void discord(Client client, string text)
        {
            Console.WriteLine(text);
            DiscordBot.sendMessageToServer(text);
        }

        [Command("addbarber")]
        public void addbarber(Client client)
        {
            new Barber(new Tuple<PedHash, Location>(PedHash.Eastsa02AFY, Location.fromVector3(client.position, client.rotation)), new Tuple<int, Vector3>(-1844444717, new Vector3(1932.952, 3725.154, 32.9944)));
        }

        [Command("addconcess")]
        public void addconcess(Client client)
        {
            new Barber(new Tuple<PedHash, Location>(PedHash.Eastsa02AFY, Location.fromVector3(client.position, client.rotation)), new Tuple<int, Vector3>(-1844444717, new Vector3(1932.952, 3725.154, 32.9944)));
        }

        [Command("addparking")]
        public void addparking(Client client)
        {
            
        }

        [Command("addclothing")]
        public void addclothing(Client client)
        {
            Location location = new Location();
            location.pos = new Vector3D().fromVector3(client.position);
            location.rot = new Vector3D().fromVector3(client.rotation);
            new ClothingStore(location, MenuManagement.Banner.LowFashion);
        }


    }
}
