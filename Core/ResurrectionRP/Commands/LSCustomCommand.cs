﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Businesses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Commands
{
    class LSCustomCommand : Script
    {
        private int etape = 0;
        private Location location = new Location();
        private Location gerant = new Location();
        private Location spawn = new Location();

        [Command("addcustom")]
        public void addcustom(Client client)
        {
            if (Players.getPlayerByClient(client).staffRank <= AdminRank.Player) return;
            
            if (etape == 0)
            {
                location.pos = new Vector3D().fromVector3(client.position);
                location.rot = new Vector3D().fromVector3(client.rotation);
                etape++;
            }
            else if (etape == 1)
            {
                gerant.pos = new Vector3D().fromVector3(client.position);
                gerant.rot = new Vector3D().fromVector3(client.rotation);
                etape++;
            }
            else if (etape == 2)
            {
                spawn.pos = new Vector3D().fromVector3(client.position);
                spawn.rot = new Vector3D().fromVector3(client.rotation);
                etape = 0;
                new LosSantosCustom(location, gerant, spawn, new Tuple<int, Vector3>(-550347177, new Vector3(-356.0905, -134.7714, 40.01295)));
                API.shared.consoleOutput("Los Santos Custom Added");
            }
            client.sendChatMessage("Etape: " + etape);
        }

        [Command("bodydamage")]
        public void bodydamage(Client client, int damage)
        {
            if (client.isInVehicle)
            {
                client.vehicle.health = damage;
            }
        }
    }
}
