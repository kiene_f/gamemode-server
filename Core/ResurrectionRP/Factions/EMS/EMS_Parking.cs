﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class EMS_Parking : Script
    {
        private MenuItem _outcar;
        private int plateId;

        public EMS_Parking()
        {
            API.onResourceStart += () =>
            {
                List<Tuple<Vector3, Vector3>> parkingpos = new List<Tuple<Vector3, Vector3>>(); // Position & Rotation
                parkingpos.Add(new Tuple<Vector3, Vector3>(new Vector3(328.0179, -1471.565, 29.77327), new Vector3(0, 0, -140.0061)));
                parkingpos.Add(new Tuple<Vector3, Vector3>(new Vector3(324.9218, -1474.695, 29.80927), new Vector3(0, 0, -133.4115)));
                parkingpos.Add(new Tuple<Vector3, Vector3>(new Vector3(321.5747, -1478.082, 29.80975), new Vector3(0, 0, -123.7875)));

                foreach (Tuple<Vector3, Vector3> parking in parkingpos)
                {
                    API.createMarker(1, parking.Item1 - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(3f, 3f, 1f), 100, 255, 255, 255);
                    CylinderColShape _colShape = API.createCylinderColShape(parking.Item1, 3.0f, 3.0f);

                    _colShape.onEntityEnterColShape += (shape, entity) =>
                    {
                        Menu menu = new Menu("", "EMS", "Choix d'un véhicule", 0, 0, Menu.MenuAnchor.MiddleRight, true);
                        menu.BannerColor = new Color(255, 0, 0, 1);
                        List<VehicleHash> _vehicleallowed = new List<VehicleHash>();

                        Client _player = API.getPlayerFromHandle(entity);
                        if (_player == null) return;
                        if (Players.getPlayerByClient(_player).getRangFaction(FactionType.EMS) ==0) { _player.sendNotificationError("~r~[INFO]~s~ Vous n'êtes pas autorisé à accéder à ce parking."); return; }

                        if (_player.vehicle != null)
                        {
                            _outcar = new MenuItem("Ranger le véhicule");
                            _outcar.ExecuteCallback = true;
                            menu.Add(_outcar);
                        } else
                        {

                            int rang = Players.getPlayerByClient(_player).getRangFaction(FactionType.EMS);
                            if (rang >= 1)
                            {
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.Ambulance);
                            }
                            if (rang >= 2)
                            {

                            }
                            if (rang >= 3)
                            {

                            }

                            foreach (VehicleHash hashallow in _vehicleallowed)
                            {
                                MenuItem _vehallow = new MenuItem(API.getVehicleDisplayName(hashallow));
                                _vehallow.ExecuteCallback = true;
                                menu.Add(_vehallow);
                            }

                        }

                        menu.Callback += (_client, _menu, _menuItem, _itemIndex, _forced, _data) =>
                        {
                            if (_menuItem == _outcar)
                            {
                                if (_client.vehicle == null) return;
                                API.deleteEntity(_client.vehicle);
                            }
                            else
                            {
                                VehicleHash hash = _vehicleallowed[_itemIndex];
                                Vehicle veh = API.createVehicle(hash, parking.Item1, parking.Item2, 255, 1);
                                veh.numberPlate = "EMS " + plateId;
                                plateId++;
                                _player.setIntoVehicle(veh, -1);
                            }
                        };

                        MenuManager.OpenMenu(_player, menu);
                    };
                    _colShape.onEntityExitColShape += (shape, entity) =>
                    {
                        Client _player = API.getPlayerFromHandle(entity);
                        if (_player == null) return;
                        _player.triggerEvent(Events.MenuManager_CloseMenu);
                    };
                }
            };
        }
    }
}
