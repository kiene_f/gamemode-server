﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Gta.Blip;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class EMS : Script
    {
        private Menu menu;
        private MenuItem _takeservice;
        private MenuItem _outservice;

        public EMS() => API.onResourceStart += (() => {
            CylinderColShape enter1_colShape = API.createCylinderColShape(new Vector3(294.9826, -1448.745, 29.96662), 1.0f, 1.0f);
            Marker markerenter1 = API.createMarker(1, enter1_colShape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            enter1_colShape.onEntityEnterColShape += ((shape, entity) => {
                if (!API.doesEntityExist(entity) && API.getEntityType(entity) != EntityType.Player) return;
                API.setEntityPosition(entity, new Vector3(248.8442, -1369.885, 24.5378));
                API.setEntityRotation(entity, new Vector3(0, 0, -41.40205));
            });

            CylinderColShape enter2_colShape = API.createCylinderColShape(new Vector3(343.1158, -1398.486, 32.50927), 1.0f, 1.0f);
            Marker markerenter2 = API.createMarker(1, enter2_colShape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            enter2_colShape.onEntityEnterColShape += ((shape, entity) => {
                if (!API.doesEntityExist(entity) && API.getEntityType(entity) != EntityType.Player) return;
                API.setEntityPosition(entity, new Vector3(249.0663, -1369.635, 24.5378));
                API.setEntityRotation(entity, new Vector3(0, 0, -41.40205));
            });

            CylinderColShape out_colShape = API.createCylinderColShape(new Vector3(246.891, -1371.776, 24.5378), 1.0f, 1.0f);
            Marker markerout = API.createMarker(1, out_colShape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            out_colShape.onEntityEnterColShape += ((shape, entity) => {
                if (!API.doesEntityExist(entity) && API.getEntityType(entity) != EntityType.Player) return;
                API.setEntityPosition(entity, new Vector3(307.3535, -1433.821, 29.9084));
            });

            Blip _qgBlip = API.createBlip(enter2_colShape.Center);
            API.setBlipShortRange(_qgBlip, true);
            API.setBlipSprite(_qgBlip, (int)BlipSprite.Hospital);
            API.setBlipColor(_qgBlip, 63);
            API.setBlipName(_qgBlip, "Hôpital");

            CylinderColShape qgwest_colShape = API.createCylinderColShape(new Vector3(268.3205, -1363.541, 24.53779), 1.0f, 1.0f);
            Marker marker = API.createMarker(1, qgwest_colShape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);

            NPC npc = new NPC((PedHash)(-1420211530), "", new Vector3(262.4204, -1359.378, 24.53778), 56, 0);
            npc.setData("Interaction", "Hospital");
            npc.playscenario("WORLD_HUMAN_SMOKING");

            qgwest_colShape.onEntityEnterColShape += (shape, entity) =>
            {
                Client player = API.shared.getPlayerFromHandle(entity);
                menu = new Menu("", "EMS", "Prise de service", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
                menu.BannerColor = new Color(255, 0, 0, 1);
                menu.Callback = ServiceMenuManager;
                menu.BackCloseMenu = true;
                if (EMSManager.hasPlayer(player))
                {
                    _outservice = new MenuItem("Quitter son service");
                    _outservice.ExecuteCallback = true;
                    menu.Add(_outservice);
                }
                else
                {
                    _takeservice = new MenuItem("Prendre son service");
                    _takeservice.ExecuteCallback = true;
                    menu.Add(_takeservice);
                }
                MenuManager.OpenMenu(player, menu);
            };

            qgwest_colShape.onEntityExitColShape += (shape, entity) => API.shared.getPlayerFromHandle(entity).triggerEvent(Events.MenuManager_CloseMenu);
            API.setWorldSyncedData("MedicAvaible", false);
        });

        private void ServiceMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == _takeservice)
            {
                if (!Players.getPlayerByClient(client).isinfaction(FactionType.EMS))
                {
                    client.sendNotificationError("Vous ne faite pas parti du EMS");
                    return;
                }

                EMSManager.addPlayer(client);
                client.triggerEvent(Events.MenuManager_CloseMenu);
                LogManager.Log($"{Players.getPlayerByClient(client).name} viens de prendre sont service au EMS");
                client.sendNotification("","~r~[EMS] ~s~Vous avez pris votre service.");
            }
            else if (menuItem == _outservice)
            {
                EMSManager.delPlayer(client);
                CharacterCreator.ApplyCharacter(client);
                client.triggerEvent(Events.MenuManager_CloseMenu);
                LogManager.Log($"{Players.getPlayerByClient(client).name} à quitter sont service au EMS");
                client.sendNotification("", "~r~[EMS] ~s~Vous avez quitter votre service.");
            }
        }
    }
}
