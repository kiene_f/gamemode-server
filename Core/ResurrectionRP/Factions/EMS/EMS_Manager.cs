﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ResurrectionRP
{
    class EMSManager : Script
    {
        private Client _client;
        private static Timer PayChech;
        private static List<Client> EMS_service = new List<Client>();
        public EMSManager() { }
        public EMSManager(Client client)
        {
            _client = client;
            if (!hasPlayer(_client))
                EMS_service.Add(_client);
            loadCharacter();
            PayChech = API.shared.delay(170000, false, () =>
            {
                int paysomme = 0;
                switch (Players.getPlayerByClient(_client).getRangFaction(FactionType.EMS))
                {
                    default:
                        {
                            break;
                        }
                }
                client.sendNotification("", $"Vous touchez votre salaire de ${paysomme}.");
            });

            API.setWorldSyncedData("MedicAvailable", true);
            API.onPlayerDisconnected += ((__client, reason) => {
                if (__client != _client) return;
                PayChech.Dispose();

                API.setWorldSyncedData("MedicAvailable", EMS_service.Exists(x => x.exists));
            });
        }

        public Client client { get => _client; }

        public static bool hasPlayer(Client player) => EMS_service.Contains(player);
        public static void addPlayer(Client player) => new EMSManager(player);
        public static bool medicAvailable() => API.shared.setWorldSyncedData("MedicAvailable", EMS_service.Exists(x => x.exists));
        public static void delPlayer(Client player)
        {
            EMS_service.Remove(player);
            PayChech.Dispose();
        }
        public void loadCharacter()
        {
            if (Players.getPlayerByClient(_client).character.Gender == 0)
            {
                API.setPlayerClothes(_client, 11, 26, 0); //Survet
                API.setPlayerClothes(_client, 3, 92, 0); // Bras
                API.setPlayerClothes(_client, 8, 15, 0); // Chemise
                API.setPlayerClothes(_client, 6, 0, 0); // Chaussure
                API.setPlayerClothes(_client, 4, 13, 0); // Pantalon
            }
            else if (Players.getPlayerByClient(_client).character.Gender == 1)
            {

            }
        }
    }
}
