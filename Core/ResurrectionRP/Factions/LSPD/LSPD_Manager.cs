﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ResurrectionRP
{
    class LSPDManager : Script
    {
        private Client _client;
        private static Timer PayChech;
        private static List<Client> LSPD_service = new List<Client>();
        public LSPDManager() { }
        public LSPDManager(Client client)
        {
            _client = client;
            if (!hasPlayer(_client))
                LSPD_service.Add(_client);
            loadCharacter();
            PayChech = API.shared.delay(170000, false, () =>
            {
                int paysomme = 0;
                switch (Players.getPlayerByClient(_client).getRangFaction(FactionType.LSPD))
                {
                    default:
                        {
                            break;
                        }
                }
                client.sendNotification("", $"Vous touchez votre salaire de ${paysomme}.");
            });

            API.onPlayerDisconnected += ((__client, reason) => {
                if (__client != _client) return;
                PayChech.Dispose();
            });
        }

        public Client client { get => _client; }

        public static bool hasPlayer(Client player) => LSPD_service.Contains(player);
        public static void addPlayer(Client player) => new LSPDManager(player);
        public static void delPlayer(Client player)
        {
            LSPD_service.Remove(player);
            PayChech.Dispose();
        }
        public void loadCharacter()
        {
            if (Players.getPlayerByClient(_client).character.Gender >= 0)
            {
                API.setPlayerClothes(client, 11, 55, 0); //Tops
                API.setPlayerClothes(client, 3, 0, 0); // Bras
                API.setPlayerClothes(client, 8, 57, 0); // Chemise
                API.setPlayerClothes(client, 6, 25, 0); // Chaussure
                API.setPlayerClothes(client, 4, 31, 0); // Pantalon
                API.setPlayerClothes(client, 8, 58, 0);
            }
            else if (Players.getPlayerByClient(_client).character.Gender == 1)
            {
                API.setPlayerClothes(client, 11, 48, 0); //Tops
                API.setPlayerClothes(client, 3, 0, 0); // Bras
                API.setPlayerClothes(client, 8, 57, 0); // Chemise
                API.setPlayerClothes(client, 6, 25, 0); // Chaussure
                API.setPlayerClothes(client, 4, 31, 0); // Pantalon
                API.setPlayerClothes(client, 8, 58, 0);
            }

            API.setPlayerAccessory(client, 0, 46, 0);

            client.giveWeapon(WeaponHash.Flashlight, 100, true, true);
            client.giveWeapon(WeaponHash.Nightstick, 250, true, true);
            client.giveWeapon(WeaponHash.StunGun, 250, true, true);


            if (Players.getPlayerByClient(_client).getRangFaction(FactionType.LSPD) >= LSPD_Rang.Officier)
            {
                client.giveWeapon(WeaponHash.HeavyPistol, 250, true, true);
                client.giveWeapon(WeaponHash.CarbineRifle, 250, true, true);
                client.giveWeapon(WeaponHash.PumpShotgun, 250, true, true);
            }

            if (Players.getPlayerByClient(_client).getRangFaction(FactionType.LSPD) >= LSPD_Rang.Sergent)
            {

            }

            if (Players.getPlayerByClient(_client).getRangFaction(FactionType.LSPD) >= LSPD_Rang.Lieutenant)
            {
                API.setPlayerClothes(client, 9, 15, 0);
            }

            if (Players.getPlayerByClient(_client).getRangFaction(FactionType.LSPD) >= LSPD_Rang.Capitaine)
            {

            }
        }
    }
}
