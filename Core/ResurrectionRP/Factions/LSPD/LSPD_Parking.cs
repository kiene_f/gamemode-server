﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class LSPD_Parking : Script
    {
        private MenuItem _outcar;
        private int plateId = 0;

        public LSPD_Parking()
        {
            API.onResourceStart += () =>
            {
                List<Tuple<Vector3, Vector3>> parkingpos = new List<Tuple<Vector3, Vector3>>(); // Position & Rotation
                parkingpos.Add(new Tuple<Vector3, Vector3>(new Vector3(452.6684f, -997.1651f, 25.74771f), new Vector3(-0.9143448f, -0.07913253f, 179.9752f)));
                parkingpos.Add(new Tuple<Vector3, Vector3>(new Vector3(447.2542f, -997.6863f, 25.74051f), new Vector3(-0.9035186f, -0.0992979f, -179.4252f)));

                foreach (Tuple<Vector3, Vector3> parking in parkingpos)
                {
                    API.createMarker(1, parking.Item1 - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(3f, 3f, 1f), 100, 255, 255, 255);
                    CylinderColShape _colShape = API.createCylinderColShape(parking.Item1, 3.0f, 3.0f);

                    _colShape.onEntityEnterColShape += (shape, entity) =>
                    {
                        Menu menu = new Menu("", "LSPD", "Choix d'un véhicule", 0, 0, Menu.MenuAnchor.MiddleRight, true);
                        menu.BannerColor = new Color(255, 0, 0, 1);
                        List<VehicleHash> _vehicleallowed = new List<VehicleHash>();

                        Client _player = API.getPlayerFromHandle(entity);
                        if (_player == null) return;
                        if (Players.getPlayerByClient(_player).getRangFaction(FactionType.EMS) == 0) { _player.sendNotificationError("~r~[INFO]~s~ Vous n'êtes pas autorisé à accéder à ce parking."); return; }

                        if (_player.vehicle != null)
                        {
                            _outcar = new MenuItem("Ranger le véhicule");
                            _outcar.ExecuteCallback = true;
                            menu.Add(_outcar);
                        } else
                        {

                            int rang = Players.getPlayerByClient(_player).getRangFaction(FactionType.LSPD);
                            if (rang >= 1)
                            {
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.Police2);
                            }
                            if (rang >= 2)
                            {
                                
                            }
                            if (rang >= 3)
                            {
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.Police3);
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.Sheriff2);
                            }
                            if (rang >= 5)
                            {
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.PoliceT);
                            }
                            if (rang >= 5)
                            {
                                _vehicleallowed.Add((VehicleHash)VehicleHashEmergency.PBus);
                            }


                            foreach (VehicleHash hashallow in _vehicleallowed)
                            {
                                MenuItem _vehallow = new MenuItem(API.getVehicleDisplayName(hashallow));
                                _vehallow.ExecuteCallback = true;
                                menu.Add(_vehallow);
                            }
                        }

                        menu.Callback += (_client, _menu, _menuItem, _itemIndex, _forced, _data) =>
                        {
                            if (_menuItem == _outcar)
                            {
                                if (_client.vehicle == null) return;
                                API.deleteEntity(_client.vehicle);
                            }
                            else
                            {
                                if (_colShape.getAllEntities().Count() > 1) { _player.sendNotificationError("Un véhicule gène la sorti!"); }
                                VehicleHash hash = _vehicleallowed[_itemIndex];
                                Vehicle veh = API.createVehicle(hash, parking.Item1, parking.Item2, 255, 1);
                                veh.numberPlate = "LSPD " + plateId;
                                plateId++;
                                _player.setIntoVehicle(veh, -1);
                            }
                        };

                        MenuManager.OpenMenu(_player, menu);
                    };
                    _colShape.onEntityExitColShape += (shape, entity) =>
                    {
                        Client _player = API.getPlayerFromHandle(entity);
                        if (_player == null) return;
                        _player.triggerEvent(Events.MenuManager_CloseMenu);
                    };
                }

            };
        }
    }
}
