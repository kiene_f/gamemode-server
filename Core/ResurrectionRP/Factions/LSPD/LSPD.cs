﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class LSPD : Script
    {
        private Menu menu;
        private MenuItem _takeservice;
        private MenuItem _outservice;

        public LSPD() => API.onResourceStart += (() => {
            Blip _qgBlip = API.createBlip(new Vector3(436.0187, -981.6024, 30.69861));
            API.setBlipShortRange(_qgBlip, true);
            API.setBlipSprite(_qgBlip, 487);
            API.setBlipColor(_qgBlip, 63);
            API.setBlipName(_qgBlip, "LSPD-Ouest");

            Vector3 _servicepos = new Vector3(456.1482, -988.5895, 30.68961);
            CylinderColShape qgwest_colShape = API.createCylinderColShape(_servicepos, 1.0f, 1.0f);
            Marker marker = API.createMarker(1, _servicepos - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);

            NPC quispe = new NPC(PedHash.FreemodeMale01,"Quispe", new Vector3(440.9815, -978.4512, 30.6896), -175f, 0);
            CylinderColShape quispee_colShape = API.createCylinderColShape(quispe.position, 30.0f, 10.0f);
            
            quispee_colShape.onEntityEnterColShape += (shape, entity) =>
            {
                Client _player = API.getPlayerFromHandle(entity);
                if (_player == null) return;
                API.sendNativeToPlayer(_player, Hash._SET_PED_HAIR_COLOR, quispe.pedHandle.handle, 1, 1);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 11, 55, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 3, 0, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 8, 57, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 6, 25, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 4, 31, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 8, 58, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 9, 1, 0);

                API.sendNativeToPlayer(_player, 0x93376B65A266EB5F, quispe.pedHandle, 0, 46, 0);
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 2, 20, 0);
                
                API.sendNativeToPlayer(_player, Hash.SET_PED_COMPONENT_VARIATION, quispe.pedHandle, 0, 42, 0);
                
            };

            qgwest_colShape.onEntityEnterColShape += (shape, entity) =>
            {
                Client player = API.getPlayerFromHandle(entity);
                menu = new Menu("", "LSPD", "Prise de service", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
                menu.BannerColor = new Color(255, 0, 0, 1);
                menu.Callback = ServiceMenuManager;
                menu.BackCloseMenu = true;
                if (LSPDManager.hasPlayer(player))
                {
                    _outservice = new MenuItem("Quitter son service");
                    _outservice.ExecuteCallback = true;
                    menu.Add(_outservice);
                }
                else
                {
                    _takeservice = new MenuItem("Prendre son service");
                    _takeservice.ExecuteCallback = true;
                    menu.Add(_takeservice);
                }
                MenuManager.OpenMenu(player, menu);
            };
            qgwest_colShape.onEntityExitColShape += (shape, entity) => API.shared.getPlayerFromHandle(entity).triggerEvent(Events.MenuManager_CloseMenu);
        });


        private void ServiceMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == _takeservice)
            {
                if (!Players.getPlayerByClient(client).isinfaction(FactionType.LSPD))
                {
                    client.sendNotificationError("Vous ne faite pas parti du LSPD");
                    return;
                }
                LogManager.Log($"{Players.getPlayerByClient(client).name} viens de prendre sont service au LSPD");
                LSPDManager.addPlayer(client);
                client.triggerEvent(Events.MenuManager_CloseMenu);
                client.sendNotification("[LSPD]", "~r~[LSPD] ~s~Vous avez pris votre service.");
            }
            else if (menuItem == _outservice)
            {
                LSPDManager.delPlayer(client);
                client.triggerEvent(Events.MenuManager_CloseMenu);
                LogManager.Log($"{Players.getPlayerByClient(client).name} à quitter sont service au LSPD");
                client.sendNotification("[LSPD]", "~r~[LSPD] ~s~Vous avez quitter votre service.");
            }
        }
    }
}
