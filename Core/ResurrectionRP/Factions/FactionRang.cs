﻿using Enums;
using Newtonsoft.Json;

namespace ResurrectionRP
{
    class FactionRang
    {
        [JsonProperty("FactionRang")]
        public FactionType FactionType { get; set; }
        public int Rang { get; set; }

        public FactionRang(FactionType factionType, int rang)
        {
            FactionType = factionType;
            Rang = rang;
        }
    }
}
