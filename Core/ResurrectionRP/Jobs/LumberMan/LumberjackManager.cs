﻿using GrandTheftMultiplayer.Shared.Math;
using GrandTheftMultiplayer.Server.API;
using System.Collections.Generic;
using GrandTheftMultiplayer.Server.Elements;
using System.Linq;
using System;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Managers;
using System.IO;
using Newtonsoft.Json;

namespace ResurrectionRP
{
    public class LumberjackManager : Script
    {


        public static List<Tree> CurrentTrees = new List<Tree>()
        {
            new Tree(new Vector3(85.94479, 2981.294, 50.37325), new Vector3(0, 0, -53.25644), TreeHeight.Big),
            new Tree(new Vector3(93.39282, 2986.978, 49.88726), new Vector3(0, 0, -52.79679), TreeHeight.Big),
            new Tree(new Vector3(104.1505, 2985.121, 49.27434), new Vector3(0, 0, -92.74699), TreeHeight.Big),
            new Tree(new Vector3(114.2379, 2987.141, 48.53143), new Vector3(0, 0, -80.8189), TreeHeight.Big),
            new Tree(new Vector3(114.5802, 2994.031, 48.732), new Vector3(0, 0, 84.18786), TreeHeight.Big),
            new Tree(new Vector3(109.657, 3002.529, 48.8354), new Vector3(0, 0, 11.11668), TreeHeight.Big),
            new Tree(new Vector3(100.2403, 3003.445, 49.11267), new Vector3(0, 0, 95.61981), TreeHeight.Big),
            new Tree(new Vector3(90.32558, 3001.313, 49.35418), new Vector3(0, 0, 101.2376), TreeHeight.Big),
            new Tree(new Vector3(83.52383, 2997.931, 49.69406), new Vector3(0, 0, 116.3494), TreeHeight.Big),
            new Tree(new Vector3(73.7542, 2990.195, 50.34149), new Vector3(0, 0, 120.4333), TreeHeight.Big),
            new Tree(new Vector3(65.90811, 2985.766, 51.04293), new Vector3(0, 0, 125.3161), TreeHeight.Big),
            new Tree(new Vector3(57.18439, 2985.041, 51.51113), new Vector3(0, 0, 88.05821), TreeHeight.Big),
            new Tree(new Vector3(61.80918, 2976.513, 52.302), new Vector3(0, 0, -120.803), TreeHeight.Big),
            new Tree(new Vector3(68.7602, 2969.952, 51.84921), new Vector3(0, 0, -134.3373), TreeHeight.Big),
            new Tree(new Vector3(77.0867, 2964.794, 51.58868), new Vector3(0, 0, -121.7121), TreeHeight.Big),
            new Tree(new Vector3(86.56479, 2964.259, 51.22439), new Vector3(0, 0, -96.72345), TreeHeight.Big),
            new Tree(new Vector3(100.737, 2965.355, 50.1329), new Vector3(0, 0, -94.6022), TreeHeight.Big),
            new Tree(new Vector3(110.8632, 2965.905, 49.20765), new Vector3(0, 0, -87.19948), TreeHeight.Big),
            new Tree(new Vector3(120.7554, 2973.716, 48.07241), new Vector3(0, 0, -48.27658), TreeHeight.Big),
            new Tree(new Vector3(121.7482, 2982.436, 48.11076), new Vector3(0, 0, -7.315151), TreeHeight.Big),
            new Tree(new Vector3(130.8159, 2986.75, 47.42355), new Vector3(0, 0, -57.6648), TreeHeight.Big),
            new Tree(new Vector3(128.3528, 2994.473, 47.97938), new Vector3(0, 0, 40.37769), TreeHeight.Big),
            new Tree(new Vector3(122.0335, 3002.851, 48.45219), new Vector3(0, 0, 36.54796), TreeHeight.Big),
            new Tree(new Vector3(101.4864, 3010.805, 48.52105), new Vector3(0, 0, 71.78363), TreeHeight.Big),
            new Tree(new Vector3(71.23142, 2999.26, 49.36197), new Vector3(0, 0, 159.085), TreeHeight.Big),
            new Tree(new Vector3(59.42102, 2998.714, 49.62062), new Vector3(0, 0, 98.18578), TreeHeight.Big)

        };

        public LumberjackManager(){}

        [Command("tree")]
        public void CreateTree(Client player, string type = "")
        {
            Vector3 playerPosGet = API.getEntityPosition(player);
            Vector3 playerRotGet = API.getEntityRotation(player);

            if (CurrentTrees.FirstOrDefault(t => t.Position.DistanceTo(playerPosGet) < 2.5f) != null)
            {
                player.sendNotificationError("Vous avez déjà fait cet arbre");
                return;
            }
            switch (type.ToLower())
            {
                case "big":
                    CurrentTrees.Add(new Tree(playerPosGet, playerRotGet, TreeHeight.Big));
                    break;
                case "medium":
                    CurrentTrees.Add(new Tree(playerPosGet, playerRotGet, TreeHeight.Big));
                    break;
                case "small":
                    CurrentTrees.Add(new Tree(playerPosGet, playerRotGet, TreeHeight.Big));
                    break;
                default:
                    API.sendChatMessageToPlayer(player, "/tree [type]    |   Types: Big, Medium, Small");
                    return;
            }

            string pPosX = (playerPosGet.X.ToString().Replace(',', '.'));
            string pPosY = (playerPosGet.Y.ToString().Replace(',', '.'));
            string pPosZ = (playerPosGet.Z.ToString().Replace(',', '.'));

            string pRotX = (playerRotGet.X.ToString().Replace(',', '.'));
            string pRotY = (playerRotGet.Y.ToString().Replace(',', '.'));
            string pRotZ = (playerRotGet.Z.ToString().Replace(',', '.'));

            File.AppendAllText(type + "trees.txt",
                string.Format("\nnew Tree(new Vector3({0}, {1}, {2}), new Vector3({3}, {4}, {5}), TreeHeight.{3}{4}),", pPosX, pPosY,
                    pPosZ, pRotX, pRotY, pRotZ, type[0].ToString().ToUpper(), type.TrimStart(type[0])));
        }


        [Command("showtreesnearme")]
        public void ShowTreesNearMe(Client player)
        {
            API.triggerClientEvent(player, "displayNearbyTrees", JsonConvert.SerializeObject(CurrentTrees.Select(t => t.Position)));
        }
    }
}
