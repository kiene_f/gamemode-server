﻿using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class Lumberman : Job
    {
        private Vector3 _farmSpawns;

        public Lumberman() { }
        public Lumberman(string name, Vector3 service, List<Location> spawns, Vector3 farmSpawns) : base("Bûcheron", service, spawns)
        {
            _farmSpawns = farmSpawns;
            Blip blip = API.createBlip(service);
            API.setBlipSprite(blip, 120);
            API.setBlipShortRange(blip, true);
            API.setBlipName(blip, name);
        }

        public override bool checkPlayerJob(PlayerHandler player)
        {
            return (player.job.GetType() == typeof(Player.Job.LumberMan));
        }

        public override void startService(PlayerHandler player)
        {
            Location spawn = getSpawnAvailable();
            if (spawn != null)
            {
                player.job = new Player.Job.LumberMan(player, spawn, _farmSpawns);
            }
            else
            {
                API.sendNotificationToPlayer(player.client, "Un camion est déjà sur le point de d'apparition.");
            }
        }

        public override void stopService(PlayerHandler player)
        {
            player.job.delete();
        }
    }
}
