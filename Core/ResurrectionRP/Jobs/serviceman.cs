﻿using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Jobs
{
    class Serviceman : Job
    {
        public override bool checkPlayerJob(PlayerHandler player)
        {
            return (player.job.GetType() == typeof(ResurrectionRP.Player.Job.ServiceMan));
        }

        public override void startService(PlayerHandler player)
        {

        }

        public override void stopService(PlayerHandler player)
        {
            player.job.delete();
        }
    }
}
