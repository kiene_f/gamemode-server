﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Jobs;
using System;
using System.Collections.Generic;

namespace ResurrectionRP
{
    class Fisher : Job
    {
        private string _boatModel;
        private List<Vector3> _farmSpawns;
        private int _boatColor;

        public Fisher(){}

        public Fisher(Vector3 service, List<Location> spawns, List<Vector3> farmSpawns, string boatModel, int boatColor) : base("Pêcheur", service, spawns)
        {
            _boatModel = boatModel;
            _farmSpawns = farmSpawns;
            _boatColor = boatColor;
            Blip blip = API.createBlip(service);
            API.setBlipSprite(blip, 68);
            API.setBlipShortRange(blip, true);
            API.setBlipName(blip, "Pêche");
        }

        public override bool checkPlayerJob(PlayerHandler player)
        {
            return (player.job.GetType() == typeof(ResurrectionRP.Player.Job.Fisher));
        }

        private Vector3 getFarmSpawn()
        {
            return (_farmSpawns[new Random().Next(1, _farmSpawns.Count)]);
        }

        public override void startService(PlayerHandler player)
        {
            Location spawn = getSpawnAvailable();
            if (spawn != null)
            {
                Vector3 farmSpawn = getFarmSpawn();
                player.job = new Player.Job.Fisher(player, spawn, farmSpawn, _boatModel, _boatColor);
            }
            else
            {
                API.sendNotificationToPlayer(player.client, "Un bateau est déjà sur le point de d'apparition.");
            }
        }

        public override void stopService(PlayerHandler player)
        {
            player.job.delete();
        }
    }
}
