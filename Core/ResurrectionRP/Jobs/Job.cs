﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ResurrectionRP.Jobs
{
    abstract class Job : Script
    {
        protected string name;
        protected Dictionary<CylinderColShape, bool> spawnsColShape;
        protected List<Location> spawns;
        protected List<Item> items;
        protected CylinderColShape service;

        public Job()
        {
        }

        public Job(string name, Vector3 service, List<Location> spawns)
        {
            this.name = name;
            this.spawns = spawns;
            this.service = API.createCylinderColShape(service, 1.0f, 0.5f);
            this.service.setData("ID_Service", false);
            GrandTheftMultiplayer.Server.Elements.Marker marker = API.createMarker(1, service - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);

            spawnsColShape = new Dictionary<CylinderColShape, bool>();
            foreach (Location spawn in this.spawns)
            {
                CylinderColShape cSpawn = API.createCylinderColShape(spawn.pos.toVector3(), 5.0f, 0.5f);
                cSpawn.onEntityEnterColShape += (shape, entity) => {spawnsColShape[(CylinderColShape)shape] = false;};
                cSpawn.onEntityExitColShape += (shape, entity) => {spawnsColShape[(CylinderColShape)shape] = true;};
                spawnsColShape.Add(cSpawn, true);
            }

            this.service.onEntityEnterColShape += (shape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                PlayerHandler player = Players.getPlayerByClient(client);
                if (player.job != null && checkPlayerJob(player) == false)
                    API.sendNotificationToPlayer(client, "Vous êtes déjà en service sur un autre métier.");
                else
                {
                    player.client.setData("job", this);
                    openServiceMenu(player);
                }
            };
            this.service.onEntityExitColShape += (shape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                client.triggerEvent(Events.MenuManager_CloseMenu);
            };
        }

        abstract public bool checkPlayerJob(PlayerHandler player);
        abstract public void startService(PlayerHandler player);
        abstract public void stopService(PlayerHandler player);

        private void serviceMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            PlayerHandler player = Players.getPlayerByClient(client);
            Job job = player.client.getData("job");
            if (menuItem.Id == "ID_start_service")
            {
                job.startService(player);
                client.triggerEvent(Events.MenuManager_CloseMenu);
            } else if (menuItem.Id == "ID_end_service")
            {
                job.stopService(player);
                player.job = null;
                client.triggerEvent(Events.MenuManager_CloseMenu);
            }
        }

        private void openServiceMenu(PlayerHandler player)
        {
            Menu menu = new Menu("ID_Service", name, "", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
            menu.Callback = serviceMenuManager;

            if (player.job == null)
            {
                MenuItem service = new MenuItem("Prendre son service", "", "ID_start_service");
                service.ExecuteCallback = true;
                menu.Add(service);
            } else
            {
                MenuItem service = new MenuItem("Prendre sa fin de service", "", "ID_end_service");
                service.ExecuteCallback = true;
                menu.Add(service);
            }

            MenuManager.OpenMenu(player.client, menu);
        }

        public Location getSpawnAvailable()
        {
            int i = 0;
            foreach (KeyValuePair<CylinderColShape, bool> spawn in spawnsColShape)
            {
                if (spawn.Value == true)
                    return (spawns[i]);
                i++;
            }
            return (null);
        }
    }
}
