﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class LogManager : Script
    {
        public static void Log(string txt)
        {
            DiscordBot.sendMessageToLog(txt);
        }
    }
}
