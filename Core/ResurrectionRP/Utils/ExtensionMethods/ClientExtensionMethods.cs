﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public static class ClientExtensionMethods
    {

        public static void sendSubTitle(this Client player, string msg, int time) =>
            player.triggerEvent(Events.display_subtitle, msg, time);

        public static void setPlayerMovementClipset(this Client player, string animSet, float speed) =>
            player.triggerEvent(Events.setPlayerMovementClipset, animSet, speed);

        public static void sendNotification(this Client client, string message) =>
            API.shared.sendNotificationToPlayer(client, message);

        public static void sendNotification(this Client client, string message, int color) =>
            API.shared.sendNotificationToPlayer(client, message, color);

        public static void sendNotificationError(this Client client, string message) =>
            API.shared.sendNotificationToPlayer(client, "[ERREUR] " + message, 27);

        public static void sendNotificationSuccess(this Client client, string message) =>
            API.shared.sendNotificationToPlayer(client, message, 25);

        public static void sendNotificationToPlayer(this Client api, Client client, string message, int color)
        {
            API.shared.sendNativeToPlayer(client, 0x92F0DA1E27DB96DC, color);
            API.shared.sendNotificationToPlayer(client, message);
        }

        public static void createCamera(this Client client, Vector3 position, Vector3 rotation, NetHandle entity) => client.triggerEvent("createCamera", position, rotation, entity);
        public static void destroyCamera(this Client client) => client.triggerEvent("destroyCamera");
    }
}
