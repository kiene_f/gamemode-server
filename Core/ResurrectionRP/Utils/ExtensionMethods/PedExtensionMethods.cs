﻿using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public static class PedExtensionMethods
    {
        public static void EnterVehicle(this Ped ped, Vehicle veh, int seat, int duration = 20, int flag = 1)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < duration; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_ENTER_VEHICLE, ped, veh, -1, seat, 1.0, flag, 0);
                }
            });
        }

        public static void WarpIntoVehicle(this Ped ped, Vehicle veh, int seat)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < 3; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_WARP_PED_INTO_VEHICLE, ped, veh, seat);
                }
            });
        }

        public static void WarpIntoVehicleForPlayer(this Ped ped, Client player, Vehicle veh, int seat)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < 3; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToPlayer(player, Hash.TASK_WARP_PED_INTO_VEHICLE, ped, veh, seat);
                }
            });
        }

        public static void ExitVehicle(this Ped ped)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < 6; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_LEAVE_ANY_VEHICLE, ped, 0, 0);
                }
            });
        }

        ///  <summary>
        ///  
        ///  </summary>
        ///  <param name="ped"></param>
        ///  <param name="x"></param>
        ///  <param name="y"></param>
        ///  <param name="z"></param>
        /// <param name="speed"></param>
        /// <param name="duration"></param>
        public static void MoveTo(this Ped ped, float x, float y, float z, int speed, int duration = 20)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < duration; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_GO_TO_COORD_ANY_MEANS, ped.handle, x, y, z, 1.0, 0, 0, 786603, 0xbf800000);
                }
            });
        }

        public static void LookAtEntity(this Ped ped, NetHandle entity, int duration = 20)
        {
            Task.Run(() =>
            {
                for (int i = 0; i < duration; i++)
                {
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_TURN_PED_TO_FACE_ENTITY, ped.handle, entity, 2000);
                }
            });
        }

        public static void TurnToPosition(this Ped ped, Vector3 position, int duration = 20)
        {
            Task.Run(() => {
                for (int i = 0; i < duration; i++)
                {
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_TURN_PED_TO_FACE_COORD, ped.handle, position, 2000);
                }
            });
        }

        public static void DriveTo(this Ped ped, Vehicle veh, float x, float y, float z, float speed)
        {
            Task.Run(async () =>
            {
                while (veh.position.DistanceTo(new Vector3(x, y, z)) > 5)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(ped, false);
                    API.shared.sendNativeToAllPlayers(Hash.TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE, ped.handle, veh, x, y, z, speed, 1 | 2 | 16 | 32 | 128 | 256 | 262144, 10f);
                }
            });
        }

        public static void ClimbOver(this Ped ped, Vehicle veh)
        {
            Task.Run(() =>
            {
                API.shared.sendNativeToAllPlayers(Hash.TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT, ped, veh);
            });
        }

        public static async Task<bool> IsInVehicle(this Ped ped, Client sender)
        {
            return await Task.Run(() => API.shared.fetchNativeFromPlayer<bool>(sender, Hash.IS_PED_IN_ANY_VEHICLE, ped, true));
        }

        public static void ClearTasks(this Ped ped)
        {
            API.shared.sendNativeToAllPlayers(Hash.CLEAR_PED_TASKS_IMMEDIATELY, ped);
        }
    }

    public static class PlayerExtensions
    {
        public static void EnterVehicle(this Client player, Vehicle veh, int seat)
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < 4; i++)
                {
                    await Task.Delay(1000);
                    API.shared.setEntityPositionFrozen(player, false);
                    API.shared.sendNativeToPlayersInDimension(player.dimension, Hash.TASK_ENTER_VEHICLE, player, veh, -1, seat, 1.0, 1, 0);
                }
            });
        }

        public static void FadeOutIn(this Client player, int time, int wait)
        {
            Task.Run(async () =>
            {
                API.shared.sendNativeToPlayer(player, Hash.DO_SCREEN_FADE_OUT, time);
                await Task.Delay(wait);
                API.shared.sendNativeToPlayer(player, Hash.DO_SCREEN_FADE_IN, time);
            });
        }

        public static void FadeOut(this Client player, int time)
        {
            API.shared.sendNativeToPlayer(player, Hash.DO_SCREEN_FADE_OUT, time);
        }

        public static void FadeIn(this Client player, int time)
        {
            API.shared.sendNativeToPlayer(player, Hash.DO_SCREEN_FADE_IN, time);
        }

        public static double GetSpeed(this Client player)
        {
            Vector3 velocity = API.shared.getPlayerVelocity(player);
            return Math.Sqrt(
                velocity.X * velocity.X +
                velocity.Y * velocity.Y +
                velocity.Z * velocity.Z
            );
        }

        public static int Ped(this Client player)
        {
            int id = API.shared.fetchNativeFromPlayer<int>(player, Hash.GET_PLAYER_PED, player);
            Console.WriteLine(id);
            return id;
        }

        public static void ClearChat(this Client player)
        {
            for (int i = 0; i < 10; i++)
            {
                API.shared.sendChatMessageToPlayer(player, " ");
            }
        }

        public static void ToggleCursorLock(this Client player, bool tog)
        {
            API.shared.triggerClientEvent(player, "ToggleCursorLock", tog);
        }
/*
        public static Player GetClosestPlayer(this Client player, int distance = 20)
        {
            Player closestPlayer = null;
            Vector3 pPos = player.position;
            foreach (Player p in Player.PlayerData.Values)
            {
                if (p.Client == player) continue;
                if (closestPlayer == null || pPos.DistanceTo(p.Client.position) < pPos.DistanceTo(closestPlayer.Client.position))
                {
                    if (pPos.DistanceTo(p.Client.position) > distance) continue;
                    closestPlayer = p;
                }
            }
            return closestPlayer;
        }
        */
        public static bool IsClimbing(this Client player)
        {
            return API.shared.fetchNativeFromPlayer<bool>(player, Hash.IS_PED_CLIMBING, player);
        }

        public static bool IsVaulting(this Client player)
        {
            return API.shared.fetchNativeFromPlayer<bool>(player, Hash.IS_PED_VAULTING, player);
        }
    }
}