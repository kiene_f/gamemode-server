﻿using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enums;

namespace ResurrectionRP
{
    public static class APIExtensionMethods
    {
        /*
        public static void SendMessageToAllFactionMemebers(this ServerAPI api, Faction faction, string message)
        {
            foreach (Player p in Player.PlayerData.Values)
            {
                if (p.Faction == faction)
                {
                    api.sendChatMessageToPlayer(p.Client, message);
                }
            }
        }
        */
        public static void sendNotificationToPlayer(this ServerAPI api, Client client, string message, int color)
        {
            api.sendNativeToPlayer(client, 0x92F0DA1E27DB96DC, color);
            api.sendNotificationToPlayer(client, message);
        }

        public static void setEntityProofs(this ServerAPI api, NetHandle entity, bool bulletProof, bool fireProof, bool explosionProof, bool collisionProof, bool meleeProof)
        {
            api.sendNativeToAllPlayers(Hash.SET_ENTITY_PROOFS, entity, bulletProof, fireProof, explosionProof, collisionProof, meleeProof, true, 1, true);
        }

        public static void SetPedIntoVehicle(this ServerAPI api, Ped ped, GrandTheftMultiplayer.Server.Elements.Vehicle veh, int seat)
        {
            api.sendNativeToAllPlayers(Hash.TASK_WARP_PED_INTO_VEHICLE, ped, veh, seat);
        }
        /*
        public static NetHandle CreatePedInVehicle(this ServerAPI api, GrandTheftMultiplayer.Server.Elements.Vehicle veh, int type, PedHash model, int seat)
        {
            return api.fetchNativeFromPlayer<NetHandle>(Player.IDs.Where(p => p != null).First(), Hash.CREATE_PED_INSIDE_VEHICLE, veh, type, model, seat, false, true);
        }
        */

        public static bool IsInFirstPerson(this ServerAPI api, Client player)
        {
            if (api.fetchNativeFromPlayer<int>(player, (ulong)0xEE778F8C7E1142E2, API.shared.fetchNativeFromPlayer<uint>(player, (ulong)0x19CAFA3C87F7C2FF, player)) == 4) return true;
            else return false;
        }

        private static readonly Random random = new Random();
        public static int RandomNumber(this ServerAPI api, int max)
        {
            return random.Next(max);
        }

        public static int RandomNumber(this ServerAPI api, int min, int max)
        {
            return random.Next(min, max);
        }
    }
}