﻿using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;

namespace ResurrectionRP
{
    public partial class Vector3D
    {
        [JsonProperty("x")]
        public float x { get; set; }
        [JsonProperty("y")]
        public float y { get; set; }
        [JsonProperty("z")]
        public float z { get; set; }

        public Vector3D() { }

        public Vector3 toVector3()
        {
            return (new Vector3(this.x, this.y, this.z));
        }

        public Vector3D fromVector3(Vector3 vector)
        {
            Vector3D vec3d = new Vector3D();
            vec3d.x = vector.X;
            vec3d.y = vector.Y;
            vec3d.z = vector.Z;
            return vec3d;
        }
    }

    public partial class Location
    {
        [JsonProperty("position")]
        public Vector3D pos { get; set; }
        [JsonProperty("rotation")]
        public Vector3D rot { get; set; }

        public Location()
        {
            this.pos = new Vector3D();
            this.rot = new Vector3D();
        }
    }

    public partial class Location
    {
        public static Location fromJson(string json) => JsonConvert.DeserializeObject<Location>(json, Converter.settings);
        public static Location fromVector3(Vector3 pos, Vector3 rot)
        {
            Location location = new Location();
            location.pos.x = pos.X;
            location.pos.y = pos.Y;
            location.pos.z = pos.Z;
            location.rot.x = rot.X;
            location.rot.y = rot.Y;
            location.rot.z = rot.Z;
            return location;
        }
    }

    public static class Serialize
    {
        public static string toJson(this Location self) => JsonConvert.SerializeObject(self, Converter.settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}