﻿using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class MoneyAPI
    {
        public MoneyAPI()
        {

        }
       
        public static async Task<bool> takeBankMoney(Client sender, int amount)
        {
            if ((Players.getPlayerByClient(sender).bank >= amount) && (amount > 0))
            {
                int newbank = Players.getPlayerByClient(sender).bank - amount;
                int newmoney = Players.getPlayerByClient(sender).money + amount;
                Players.getPlayerByClient(sender).bank = newbank;
                Players.getPlayerByClient(sender).money = newmoney;
                await Players.getPlayerByClient(sender).updatePlayerInfo();
                return true;
            }
            return false;
        }

        public static bool giveBankMoney(Client sender, int amount)
        {
            if ((Players.getPlayerByClient(sender).money >= amount) && (amount > 0))
            {
                int newmoney = Players.getPlayerByClient(sender).money - amount;
                int bankmoney = Players.getPlayerByClient(sender).bank + amount;
                Players.getPlayerByClient(sender).money = newmoney;
                Players.getPlayerByClient(sender).bank = bankmoney;
                return true;
            }
            return false;
        }


        /*
       public static async Task giveMoney(Client sender, int amount)
       {
           if (amount > 0)
           {
               Players.getPlayerByClient(sender).bank += amount;
               await DBPlayers.updatePlayerMoney(sender);
           }
       }

       public static async Task giveMoneyToOther(Client sender, Client receiver, int amount)
       {
           if (amount > 0)
           {
               int playermoney = Players.getPlayerByClient(sender).bank;
               if ((playermoney - amount) >= 0)
               {
                   int receivermoney = Players.getPlayerByClient(receiver).bank;
                   int sendermoney = Players.getPlayerByClient(sender).bank;
                   Players.getPlayerByClient(receiver).bank += amount;
                   Players.getPlayerByClient(sender).bank -= amount;

                   await DBPlayers.updatePlayerMoney(sender);
               }
           }
       }*/
    }
}
