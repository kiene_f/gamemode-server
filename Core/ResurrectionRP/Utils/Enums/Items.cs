﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    public class Items
    {
        public const int
            Fromage = 1,
            Pain = 2,
            Canne = 3,
            Sardine = 4,
            GrappeRaisin = 5,
            Raisin = 6,
            MineraiCuivre = 7,
            Cuivre = 8,
            Sable = 9,
            Bouteille = 10,
            Vin = 11,
            RodinDeBois = 12,
            PlancheBois = 13,
            Jerrycan = 14;
    }
}
