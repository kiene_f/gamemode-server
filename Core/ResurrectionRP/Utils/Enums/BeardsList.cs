﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Utils.Enums
{
    class BeardsHairsList : Script
    {
        public static List<Tuple<int, string>> beards;
        public static List<Tuple<int, string>> hairs;
        public BeardsHairsList()
        {
            API.onResourceStart += (() =>
            {
                beards = new List<Tuple<int, string>>();
                beards.Add(new Tuple<int, string>(-1, "Rasé précis"));
                beards.Add(new Tuple<int, string>(0, "Light Stubble"));
                beards.Add(new Tuple<int, string>(1, "Balbo"));
                beards.Add(new Tuple<int, string>(2, "Circle Beard"));
                beards.Add(new Tuple<int, string>(3, "Goatee"));
                beards.Add(new Tuple<int, string>(4, "Chin"));
                beards.Add(new Tuple<int, string>(5, "Chin Fuzz"));
                beards.Add(new Tuple<int, string>(6, "Pencil Chin Strap"));
                beards.Add(new Tuple<int, string>(7, "Scruffy"));
                beards.Add(new Tuple<int, string>(8, "Musketeer"));
                beards.Add(new Tuple<int, string>(9, "Mustache"));
                beards.Add(new Tuple<int, string>(10, "Trimmed Beard"));
                beards.Add(new Tuple<int, string>(11, "Stubble"));
                beards.Add(new Tuple<int, string>(12, "Thin Circle Beard"));
                beards.Add(new Tuple<int, string>(13, "Horseshoe"));
                beards.Add(new Tuple<int, string>(14, "Pencil and 'Chops"));
                beards.Add(new Tuple<int, string>(15, "Chin Strap Beard"));
                beards.Add(new Tuple<int, string>(16, "Balbo and Sideburns"));
                beards.Add(new Tuple<int, string>(17, "Mutton Chops"));
                beards.Add(new Tuple<int, string>(18, "Scruffy Beard"));
                beards.Add(new Tuple<int, string>(19, "Curly"));
                beards.Add(new Tuple<int, string>(20, "Curly & Deep Stranger"));
                beards.Add(new Tuple<int, string>(21, "Handlebar"));
                beards.Add(new Tuple<int, string>(22, "Faustic"));
                beards.Add(new Tuple<int, string>(23, "Otto & Patch"));
                beards.Add(new Tuple<int, string>(24, "Otto and Full Stranger"));
                beards.Add(new Tuple<int, string>(25, "Light Franz"));
                beards.Add(new Tuple<int, string>(26, "The Hampstead"));
                beards.Add(new Tuple<int, string>(27, "The Ambrose"));
                beards.Add(new Tuple<int, string>(28, "Lincoln Curtain"));

                hairs = new List<Tuple<int, string>>();
                hairs.Add(new Tuple<int, string>(0, "Close Shave"));
                hairs.Add(new Tuple<int, string>(1, "Buzzcut"));
                hairs.Add(new Tuple<int, string>(2, "Faux Hawk"));
                hairs.Add(new Tuple<int, string>(3, "Hipster"));
                hairs.Add(new Tuple<int, string>(4, "Side Parting"));
                hairs.Add(new Tuple<int, string>(5, "Shorter Cut"));
                hairs.Add(new Tuple<int, string>(6, "Biker"));
                hairs.Add(new Tuple<int, string>(7, "Ponytail"));
                hairs.Add(new Tuple<int, string>(8, "Cornrows"));
                hairs.Add(new Tuple<int, string>(9, "Slicked"));
                hairs.Add(new Tuple<int, string>(10, "Short Brushed"));
                hairs.Add(new Tuple<int, string>(11, "Spikey"));
                hairs.Add(new Tuple<int, string>(12, "Caesar"));
                hairs.Add(new Tuple<int, string>(13, "Chopped"));
                hairs.Add(new Tuple<int, string>(14, "Dreads"));
                hairs.Add(new Tuple<int, string>(15, "Long Hair"));
                hairs.Add(new Tuple<int, string>(16, "Shaggy Curls"));
                hairs.Add(new Tuple<int, string>(17, "Surfer Dude"));
                hairs.Add(new Tuple<int, string>(18, "Short Side Part"));
                hairs.Add(new Tuple<int, string>(19, "High Slicked Sides"));
                hairs.Add(new Tuple<int, string>(20, "Long Slicked"));
                hairs.Add(new Tuple<int, string>(21, "Hipster Youth"));
                hairs.Add(new Tuple<int, string>(22, "Mullet"));
                hairs.Add(new Tuple<int, string>(24, "Classic Cornrows"));
                hairs.Add(new Tuple<int, string>(25, "Palm Cornrows"));
                hairs.Add(new Tuple<int, string>(26, "Lightning Cornrows"));
                hairs.Add(new Tuple<int, string>(27, "Whipped Cornrows"));
                hairs.Add(new Tuple<int, string>(28, "Zig Zag Cornrows"));
                hairs.Add(new Tuple<int, string>(29, "Snail Cornrows"));
                hairs.Add(new Tuple<int, string>(30, "Hightop"));
                hairs.Add(new Tuple<int, string>(31, "Loose Swept Back"));
                hairs.Add(new Tuple<int, string>(32, "Undercut Swept Back"));
                hairs.Add(new Tuple<int, string>(33, "Undercut Swept Side"));
                hairs.Add(new Tuple<int, string>(34, "Spiked Mohawk"));
                hairs.Add(new Tuple<int, string>(35, "Mod"));
                hairs.Add(new Tuple<int, string>(36, "Layered Mod"));

            });

        }
    }
}
