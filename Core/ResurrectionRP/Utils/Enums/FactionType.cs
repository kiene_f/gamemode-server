﻿namespace Enums
{
    public enum FactionType
    {
        LSPD,
        EMS,
        LAFD,
        Mayor
    }
}