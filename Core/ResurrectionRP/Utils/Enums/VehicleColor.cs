namespace Enums
{
    public enum VehicleColor
    {
        MetallicBlack = 0,
        MetallicGraphiteBlack = 1,
        MetallicBlackSteal = 2,
        MetallicDarkSilver = 3,
        MetallicSilver = 4,
        MetallicBlueSilver = 5,
        MetallicSteelGray = 6,
        MetallicShadowSilver = 7,
        MetallicStoneSilver = 8,
        MetallicMidnightSilver = 9,
        MetallicGunMetal = 10,
        MetallicAnthraciteGrey = 11,
        MatteBlack = 12,

    }
}
