﻿namespace Enums
{
    public class LSPD_Rang
    {
        public const int
            Cadet = 1,
            Officier = 2,
            Sergent = 3,
            Lieutenant = 4,
            Capitaine = 5;
    }
}
