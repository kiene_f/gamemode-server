﻿using GrandTheftMultiplayer.Shared;

namespace Enums
{
    static class Events
    {
        public const string PlayerInitialised = "PlayerInitialised";

        // Menu
        public const string OpenAdminMenu = "OpenAdminMenu";
        public const string OpenMainMenu = "OpenMainMenu";
        public const string OpenVehicleMenu = "OpenVehicleMenu";
        public const string OpenATMMenu = "OpenATMMenu";
        public const string OpenGasPompMenu = "OpenGasPompMenu";
        public const string MenuManager_CloseMenu = "MenuManager_CloseMenu";
        public const string InteractionKey = "InteractionKey";
        public const string BuyKey = "BuyKey";


        public const string SetToWayPoint = "SetToWayPoint";
        public const string UpdateMoneyHUD = "UpdateMoneyHUD";

        public const string AnnonceGlobal = "AnnonceGlobal";

        public const string startIntroduction = "start_introduction";

        public const string setWaypoint = "setWaypoint";
        public const string createMarker = "createMarker";

        public const string merrychristmas = "merrychristmas"; 
        public const string startstatut = "startstatut";
       
        public const string UnlockVehicle = "UnlockVehicle";

        // API Extension
        public const string setPlayerMovementClipset = "setPlayerMovementClipset";
        public const string display_subtitle = "display_subtitle";
    }
    
}
