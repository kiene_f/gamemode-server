﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class ConsoleScript : Script
    {
        public ConsoleScript()
        {
            API.onResourceStart += OnResourceStartHandler;
        }

        public void OnResourceStartHandler()
        {
            Task.Run(() =>
            {
                while (true)
                {

                    string cmd = Console.ReadLine();
                    string param = "";
                    if (cmd == null) return;
                    int space = cmd.IndexOf(" ");

                    if (space > 0)
                    {
                        param = cmd.Remove(0, space + 1);
                        cmd = cmd.Remove(space, cmd.Length - space);
                        switch (cmd)
                        {
                            case "serverstop":

                                API.stopResource("resurrectionRP");
                                Environment.Exit(2);
                                break;
                            case "start":
                                API.startResource(param);
                                break;
                            case "stop":
                                API.stopResource(param);
                                break;
                            case "restart":
                                API.stopResource(param);
                                API.startResource(param);
                                break;
                            default:
                                Console.WriteLine(cmd + " est inconnu");
                                break;
                        }
                    }
                }
            });

        }
    }
}
