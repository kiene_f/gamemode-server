﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class FPSCounter : Script
    {
        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;
        private static string title;

        public FPSCounter()
        {
            API.onUpdate += API_onUpdate;
            title = Console.Title;
        }

        private void API_onUpdate()
        {
            Console.Title = title + " FPS: " + CalculateFrameRate();
        }

        public static int CalculateFrameRate()
        {
            if (Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }


    }
}

