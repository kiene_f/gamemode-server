﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class Economy : Script
    {
        // Taxes Variable
        public static double Taxe_Market = 1.0;
        public static double Taxe_Essence = 1.0;
        public static double Taxe_Exportation = 1.0;

        // Price
        public static int Price_Essence = 3;
        public static int Price_Parking = 200;
        public static int Price_Soins = 500;
        public static int Price_RespawnSoins = 1000;

        // Caisse
        public static int CaissePublique = 50000;

        // Salaire
        public static int Maire = 5000;

        public static int LSPD_Capitaine = 4000;
        public static int LSPD_Lieutenant = 2500;
        public static int LSPD_Sergent = 1800;
        public static int LSPD_Officier = 1500;
        public static int LSPD_Cadet = 1000;


        public Economy()
        {

        }

        public static double CalculPriceTaxe(int Price, int Taxes)
        {
            double _price = ((double)Price * ((double)Taxes / (double)100));
            return _price;
        }

        public static double CalculNewPrice(int Price, double Taxes)
        {
            double _price = (Price + Taxes);
            return _price;
        }

        public static decimal CustomRound(decimal num)
        {
            return Math.Round(num * 20.0M, MidpointRounding.AwayFromZero) / 20.0M;
        }

    }
}
