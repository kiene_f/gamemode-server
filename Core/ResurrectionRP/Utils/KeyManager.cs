﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using MenuManagement;
using ResurrectionRP.Businesses;
using ResurrectionRP.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ResurrectionRP
{
    class KeyManager : Script
    {
        public KeyManager()
        {
            API.onClientEventTrigger += OnKeyDown;
        }

        private async void OnKeyDown(Client sender, string eventName, object[] arguments)
        {
            if (eventName == Events.BuyKey)
            {
                NetHandle ped = new NetHandle();
                try
                {
                    ped = (NetHandle)arguments[0];
                } catch { }
                if (ped.IsNull) return;
                NPC npc = NPC.getNPCbyPed(ped);
                Businesses.Businesses business = Businesses.Businesses.getBusinessByPed(npc);
                if (business != null)
                    new BusinessMenu(sender, business);
                return;
            }
            if (eventName != Events.InteractionKey) return;
            PlayerHandler player = Players.getPlayerByClient(sender);
            if (player == null && player.isOnProgress) return;
            if (player.client.hasData("FarmZone"))
                await Farm.startFarming(sender);
            else
            {
                NetHandle ped = new NetHandle();
                try
                {
                    ped = (NetHandle)arguments[0];
                }
                catch { }
                if (ped.IsNull) return;
                NPC npc = NPC.getNPCbyPed(ped);
                await getInteraction(sender, npc);
            }
                
        }
        private async Task getInteraction(Client player, NPC ped)
        {
            if (!ped.hasData("Interaction")) return;
            switch (ped.getData("Interaction"))
            {
                case "Hospital":
                    new HospitalMenu(player, ped);
                    break;
                case "ScooterRent":
                    // ScootRent.openMenuScootRent(player);
                    break;
                case "Market":
                    new MarketMenu(player, ped);
                    break;
                case "Concess":
                    //await VehicleShop.openMenuConcess(player, ped);
                    break;
                case "Ammunation":
                    // Ammunation.openMenuAmmunation(player);
                    break;
                case "Cops":
                    //LSPD_Main.onOpenCopsMenu(player);
                    break;
                case "Traitement":
                    await Processing.startProcessing(player, ped);
                    break;
                case "Trader":
                    await Trader.startTraid(player, ped);
                    break;
                case "Prefecture":
                    //Licence.OpenMenuLicence(player);
                    break;
                case "Barber":
                    new BarberMenu(player, ped, Banner.Barber3);
                    break;
                case "CarDealer":
                    new CarDealerMenu(player, ped);
                    break;
                case "Bennys":
                    new BennysMenu(player, ped);
                    break;
                default:
                    break;
            }
            return;
        }
    }
}
