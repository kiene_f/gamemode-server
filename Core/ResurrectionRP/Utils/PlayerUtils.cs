﻿using System;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System.Collections.Generic;
using Enums;

namespace ResurrectionRP
{
    public class PlayerUtils : Script
    {

        public PlayerUtils()
        {
            API.onClientEventTrigger += onClientEventTrigger;
        }

        public static bool isPlayerLoggedIn(Client player)
        {
            if (API.shared.hasEntitySyncedData(player, "LOGGED_IN"))
            {
                return API.shared.getEntitySyncedData(player, "LOGGED_IN");
            }
            return false;
        }

        public void onClientEventTrigger(Client sender, string eventName, params object[] args)
        {
            if (eventName == "AnimationSystem")
            {
                API.playPlayerAnimation(sender, (int)(AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl), args[0].ToString(), args[1].ToString());
            }
        }

        public static Client getClientPlayerByName(string name)
        {
            List<Client> allPlayers = API.shared.getAllPlayers();
            foreach (Client player in allPlayers)
            {
                if (player.socialClubName == name) { return player; }
            }
            return null;
        }

        public static void onProgress(Client player, bool statut = true)
        {
            API.shared.setEntitySyncedData(player, "InProgress", statut);
        }

        public static bool isOnProgress(Client player)
        {
            bool statut = false;
            statut = (bool)API.shared.getEntitySyncedData(player, "InProgress");
            return statut;
        }

        public static bool isArrested(Client player)
        {
            bool statut = false;
            statut = (bool)API.shared.getEntitySyncedData(player, "Arrested");
            return statut;
        }
    }
}