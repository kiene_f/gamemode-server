﻿var called = false;
var requestedMedic = 0;
API.onResourceStart.connect(function () {
    API.onUpdate.connect(medicalThread);
    //API.onServerEventTrigger.connect(onServerEventTrigger);
    API.onKeyDown.connect(onKeyDownPress);
});

function medicalThread() {
    if (API.isPlayerDead(API.getLocalPlayer())) {
        API.playScreenEffect("DeathFailOut", 1, false);
        sc = API.requestScaleform('MP_BIG_MESSAGE_FREEMODE')
        sc.CallFunction("SHOW_SHARD_WASTED_MP_MESSAGE", "~r~Vous êtes dans le Coma!")
        API.renderScaleform(sc, 0, 0, 1280, 720)
        if (API.getWorldSyncedData("MedicAvailable")) {
            API.sendNotification("Appuyer sur la touche~r~ X~s~ pour contacter les secours, sinon~r~ M ~s~pour en finir...");
        } else {
            API.sendNotification("Aucun médecin n'est disponible, appuyez sur~r~ M~s~ pour réapparaitre à l'hôpital.");
        }
    }
};

function onKeyDownPress(sender, args) {
    if (API.isPlayerDead(API.getLocalPlayer())) {
        if (args.KeyCode == Keys.M) {
            if ((requestedMedic == 0) || (API.getGameTime() > (requestedMedic + 300000))) {
                call = false;
                requestedMedic = 0;
                API.callNative("3182695371859369073", false); //_DISABLE_AUTOMATIC_RESPAWN
                API.callNative("15737497366831513362", true); //SET_FADE_IN_AFTER_DEATH_ARREST
                API.callNative("5339263777479621510", true); //SET_FADE_OUT_AFTER_DEATH
            } else {
                var waitingTime = Math.round(((requestedMedic + 300000) - API.getGameTime()) / 1000);
                API.sendNotification("Vous avez appelé un médecin et devez encore attendre ~r~" + waitingTime + " ~w~secondes pour en finir.");
            }
        }
        else if (args.KeyCode == Keys.X) {
            if ((!called) && (API.getWorldSyncedData("MedicAvailable"))) {
                requestedMedic = API.getGameTime();
                call = true;
                API.sendNotification("~r~Appel transmis.");
                //API.triggerServerEvent("CALL", "Urgence");
            }
        }
    }
}