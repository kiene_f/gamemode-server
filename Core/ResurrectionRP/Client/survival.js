﻿var drink = 100;
var hunger = 100;
var loaded = false;
API.onResourceStart.connect(function () {
    API.onUpdate.connect(onUpdateHUD);
});

function onUpdateHUD() {
    if (!API.getHudVisible() && !loaded) return;
    var res_X = API.getScreenResolutionMaintainRatio().Width;
    var res_Y = API.getScreenResolutionMaintainRatio().Height;
    player = API.getLocalPlayer();
    var health = API.getPlayerHealth(player);
    var maxhealth = API.returnNative("GET_ENTITY_MAX_HEALTH", 0, player);
    var healthpercent = Math.floor((health / maxhealth) * 200);
    var drink = API.getEntitySyncedData(player, "thirst");
    var hunger = API.getEntitySyncedData(player, "hunger");

    API.dxDrawTexture("/Client/Pictures/coeur.png", new Point(Math.round(res_X - 120), Math.round(res_Y - 960)), new Size(40, 40), 1);
    API.dxDrawTexture("/Client/Pictures/faim.png", new Point(Math.round(res_X - 115), Math.round(res_Y - 910)), new Size(40, 40), 1);
    API.dxDrawTexture("/Client/Pictures/soif.png", new Point(Math.round(res_X - 118), Math.round(res_Y - 865)), new Size(35, 35), 1);

    //FAIM
    if (hunger <= 100) {
        API.drawText(hunger + "%", Math.round(res_X - 5), Math.round(res_Y - 908), 0.45, 255, 255, 255, 255, 7, 2, false, true, 0);
    }
    else if (hunger <= 60) {
        API.drawText(hunger + "%", Math.round(res_X - 5), Math.round(res_Y - 908), 0.45, 219, 122, 46, 255, 7, 2, false, true, 0);
    }
    else if (hunger <= 30) {
        API.drawText(hunger + "%", Math.round(res_X - 5), Math.round(res_Y - 908), 0.45, 219, 46, 46, 255, 7, 2, false, true, 0);
    }

    //Soif
    if (drink <= 100) {
        API.drawText(drink + "%", Math.round(res_X - 5), Math.round(res_Y - 860), 0.45, 255, 255, 255, 255, 7, 2, false, true, 0);
    }
    else if (drink <= 60) {
        API.drawText(drink + "%", Math.round(res_X - 5), Math.round(res_Y - 840), 0.45, 219, 122, 46, 255, 7, 2, false, true, 0);
    }
    else if (drink <= 30) {
        API.drawText(drink + "%", Math.round(res_X - 5), Math.round(res_Y - 840), 0.45, 219, 46, 46, 255, 7, 2, false, true, 0);
    }

    //VIE
    if (healthpercent <= 100) {
        API.drawText(healthpercent + "%", Math.round(res_X - 5), Math.round(res_Y - 955), 0.45, 255, 255, 255, 255, 7, 2, false, true, 0);
    }
    else if (healthpercent <= 60) {
        API.drawText(healthpercent + "%", Math.round(res_X - 5), Math.round(res_Y - 945), 0.45, 219, 122, 46, 255, 7, 2, false, true, 0);
    }
    else if (healthpercent <= 30) {
        API.drawText(healthpercent + "%", Math.round(res_X - 5), Math.round(res_Y - 945), 0.45, 219, 46, 46, 255, 7, 2, false, true, 0);
    }


};