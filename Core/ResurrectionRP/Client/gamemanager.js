﻿vehicle_fuel = 0;
API.onResourceStart.connect(function () {
    API.verifyIntegrityOfCache();
    //API.onUpdate.connect(onUpdate);
    var id1 = API.every(5000, "fuelsystem");
});

API.onServerEventTrigger.connect(function (eventName, args) {

    switch (eventName) {
        case 'startstatut': {
            API.sendNotification("Bienvenue sur le serveur GTAV ~r~Resurrection RP~s~.");
            break;
        }

        case 'merrychristmas': {
            API.setSnowEnabled(true, true, true);
            break;
        }

        case 'display_subtitle': {
            API.displaySubtitle(args[0], args[1]);
            break;
        }

        case 'setPlayerMovementClipset': {
            API.setPlayerMovementClipset(API.getLocalPlayer(),args[0], args[1]);
            break;
        }

        case 'createCamera': {
            var newCamera = API.createCamera(args[0], args[1]);
            API.setActiveCamera(newCamera);
            break;
        }

        case 'destroyCamera': {
            API.setActiveCamera(null);
            break;
        }
    }
});

function fuelsystem() {
    if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
        if (API.getVehicleEngineStatus(API.getPlayerVehicle(API.getLocalPlayer()))) {
            var car = API.getPlayerVehicle(API.getLocalPlayer());
            var rpm = API.getVehicleRPM(car);
            vehicle_fuel = API.getVehicleFuelLevel(car);
            vehicle_fuel = vehicle_fuel - rpm * 0.0008;
            API.sendChatMessage(""+vehicle_fuel);
            API.setVehicleFuelLevel(car, vehicle_fuel);
        }
    }
};

API.onResourceStop.connect(function () {
    API.sendNotification("Vous êtes déconnecté du serveur GTAV ~r~Resurrection RP~s~.");
});
