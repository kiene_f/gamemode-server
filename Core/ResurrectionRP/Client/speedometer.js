﻿API.onResourceStart.connect(function () {
    API.onUpdate.connect(onUpdate);
});


var curNeedle = "Client/Pictures/speedometer/needle_day";
var curTachometer = "Client/Pictures/speedometer/tachometer_day";
var curSpeedometer = "Client/Pictures/speedometer/speedometer_day";
var curFuelGauge = "Client/Pictures/speedometer/fuelgauge_day";
var rpm = 0;
var degree = 0;
var blinkertick = 0;
var showBlinker = false;
var overwriteChecks = false;
var showFuelGauge = true;
var showLowFuelYellow = false;
var curAlpha = 0;

function onUpdate() {
    if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
        var veh = API.getPlayerVehicle(API.getLocalPlayer());
        if (API.returnNative('IS_THIS_MODEL_A_CAR', 8, API.getEntityModel(veh))) {
            if (API.getPlayerVehicleSeat(API.getLocalPlayer()) === -1) {
                if (curAlpha >= 255) {
                    curAlpha = 255;
                } else {
                    curAlpha = curAlpha + 5;
                }

            } else {
                if (curAlpha <= 0) {
                    curAlpha = 0;
                } else {
                    curAlpha = curAlpha - 5;
                }
            }
            API.dxDrawTexture(curSpeedometer + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 450), Math.round(API.getScreenResolutionMaintainRatio().Height - 300)), new Size(200, 200), 1);
            if (API.doesEntityExist(veh) && !API.isEntityDead(veh)) {
                var degree = 0;
                var step = 2.32833;
                var vel = API.getEntityVelocity(veh);
                var speed = Math.sqrt(vel.X * vel.X +
                    vel.Y * vel.Y +
                    vel.Z * vel.Z);

                if (speed > 0) { degree = (speed * 2.036936) * step }
                if (degree > 290) { degree = 290; }
                /*
                if (API.getVehicleClass(veh) >= 0 && API.getVehicleClass(veh) >= 13 || API.getVehicleClass(veh) >= 17) {
                    curAlpha = 0;
                }*/

            } else {
                rpm = 0;
                degree = 0;
            }/*
        if (rpm < 0.12 || rpm == null) {
            rpm = 0.12;
        }
		*/
            if (overwriteChecks) {
                showHighBeams, showLowBeams, showBlinker, blinkerleft, blinkerright, showDamageRed, showLowFuelRed, showLowOil = true, true, true, true, true, true, true, true;
            }
            API.dxDrawTexture(curNeedle + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 450), Math.round(API.getScreenResolutionMaintainRatio().Height - 300)), new Size(200, 200), degree);
            var OilLevel = API.getVehicleOilLevel(veh);
            var FuelLevel = API.getVehicleFuelLevel(veh);
            var MaxFuelLevel = 100;
            if (FuelLevel <= MaxFuelLevel * 0.25 && FuelLevel > MaxFuelLevel * 0.13) {
                showLowFuelYellow, showLowFuelRed = true, false;
            } else if (FuelLevel <= MaxFuelLevel * 0.2) {
                showLowFuelYellow, showLowFuelRed = false, false;
            }
            if (OilLevel <= 2.493) {
                showLowOil = true
            } else {
                showLowOil = false;
            }
            if (MaxFuelLevel !== 0) {
                API.dxDrawTexture(curTachometer + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 250), Math.round(API.getScreenResolutionMaintainRatio().Height - 300)), new Size(200, 200), 1);
                rpm = API.getVehicleRPM(veh);
                API.dxDrawTexture(curNeedle + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 250), Math.round(API.getScreenResolutionMaintainRatio().Height - 300)), new Size(200, 200), rpm * 220);
            }
            if (showFuelGauge) {
                API.dxDrawTexture(curFuelGauge + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 290), Math.round(API.getScreenResolutionMaintainRatio().Height - 305)), new Size(80, 40), 1);
                API.dxDrawTexture(curNeedle + ".png", new Point(Math.round(API.getScreenResolutionMaintainRatio().Width - 290), Math.round(API.getScreenResolutionMaintainRatio().Height - 305)), new Size(80, 80), 80 + FuelLevel / MaxFuelLevel * 110);

            }
        }
    }
}

