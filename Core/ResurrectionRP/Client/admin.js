﻿var breaking_news = false;
API.onResourceStart.connect(() => {
    API.onServerEventTrigger.connect(ServerEventTrigger);
});

function ServerEventTrigger(name, args) {
    if (name == "SetToWayPoint") {
        if (API.isWaypointSet()) {
            var pos = API.getWaypointPosition();
            var player = API.getLocalPlayer();
            if (API.isPlayerInAnyVehicle(player)) {
                API.setEntityPosition(API.getPlayerVehicle(player), pos, true);
            } else {
                API.setEntityPosition(player, pos, true);
            }
        }
    }
    else if (name == "AnnonceGlobal") {
        breaking_news = true;
        sc = API.requestScaleform('breaking_news');
        sc.CallFunction('SET_TEXT', args[1], args[0]);
        API.after(30000, "stop_breaking_news");
    }
}

function stop_breaking_news() {
    breaking_news = false;
}

API.onUpdate.connect(() => {
    if (breaking_news) {
        API.renderScaleform(sc, 0, 120, 1280, 720)
    }
})