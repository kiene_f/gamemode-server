﻿let drawinfoopen = false;
let TitleText = "";
let Immatriculation = "";
let Carrosserie = 100;
let Moteur = 100;
let Huile = 100;
let Proprietaire = "";
API.onResourceStart.connect(function () {
    API.onUpdate.connect(drawinfo);
    API.onServerEventTrigger.connect(onServerEventTrigger);
    API.onKeyDown.connect(onKeyDownPress);
    drawinfoopen = false;
});


function onServerEventTrigger(name, args) {
    if (name === "checkvehicle") {
        drawinfoopen = !drawinfoopen;
        TitleText = args[0];
        Proprietaire = args[1];
        Immatriculation = args[2];
        Carrosserie = args[3];
        Moteur = args[4];
        Huile = args[5];
    } else if (name === "closecheckvehicle") {
        drawinfoopen = false;
    }
};

function drawinfo() {
    if (!drawinfoopen) return;
    if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
        var veh = API.getPlayerVehicle(API.getLocalPlayer());
        if (API.doesEntityExist(veh) && !API.isEntityDead(veh)) {
            var res = API.getScreenResolutionMaintainRatio();
            var posX = (res.Width / 2) + 500;
            var posY = (res.Height / 2) - 280;
            API.drawRectangle(posX, posY, 400, 480, 0, 0, 0, 220);
            API.drawText(TitleText, posX + 15, posY + 5, 0.5, 255, 255, 255, 255, 6, 0, false, false, 390); // Information
            API.drawRectangle(posX, posY + 50, 400, 2, 255, 255, 255, 220);

            API.drawText("Propriétaire:", posX + 15, posY + 70, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            API.drawText(Proprietaire, posX + 15, posY + 70, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);

            API.drawText("Immatriculation:", posX + 15, posY + 100, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            API.drawText(Immatriculation, posX + 15, posY + 100, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);

            API.drawText("Etat Carrosserie: ", posX + 15, posY + 130, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            API.drawText(Carrosserie + " %", posX + 15, posY + 130, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);

            API.drawText("Etat Moteur: ", posX + 15, posY + 160, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            API.drawText(Moteur + " %", posX + 15, posY + 160, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);

            API.drawText("Niveau d'huile: ", posX + 15, posY + 190, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            if (API.getVehicleOilLevel(veh) <= 2.493) {
                API.drawText("Faible", posX + 15, posY + 190, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);
            } else {
                API.drawText("Bon", posX + 15, posY + 190, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);
            }

            API.drawText("Niveau de carburant: ", posX + 15, posY + 220, 0.3, 255, 255, 255, 255, 0, 0, false, false, 350);
            API.drawText(API.getVehicleFuelLevel(veh) + " %", posX + 15, posY + 220, 0.3, 255, 255, 255, 255, 0, 2, false, false, 350);
        }
    }
}

function onKeyDownPress(sender, args) {

}