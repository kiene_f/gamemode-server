"use strict";
let erreur = 0;
API.onServerEventTrigger.connect(function (eventName, args) {
    switch (eventName) {
        case "setLoginUiVisible":
            var campos = new Vector3(-1223.413, -1273.963, 69.314);
            var camrot = new Vector3(0, 0, -27.13915);
            var current_cam = API.createCamera(campos, camrot);
            API.setActiveCamera(current_cam);
            API.callNative("_SET_FOCUS_AREA", campos.X, campos.Y, campos.Z, 0.0, 0.0, 0.0);

            LoginUIOpen = args[0];
            if (args[0]) {
                API.setCanOpenChat(false);
                API.disableVehicleEnteringKeys(true);
                API.disableAlternativeMainMenuKey(true);
                API.setHudVisible(false);
                API.setChatVisible(false);
                cursorIndex = 0;
            } else {
                API.setCanOpenChat(true);
                API.disableVehicleEnteringKeys(false);
                API.disableAlternativeMainMenuKey(false);
                API.setHudVisible(true);
                API.setChatVisible(true);
            }
            if (args[1]) {
                erreur++;
            } else {
                erreur = 0;
            }
            break;
        case "loginAccepted":
            API.setCanOpenChat(true);
            API.disableVehicleEnteringKeys(false);
            API.disableAlternativeMainMenuKey(false);
            API.setHudVisible(true);
            API.setChatVisible(true);
            API.setActiveCamera(null);
            API.callNative("CLEAR_FOCUS"); //RESET FOCUS
            break;
    }
});
API.onUpdate.connect(function () {
    if (LoginUIOpen) {
        DrawLoginScreen();
        API.disableAllControlsThisFrame();
    }
});
var LoginUIOpen = false;
var password = "";
var passwordPlaceholder = "Entrer le mot de passe";
var res = API.getScreenResolutionMaintainRatio();
var safe = (res.Height / 4);
var posX = safe * 2.5;
var posY = safe;
var Width = res.Width - (5 * safe);
var Height = 2 * safe;
var PasswordY = posY + 290;
var defaultTextBoxBg = new Vector3(241, 241, 241);
var highlightTextBoxBg = new Vector3(183, 255, 96);
var defaultButtonBg = new Vector3(55, 55, 55);
var highlightButtonBg = new Vector3(122, 122, 122);
var cursorIndex = 0;
function DrawLoginScreen() {
    API.drawRectangle(posX, posY, Width, Height, 30, 30, 30, 240);
    API.dxDrawTexture("/Client/Pictures/logo.png", new Point(Math.round(posX + (Width / 4.4)), Math.round(posY + 20)), new Size(300, 300), 1);
    API.drawText("Password", posX + 50, PasswordY, 0.7, 255, 255, 255, 255, 4, 0, false, false, 500);
    if (cursorIndex === 0) {
        API.drawRectangle(posX + 50, PasswordY + 50, Width - 100, 50, highlightTextBoxBg.X, highlightTextBoxBg.Y, highlightTextBoxBg.Z, 255);
    }
    else {
        API.drawRectangle(posX + 50, PasswordY + 50, Width - 100, 50, defaultTextBoxBg.X, defaultTextBoxBg.Y, defaultTextBoxBg.Z, 255);
    }
    if (password.length === 0) {
        API.drawText(passwordPlaceholder, posX + 60, PasswordY + 50, 0.7, 50, 50, 50, 255, 4, 0, false, false, Width - 120);
    }
    else {
        API.drawText(password.replace(/./g, "*"), posX + 60, PasswordY + 60, 0.7, 50, 50, 50, 255, 4, 0, false, false, Width - 120);
    }
    if (cursorIndex === 1) {
        API.drawRectangle(posX + 50, PasswordY + 130, Width - 100, 65, highlightButtonBg.X, highlightButtonBg.Y, highlightButtonBg.Z, 240);
    }
    else {
        API.drawRectangle(posX + 50, PasswordY + 130, Width - 100, 65, defaultButtonBg.X, defaultButtonBg.Y, defaultButtonBg.Z, 240);
    }
    API.drawText("Accepter", posX + (Width / 2), PasswordY + 135, 0.7, 255, 255, 255, 255, 4, 1, false, false, 500);
    if (erreur !== 0) {
        if (erreur === 1) {
            API.drawText("~r~[ERREUR] Mots de passe incorrecte 1�re tentative", posX + 60, PasswordY + 210, 0.3, 50, 50, 50, 255, 4, 0, false, false, Width - 120);

        } else if (erreur === 2) {
            API.drawText("~r~[ERREUR] Mots de passe incorrecte seconde tentative", posX + 60, PasswordY + 210, 0.3, 50, 50, 50, 255, 4, 0, false, false, Width - 120);

        } else if (erreur === 3) {
            erreur = 0;
            API.disconnect("Mots de passe incorrecte troisieme tentative");
        }
    }
}
API.onKeyDown.connect(function (sender, e) {
    if (LoginUIOpen) {
        switch (e.KeyCode) {
            case Keys.Tab:
                switch (cursorIndex) {
                    case 0:
                        cursorIndex = 1;
                        break;
                    case 1:
                        cursorIndex = 0;
                        break;
                }
                break;
            case Keys.Up:
                if (cursorIndex === 0) {
                    cursorIndex = 1;
                }
                else {
                    cursorIndex--;
                }
                break;
            case Keys.Down:
                if (cursorIndex === 1) {
                    cursorIndex = 0;
                }
                else {
                    cursorIndex++;
                }
                break;
            case Keys.Enter:
                    LoginButtonTriggered();
                break;
            case Keys.Back:
                if (password.length !== 0) {
                    password = password.substring(0, password.length - 1);
                }
                break;
            default:
                password += API.getCharFromKey(e.KeyValue, e.Shift, e.Control, e.Alt);
        }
    }
});
function LoginButtonTriggered() {
    LoginUIOpen = false;
    API.disableVehicleEnteringKeys(false);
    API.triggerServerEvent("Login", password);
}
