﻿var loaded = false;
var lookingAtEntity = null;
var lookingAtPosition = null;
var modelname = null;
var textlabel = null;
var detectbool = false;
var atmdetect = false;
var vendingdetect = false;
var pumpdetect = false;

API.onResourceStart.connect(() => {
    API.onKeyDown.connect(onKeyDown);
    API.onServerEventTrigger.connect(ServerEventTrigger);
    var id1 = API.every(100, "HandleRayTracing");
})

function ServerEventTrigger(name, args) {
    if (name === "PlayerInitialised") { loaded = true; }
}

class cefExternal {
    constructor(resourcePath) {
        this.path = resourcePath;
        this.open = false;
    }

    show() {
        if (this.open === false) {
            this.open = true;
            const resolution = API.getScreenResolution();

            this.browser = API.createCefBrowser(API.getScreenResolution().Width, API.getScreenResolution().Height, false);
            API.setCefBrowserPosition(this.browser, 0, 0);
            API.waitUntilCefBrowserInit(this.browser);
            API.loadPageCefBrowser(this.browser, this.path);
            API.showCursor(true);
        }
    }

    destroy() {
        this.open = false;
        API.destroyCefBrowser(this.browser);
        API.showCursor(false);
    }

    eval(string) {
        this.browser.eval(string);
    }
}

var phone = null;

function onKeyDown(sender, args) {
    if (!loaded) return;
    switch (args.KeyCode) {
        case Keys.F10: {
            API.triggerServerEvent("OpenAdminMenu");
            break;
        }

        case Keys.F2: {
            API.triggerServerEvent("OpenMainMenu");
            break;
        }
        
        case Keys.I: {
            API.triggerServerEvent("OpenVehicleMenu");
            break;
        }

        case Keys.F3: {
            if (phone == null) {
                phone = new cefExternal("http://127.0.0.1:3000/");
                API.playSoundFrontEnd("Pull_Out", "Phone_SoundSet_Franklin");
                phone.show();
            }
            else {
                phone.destroy();
                phone = null;
            }
            break;
        }

        case Keys.E: {
            if (atmdetect) {
                API.triggerServerEvent("OpenATMMenu");
            }
            else if (pumpdetect) {
                API.triggerServerEvent("OpenGasPompMenu");
            }
            else {
                var peds = API.getStreamedPeds();
                for (var i = 0; i < peds.Length; i++) {
                    var pos = API.returnNative("GET_ENTITY_COORDS", 5, peds[i], true);
                    if (pos.DistanceTo(API.getEntityPosition(API.getLocalPlayer())) < 2) {
                        API.triggerServerEvent("InteractionKey", peds[i]);
                        break;
                    }
                }
            }
            break;
        }

        case Keys.I: {
            var peds = API.getStreamedPeds();
            for (var i = 0; i < peds.Length; i++) {
                var pos = API.returnNative("GET_ENTITY_COORDS", 5, peds[i], true);
                if (pos.DistanceTo(API.getEntityPosition(API.getLocalPlayer())) < 2) {
                    API.triggerServerEvent("BuyKey", peds[i]);
                    break;
                }
            }
            break;
        }


        case Keys.U: {
            API.triggerServerEvent("UnlockVehicle");
            break;
        }

        case Keys.M: {
            API.triggerServerEvent("VoiceRangeChange");
            break;
        }

        case Keys.T: {
            if (API.getEntitySyncedData(API.getLocalPlayer(), "adminrank") < 1) {
                API.setCanOpenChat(false);
            } else {
                API.setCanOpenChat(true);
            }
            if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
                API.triggerServerEvent("EngineStart");
            }
            break;
        }
    }

}


function HandleRayTracing() {
    if (!loaded) { return; }
    if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) { return; }
    lookingAtEntity = GetlookingAtEntity();
    lookingAtPosition = lookingAtEntity != null ? API.getEntityPosition(lookingAtEntity) : null;
    if (lookingAtEntity != null || lookingAtPosition != null) {
        var modellooking = API.getEntityModel(lookingAtEntity);
        if ((AtmModels.indexOf(modellooking) != -1)) {
            modelname = "le distributeur.";
            lookingAtPosition.Z += 1;
            lookingAtPosition.Y += 1;
            atmdetect = true;
        }
        if ((VendingModels.indexOf(modellooking) != -1)) {
            modelname = "le distributeur.";
            lookingAtPosition.Z += 1;
            lookingAtPosition.Y += 1;
            vendingdetect = true;
        }
        if ((GasPumpModels.indexOf(modellooking) != -1)) {
            modelname = "la pompe à essence.";
            lookingAtPosition.Z += 1;
            pumpdetect = true;
        }

        if (!detectbool) {
            textlabel = API.createTextLabel("Appuyer sur la touche ~r~E~s~ pour interagir", lookingAtPosition, 35, 0.5, true);
            API.startMusic("Client/Sounds/blip.mp3", false);
            detectbool = true;
        }
    } else {
        if (detectbool) {
            API.deleteEntity(textlabel);
            textlabel = null;
            detectbool = false;
            pumpdetect = false;
            atmdetect = false;
            vendingdetect = false;
        }
    }
}

function getAimPoint() {
    var resolution = API.getScreenResolution();
    return API.screenToWorld(new PointF(resolution.Width / 2, resolution.Height / 2));
}

function Vector3Lerp(start, end, fraction) {
    return new Vector3(
        (start.X + (end.X - start.X) * fraction),
        (start.Y + (end.Y - start.Y) * fraction),
        (start.Z + (end.Z - start.Z) * fraction)
    );
}

var AtmModels = [
    -1126237515,
    -1364697528,
    506770882,
    -870868698
];

var GasPumpModels = [
    1339433404,
    1933174915,
    -2007231801,
    -462817101,
    -469694731,
    1694452750,
    1694,
    750
];

var VendingMachineModels = [
    992069095,
    690372739,
    1114264700,
    -1317235795,
    -1034034125,
    1099892058,
    -654402915
];

function GetlookingAtEntity() {
    var startPoint = API.getGameplayCamPos();
    var aimPoint = getAimPoint();

    startPoint.Add(aimPoint);

    var endPoint = Vector3Lerp(startPoint, aimPoint, 1.1);
    var rayCast = API.createRaycast(startPoint, endPoint, 16, null);

    if (!rayCast.didHitEntity) return null;

    var hitEntityHandle = rayCast.hitEntity;
    var entityModel = API.getEntityModel(hitEntityHandle);

    if (AtmModels.indexOf(entityModel) == -1 && GasPumpModels.indexOf(entityModel) == -1 && VendingMachineModels.indexOf(entityModel) == -1) return null;

    if (API.getEntityPosition(hitEntityHandle).DistanceTo(API.getEntityPosition(API.getLocalPlayer())) > 4) return null;

    return hitEntityHandle;
}

API.onResourceStop.connect(() => {
    soundplayed = false;
    loaded = false;
})
