﻿API.onResourceStart.connect(() => {
    API.onServerEventTrigger.connect(ServerEventTrigger);
});

function ServerEventTrigger(name, args) {
    if (name == "setWaypoint") {
        API.setWaypoint(args[0], args[1]);
    }
    if (name == "createMarker") {
        API.createMarker(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
    }
}