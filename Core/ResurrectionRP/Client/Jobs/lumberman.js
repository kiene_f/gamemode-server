﻿var canHatchet = true;
var previousTime = 0;
var HatchetTimeout = 4;

API.onResourceStart.connect(() => {
    API.onUpdate.connect(onUpdate);
});

function onUpdate() {
    if (!API.hasEntitySyncedData(API.getLocalPlayer(), "LumberZone")) return;
    if (!API.getEntitySyncedData(API.getLocalPlayer(), "LumberZone")) return;
    skillTimer();

    if (API.isControlJustPressed(24)) {
        if (API.getPlayerCurrentWeapon() === -102973651) {
            if (canHatchet) {
                var objects = API.getStreamedObjects();
                for (var i = 0; i < objects.Length; i++) {
                    var pos = API.returnNative("GET_ENTITY_COORDS", 5, objects[i], true);
                    if (pos.DistanceTo(API.getEntityPosition(API.getLocalPlayer())) < 2) {
                        if (API.getEntityModel(objects[i]) === -1279773008) {
                            API.triggerServerEvent("ValidateLumberjackSwing", objects[i]);
                            canHatchet = false;
                            return;
                        }
                    }
                }
            }
        }
    }
    else if (API.isControlJustPressed(38)) {
        var objects = API.getStreamedObjects();
        for (var i = 0; i < objects.Length; i++) {
            var pos = API.returnNative("GET_ENTITY_COORDS", 5, objects[i], true);
            if (pos.DistanceTo(API.getEntityPosition(API.getLocalPlayer())) < 2) {
                if (API.getEntityModel(objects[i]) === 1366334172) {
                    API.triggerServerEvent("PickupLumber", objects[i]);
                    return;
                }
            }
        }
    }
};

function skillTimer() {
    if (Math.ceil(API.getGlobalTime() / 100) * 100 % 500 === 0) {
        if (previousTime === Math.ceil(API.getGlobalTime() / 100) * 100) return;
        if (!canHatchet) {
            if (HatchetTimeout > 0) HatchetTimeout--;
            else {
                canHatchet = true;
                HatchetTimeout = 4;
            }
        }
        previousTime = Math.ceil(API.getGlobalTime() / 100) * 100;
    }
};

function getForwardPos(player, distance) {
    var pos = API.getEntityPosition(player);
    var rot = API.getEntityRotation(player).Z;

    var xOff = -(Math.sin((rot * Math.PI) / 180) * distance);
    var yOff = Math.cos((rot * Math.PI) / 180) * distance;

    return pos.Add(new Vector3(xOff, yOff, 0));
}
