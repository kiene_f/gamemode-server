﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Database : Script
    {
        private static string _connectionStr;
        public static bool MysqlReady = false;

        public Database()
        {
            API.onResourceStart += onResourceStart;
        }

        private async void onResourceStart()
        {
            try
            {
                _connectionStr = string.Format("server={0};user={1};password={2};database={3};port={4};SslMode=none",
                        API.getSetting<string>("host"),
                        API.getSetting<string>("user"),
                        API.getSetting<string>("password"),
                        API.getSetting<string>("database"),
                        API.getSetting<string>("port")
                    );
                using (MySqlConnection conn = new MySqlConnection(_connectionStr))
                {
                    API.consoleOutput("[MySQL][INFO] Attempting connecting to MySQL");
                    await conn.OpenAsync();
                    if (conn.State == ConnectionState.Open)
                    {
                        API.consoleOutput("[MySQL][INFO] Connected to MySQL");
                        MysqlReady = true;
                    }
                }
            }
            catch (MySqlException ex)
            {
                API.consoleOutput("[MySQL][ERROR] " + ex.ToString());
                API.delay(5000, true, () => { Environment.Exit(1); });
            }
            await Task.FromResult(0);
        }

        public static async Task<int> insertQuery(string sql)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionStr))
                {
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    int id = Convert.ToInt32(cmd.LastInsertedId);
                    await conn.CloseAsync();
                    return (id);
                }
            }
            catch (MySqlException ex) { API.shared.consoleOutput("[MySQL][ERROR] " + ex.ToString()); return (-1);}
        }

        public static async Task<DataTable> getQuery(string sql)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionStr))
                {
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    await conn.OpenAsync();
                    DbDataReader rdr = await cmd.ExecuteReaderAsync();
                    DataTable results = new DataTable();
                    results.Load(rdr);
                    rdr.Close();
                    await conn.CloseAsync();
                    return results;
                }
            }
            catch (MySqlException ex)
            {
                API.shared.consoleOutput("[MySQL][ERROR] " + ex.ToString());
                return null;
            }
        }

        public static DataTable getQueryNoAsync(string sql)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionStr))
                {
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    conn.OpenAsync();
                    DbDataReader rdr = cmd.ExecuteReader();
                    DataTable results = new DataTable();
                    results.Load(rdr);
                    rdr.Close();
                    conn.CloseAsync();
                    return results;
                }
            }
            catch (MySqlException ex)
            {
                API.shared.consoleOutput("[MySQL][ERROR] " + ex.ToString());
                return null;
            }
        }

        public static int boolToInt(bool check)
        {
            if (check)
                return 1;
            else
                return 0;

        }

        public async static Task waitMysqlReady()
        {
            while (!MysqlReady) { };
            await Task.FromResult(0);
        }
    }
}
