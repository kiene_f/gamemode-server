﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class CommercialAgency : Script
    {
        private MenuItem _acheter;
        private MenuItem _vendre;

        public CommercialAgency()
        {
            API.onResourceStart += (() =>
            {
                Vector3 _position = new Vector3(-709.8483f, -128.1362f, 37.81717f);
                CylinderColShape _col = API.createCylinderColShape(_position, 2f, 2f);
                Marker _marker = API.createMarker(1, _col.Center - new Vector3(0, 0, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);

                _col.onEntityEnterColShape += ((_shape, _entity) =>
                {
                    Client _player = API.getPlayerFromHandle(_entity);
                    if (_player == null) return;
                    OpenCommerceMenu(_player);
                });

            });
        }

        private void OpenCommerceMenu(Client client)
        {
            Menu menu = new Menu("AgenceCommercial", "AgenceCommercial", "", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
            menu.BannerColor = new Color(0, 255, 255, 64);
            menu.Callback = MenuCallback;

            _acheter = new MenuItem("Acheter un commerce", "Acheter un commerce disponible.");
            _acheter.ExecuteCallback = true;
            menu.Add(_acheter);

            _vendre = new MenuItem("Vendre un commerce", "Vendre votre commerce.");
            _vendre.ExecuteCallback = true;
            menu.Add(_vendre);

            MenuManager.OpenMenu(client, menu);
        }

        private void MenuCallback(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == _acheter)
            {
                Menu _menu = new Menu("AgenceCommercial", "AgenceCommercial", "Commerces disponible:", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
                menu.BannerColor = new Color(0, 255, 255, 64);

                foreach(var commerce in Businesses.Businesses.businesses)
                {
                    if (commerce.owner == null)
                    {
                        Console.WriteLine(commerce.type);
                        /*
                        switch (commerce.type)
                        {
                            case "":

                        }
                        */

                       // MenuItem item = new MenuItem(commerce.)
                    }
                }

                MenuManager.OpenMenu(client, menu);
            }
            else if (menuItem == _vendre)
            {

            }
        }
    }
}
