﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using ResurrectionRP.Businesses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleInfoLoader;
using VehicleInfoLoader.Data;

namespace ResurrectionRP.Menus
{
    class BennysMenu
    {
        private Menu mainmenu;
        private VehicleManifest vehmanifest;
        private VehicleHandler vh;
        private Vehicle veh;
        private MenuItem returnitem;

        public BennysMenu(Client client, NPC ped)
        {
            var _bennys = Bennys.getBennysByPed(ped);
            if (!_bennys.employees.Contains(client.socialClubName))
            {
                client.sendNotificationError("Vous n'êtes pas employé ici!");
                return;
            }
            veh = VehicleHandler.getNearestVehicle(_bennys.positionZone.pos.toVector3(), 3f);
            vh = Vehicles.getVehicleByVehicle(veh);
            if (veh == null)
            {
                client.sendNotificationError("Aucun véhicule dans la zone!");
                return;
            }
            try
            {
                vehmanifest = veh.Manifest();
            }
            catch { }

            if (vehmanifest == null) return;
            if (!vehmanifest.HasMods)
            {
                client.sendNotificationError("Modification impossible sur ce véhicule!");
                return;
            }

            mainmenu = new Menu("benny", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true, banner: Banner.SuperMod);
            mainmenu.Callback = menuCallBack;
            mainmenu.OnItemSelect = "menu.Visible = false;"; // SHITING FIX FOR +10 MENUITEM !

            var modlist = new Dictionary<int, string>();
            modlist.Add(0, "Spoiler");
            modlist.Add(1, "Pare-choc Avant");
            modlist.Add(2, "Pare-choc Arrière");
            modlist.Add(3, "Jupe latérale");
            modlist.Add(4, "Échappement");
            modlist.Add(5, "Cadre");
            modlist.Add(6, "Grille");
            modlist.Add(7, "Capot");
            modlist.Add(8, "Aile");
            modlist.Add(9, "Aile Droite");
            modlist.Add(10, "Toit");
            modlist.Add(11, "Moteur");
            modlist.Add(12, "Frein");
            modlist.Add(13, "Transmission");
            modlist.Add(14, "Klaxon");
            modlist.Add(15, "Suspension");
            modlist.Add(16, "Armure");
            modlist.Add(18, "Turbo");
            modlist.Add(22, "Xenon");
            modlist.Add(23, "Roues");
            if (veh.isBike)
                modlist.Add(24, "Roues arrière");
            modlist.Add(25, "Supports de plaque");
            modlist.Add(27, "Design intérieur");
            modlist.Add(28, "Ornements");
            modlist.Add(30, "Compteur");
            modlist.Add(33, "Volant");
            modlist.Add(34, "Levier de vitesses");
            modlist.Add(35, "Plaques");
            modlist.Add(38, "Hydraulics");
            modlist.Add(48, "Skin");
            modlist.Add(62, "Assiette");
            modlist.Add(66, "Couleur 1");
            modlist.Add(67, "Couleur 2");
            modlist.Add(69, "Teinte des vitres");
            modlist.Add(74, "Couleur du tableau de bord");
            modlist.Add(75, "Couleur de garniture");

            foreach (var mod in modlist)
            {
                if (vehmanifest.HasMod(mod.Key))
                {
                    MenuItem item = new MenuItem(mod.Value, executeCallback: true);
                    item.setData("mod", mod.Key);
                    mainmenu.Add(item);
                }
            }

            MenuManager.OpenMenu(client, mainmenu);
        }

        private void menuCallBack(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (!menuItem.hasData("mod") && menuItem == returnitem) return;
            int menuItemID = menuItem.getData("mod");
            MenuManager.CloseMenu(client);
            Menu submenu = new Menu("1", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true, banner: Banner.SuperMod);


            foreach (var mod in vehmanifest.Mods(menuItemID))
                submenu.Add(new MenuItem(mod.localizedName, executeCallback: true, executeCallbackIndexChange: true));

            returnitem = new MenuItem("~r~Retourner", executeCallback: true);
            submenu.Add(returnitem);

            MenuManager.OpenMenu(client, submenu);
            veh.setMod(menuItemID, 0);
            submenu.CallbackCurrentItem = ((_client, _menu, _menuItem, _itemIndex, _data) => {
                veh.setMod(menuItemID, _itemIndex);
            });

            submenu.Callback = ((_client, _menu, _menuItem, _itemIndex, _forced, _data) =>
            {
                if (_menuItem == returnitem)
                    MenuManager.OpenMenu(client, mainmenu);
                else
                {
                    if (vh.mods.ContainsKey(menuItemID))
                        vh.mods[menuItemID] = _itemIndex;
                    else
                        vh.mods.Add(menuItemID, _itemIndex);
                }
            });
            submenu.Finalizer = ((_client, _menu) => {

                var mods = vh.mods;
                if (mods != null && mods.Count > 0)
                    foreach (KeyValuePair<int, int> mod in mods)
                        API.shared.setVehicleMod(veh, mod.Key, mod.Value);

            });

        }
    }
}
