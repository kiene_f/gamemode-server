﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using MenuManagement;
using ResurrectionRP.Businesses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Menus
{
    class ClothingStoreMenu
    {
        private ClothingStore _clothingStore;
        private PlayerHandler _player;
        private ListItem LegsItem;
        private ListItem TorsoItem;
        private ListItem FeetItem;
        private ListItem UndershirtsItem;
        private ListItem TopsItem;

        public ClothingStoreMenu(Client client, CylinderColShape colshape, Banner banner)
        {
            _player = Players.getPlayerByClient(client);
            _clothingStore = ClothingStore.getClothingStoreByColshape(colshape);

            Menu menu = new Menu("ClothingStore", "", "", 0, 0, Menu.MenuAnchor.MiddleRight);
            menu.BannerSprite = banner;
            menu.Callback = clothingOwnerMenuManager;
            menu.BackCloseMenu = true;
            menu.OnListChange = "";

            if (_player.character.Gender == 0)
            {
                List<string> Torso = new List<string>();
                for(int i = 0; i < 134; i++) { Torso.Add(i.ToString()); }
                TorsoItem = new ListItem("Torse", "", "torse", Torso, 0);
                TorsoItem.ExecuteCallback = true;
                menu.Add(TorsoItem);

                List<string> Legs = new List<string>();
                for (int i = 0; i < 93; i++) { Legs.Add(i.ToString()); }
                LegsItem = new ListItem("Jambes", "", "legs", Legs, 0);
                LegsItem.ExecuteCallback = true;
                menu.Add(LegsItem);

                List<string> Feet = new List<string>();
                for (int i = 0; i < 66; i++) { Feet.Add(i.ToString()); }
                FeetItem = new ListItem("Chaussures", "", "feet", Feet, 0);
                FeetItem.ExecuteCallback = true;
                menu.Add(FeetItem);

                List<string> Undershirts = new List<string>();
                for (int i = 0; i < 121; i++) { Undershirts.Add(i.ToString()); }
                UndershirtsItem = new ListItem("Maillots", "", "undershirts", Undershirts, 0);
                UndershirtsItem.ExecuteCallback = true;
                menu.Add(UndershirtsItem);

                List<string> Tops = new List<string>();
                for (int i = 0; i < 240; i++) { Tops.Add(i.ToString()); }
                TopsItem = new ListItem("Vestes", "", "tops", Tops, 0);
                TopsItem.ExecuteCallback = true;
                menu.Add(TopsItem);
            }
            else if (_player.character.Gender == 1)
            {
                List<string> Torso = new List<string>();
                for (int i = 0; i < 168; i++) { Torso.Add(i.ToString()); }
                TorsoItem = new ListItem("Torse", "", "torse", Torso, 0);
                TorsoItem.ExecuteCallback = true;
                menu.Add(TorsoItem);

                List<string> Legs = new List<string>();
                for (int i = 0; i < 96; i++) { Legs.Add(i.ToString()); }
                LegsItem = new ListItem("Jambes", "", "legs", Legs, 0);
                LegsItem.ExecuteCallback = true;
                menu.Add(LegsItem);

                List<string> Feet = new List<string>();
                for (int i = 0; i < 69; i++) { Feet.Add(i.ToString()); }
                FeetItem = new ListItem("Chaussures", "", "feet", Feet, 0);
                FeetItem.ExecuteCallback = true;
                menu.Add(FeetItem);

                List<string> Undershirts = new List<string>();
                for (int i = 0; i < 151; i++) { Undershirts.Add(i.ToString()); }
                UndershirtsItem = new ListItem("Maillots", "", "undershirts", Undershirts, 0);
                UndershirtsItem.ExecuteCallback = true;
                menu.Add(UndershirtsItem);

                List<string> Tops = new List<string>();
                for (int i = 0; i < 248; i++) { Tops.Add(i.ToString()); }
                TopsItem = new ListItem("Vestes", "", "tops", Tops, 0);
                TopsItem.ExecuteCallback = true;
                menu.Add(TopsItem);
            }
            else
            {
                _player.client.sendNotificationError("Votre personnage ne vous permet pas d'utiliser ce magasin de vêtement.");
            }
        }

        private void clothingOwnerMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == LegsItem)
            {

            }
            else if (menuItem == LegsItem)
            {

            }
            else if (menuItem == FeetItem)
            {

            }
            else if (menuItem == UndershirtsItem)
            {

            }
            else if (menuItem == TopsItem)
            {

            }
        }
    }
}
