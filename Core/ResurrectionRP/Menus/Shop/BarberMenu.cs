﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using ResurrectionRP.Businesses;
using ResurrectionRP.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ResurrectionRP.Menus
{
    class BarberMenu
    {
        private Barber _barber;
        private PlayerHandler _player;
        private PlayerHandler PlayerSelected;
        private MenuItem item;
        private Banner _banner;
        private MenuItem itemCoupe;
        private MenuItem itemBarbe;
        private Menu barbermenu;
        private Menu cheveuxmenu;
        private int hair =0;
        private int beard =0;
        private ListItem _beardpricelist;
        private ListItem _hairpricelist;

        public BarberMenu(Client client, NPC ped, Banner banner)
        {
            _player = Players.getPlayerByClient(client);
            _barber = Barber.getBarberByPed(ped);
            _banner = banner;
            
            if (_barber.isOwner(client))
            {
                Console.WriteLine(_barber.owner);
                Menu mainmenu = new Menu("Barber", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true);
                mainmenu.BannerSprite = _banner;
                mainmenu.Finalizer = ((_client, menu)=> { CharacterCreator.ApplyCharacter(PlayerSelected.client); });
                
                item = new MenuItem("Récupérer l'argent (" + _barber.cashBox + " $)", "", "ID_money");
                item.ExecuteCallback = true;
                mainmenu.Add(item);
                
                List<string> _playerlist = new List<string>();
                foreach (PlayerHandler player in Players.getNearestPlayer(client, 20f)) { _playerlist.Add(player.name); };
                ListItem _playerSelected = new ListItem("Client:", "Choix du client", "PlayerSelected", _playerlist, _playerlist.FindIndex(x => x == client.name));
                _playerSelected.ExecuteCallback = true;
                mainmenu.Add(_playerSelected);
                PlayerSelected = Players.getNearestPlayer(client, 5f)[0];
                List<string> pricelist = new List<string>();
                for (int i = 0; i <= 20000; i += 100) { pricelist.Add(i.ToString()); };
                
                _beardpricelist = new ListItem("Prix taille de barbe:", "Choix du prix de la barbe", "ID_beardprice", pricelist, _barber.beardprice);
                _beardpricelist.ExecuteCallback = true;
                mainmenu.Add(_beardpricelist);
                
                _hairpricelist = new ListItem("Prix coupe de cheveux:", "Choix du prix de la coupe", "ID_hairprice", pricelist, _barber.hairprice);
                _hairpricelist.ExecuteCallback = true;
                mainmenu.Add(_hairpricelist);
                
                itemCoupe = new MenuItem("Faire une coupe de cheveux", $"Coupe de cheveux à ~r~{PlayerSelected.name}", "ID_cheveux");
                itemCoupe.ExecuteCallback = true;
                mainmenu.Add(itemCoupe);
                
                itemBarbe = new MenuItem("Tailler une barbe", $"Tailler une barbe à ~r~{PlayerSelected.name}", "ID_barbe");
                itemBarbe.ExecuteCallback = true;
                mainmenu.Add(itemBarbe);
                
                mainmenu.Callback = ((sender, menu, menuitem, itemindex, forced, data) => {

                    PlayerSelected = Players.getPlayerByName(Convert.ToString(data["PlayerSelected"]["Value"]).Replace("{", "").Replace("}", ""));
                    if (PlayerSelected == null) PlayerSelected = Players.getPlayerByName(_playerlist[0]);

                    List<string> _couleurlist = new List<string>();
                    for (int i = 0; i <= 255; i++) { _couleurlist.Add(i.ToString()); };
                    ListItem couleurlistitem = new ListItem("Couleur:", "Choix de la couleur", "Couleur", _couleurlist, 0);
                    couleurlistitem.ExecuteCallback = true;

                    MenuItem valider = new MenuItem("~r~Valider", "", "Hairs");
                    valider.ExecuteCallback = true;

                    switch (menuitem.Id)
                    {
                        case "ID_beardprice":
                            this._barber.beardprice = Convert.ToInt32(data["ID_beardprice"]["Value"]);
                            client.sendNotificationSuccess("Vous avez changer le prix à : $" + _barber.beardprice.ToString());
                            break;
                        case "ID_hairprice":
                            this._barber.hairprice = Convert.ToInt32(data["ID_hairprice"]["Value"]);
                            client.sendNotificationSuccess("Vous avez changer le prix à : $" + _barber.hairprice.ToString());
                            break;
                        case "ID_money":
                            client.sendNotificationSuccess($"Vous avez récupérer {_barber.cashBox} des caisse de votre commerce.");
                            _player.money += _barber.cashBox;
                            _barber.cashBox = 0;
                            break;
                        case "ID_cheveux":
                            cheveuxmenu = new Menu("Barber", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                            cheveuxmenu.BannerSprite = _banner;
                            cheveuxmenu.Add(couleurlistitem);
                            cheveuxmenu.Add(valider);

                            foreach (var hairs in BeardsHairsList.hairs)
                            {
                                MenuItem item = new MenuItem(hairs.Item2, "","Hairs");
                                item.ExecuteCallback = true;
                                cheveuxmenu.Add(item);
                            }
                            cheveuxmenu.Callback = (async (_client, _menu, _menuitem, _itemindex, _forced, _data) =>
                            {
                                if(_menuitem == couleurlistitem)
                                {
                                    API.shared.sendNativeToPlayersInRange(PlayerSelected.client.position, 50f, Hash._SET_PED_HAIR_COLOR, PlayerSelected.client, Convert.ToInt32(_data["Couleur"]["Value"]), Convert.ToInt32(_data["Couleur"]["Value"]));
                                }
                                else if (_menuitem == valider)
                                {
                                    if (PlayerSelected.money >= _barber.hairprice)
                                    {
                                        PlayerSelected.money -= _barber.hairprice;
                                        CharacterCreator.CustomPlayerData[PlayerSelected.client].Hair.Hair = hair;
                                        CharacterCreator.CustomPlayerData[PlayerSelected.client].Hair.Color = Convert.ToInt32(_data["Couleur"]["Value"]);
                                        CharacterCreator.ApplyCharacter(PlayerSelected.client);
                                        await _player.updateCharacter();
                                    }else
                                    {
                                        PlayerSelected.client.sendNotificationError("Vous n'avez pas assez d'argent sur vous!");
                                    }
                                    MenuManager.CloseMenu(client);
                                }
                                else
                                {
                                    hair = BeardsHairsList.hairs[_itemindex -2].Item1;
                                    API.shared.setPlayerClothes(PlayerSelected.client, 2, hair , 0);
                                    API.shared.sendNativeToPlayersInRange(PlayerSelected.client.position, 50f, Hash._SET_PED_HAIR_COLOR, PlayerSelected.client, Convert.ToInt32(_data["Couleur"]["Value"]), 0);
                                }
                            });
                            MenuManager.OpenMenu(client, cheveuxmenu);
                            break;
                        case "ID_barbe":
                            barbermenu = new Menu("Barber", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                            barbermenu.BannerSprite = _banner;
                            barbermenu.BackCloseMenu = true;
                            barbermenu.Add(couleurlistitem);
                            barbermenu.Add(valider);

                            foreach (var beard in BeardsHairsList.beards)
                            {
                                MenuItem item = new MenuItem(beard.Item2);
                                item.ExecuteCallback = true;
                                barbermenu.Add(item);
                            }
                            barbermenu.Callback = (async (_client, _menu, _menuitem, _itemindex, _forced, _data) =>
                            {
                                if (_menuitem == couleurlistitem)
                                {
                                    API.shared.sendNativeToPlayersInRange(PlayerSelected.client.position, 50f, Hash._SET_PED_HEAD_OVERLAY_COLOR, PlayerSelected.client, 1, 1, Convert.ToInt32(_data["Couleur"]["Value"]), 0);
                                }
                                else if (_menuitem == valider)
                                {
                                    if (PlayerSelected.money >= _barber.beardprice)
                                    {
                                        PlayerSelected.money -=_barber.beardprice;
                                        CharacterCreator.CustomPlayerData[PlayerSelected.client].Appearance[1].Value = beard;
                                        CharacterCreator.CustomPlayerData[PlayerSelected.client].Appearance[1].Opacity = 255;
                                        CharacterCreator.CustomPlayerData[PlayerSelected.client].BeardColor = Convert.ToInt32(_data["Couleur"]["Value"]);
                                        CharacterCreator.ApplyCharacter(PlayerSelected.client);
                                        await _player.updateCharacter();
                                    }
                                    else
                                    {
                                        PlayerSelected.client.sendNotificationError("Vous n'avez pas assez d'argent sur vous!");
                                    }
                                    MenuManager.CloseMenu(client); 
                                }
                                else
                                {
                                    beard = BeardsHairsList.beards[_itemindex - 2].Item1;
                                    API.shared.sendNativeToPlayersInRange(PlayerSelected.client.position, 50f, Hash.SET_PED_HEAD_OVERLAY, PlayerSelected, 1, beard, 255);
                                    API.shared.sendNativeToPlayersInRange(PlayerSelected.client.position, 50f, Hash._SET_PED_HEAD_OVERLAY_COLOR, PlayerSelected.client, 1, 1, Convert.ToInt32(_data["Couleur"]["Value"]), 0);
                                }
                            });
                            MenuManager.OpenMenu(client, barbermenu);
                            break;
                    }
                });
                MenuManager.OpenMenu(client, mainmenu);
            }
            else
            {
                client.sendNotificationError("Vous n'êtes pas le proprietaire.");
            }
        }
    }
}
