﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResurrectionRP.Businesses;
using MenuManagement;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Gta.Vehicle;
using Enums;
using VehicleInfoLoader;
using VehicleInfoLoader.Data;

namespace ResurrectionRP.Menus
{
    class CarDealerMenu
    {
        private CarDealer _concess;
        private PlayerHandler _player;
        private int primarycolorselect = 0;
        private int secondcolorselect = 0;
        private Vehicle VehTempo;
        private Tuple<VehicleHash, int, int> vehiclechoised;
        private Vector3 _spawnpos;

        public CarDealerMenu(Client client, NPC ped)
        {
            _player = Players.getPlayerByClient(client);
            _concess = CarDealer.getCarDealerByPed(ped);

            Menu mainmenu = new Menu(_concess.name, _concess.name, "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true);
            mainmenu.BannerSprite = Banner.Garage;

            if (_concess.vehiclelist != null)
            {
                foreach (var vehiclelist in _concess.vehiclelist)
                {
                    var vehinfo = VehicleInfoLoader.VehicleInfoLoader.Get(vehiclelist.Item1);
                    string electric = "Non";
                    if (vehinfo.electric) electric = "Oui";

                    MenuItem VehicleItem = new MenuItem(vehinfo.localizedManufacturer+ " " + vehinfo.localizedName)
                    {
                        RightLabel = "$" + Convert.ToString(vehiclelist.Item2),
                        ExecuteCallback = true,
                        ExecuteCallbackIndexChange = true,
                        Description =
                        $"Taille du coffre: {vehiclelist.Item3.ToString()} \n" +
                        $"Nombre de place: {vehinfo.maxOccupants} \n" +
                        $"Electrique: {electric}"
                    };
                    mainmenu.Add(VehicleItem);
                }
            }
            _spawnpos = new Vector3();
            if (_concess.cylcol1.getAllEntities().Count() <= 0)
                _spawnpos = _concess.cylcol1.Center;
            else if (_concess.cylcol1.getAllEntities().Count() > 0 && _concess.cylcol2.getAllEntities().Count() <= 0)
                _spawnpos = _concess.cylcol2.Center;
            else
            {
                _player.client.sendNotificationError("Aucune place n'est disponible.");
                return;
            }

            vehPreview(_concess.vehiclelist[0].Item1);
            mainmenu.CallbackCurrentItem = ((_client, _menu, _menuitem, _itemindex, _data) => {
                vehPreview(_concess.vehiclelist[_itemindex].Item1);
            });

            mainmenu.Callback = ((_client, _menu, _menuitem, _itemindex, _forced, _data) =>
            {
                vehiclechoised = _concess.vehiclelist[_itemindex];
                Task.Run(() =>
                {
                    API.shared.sleep(100);
                    if (_player.money >= vehiclechoised.Item2)
                    {
                        int money = _player.money;
                        money -= vehiclechoised.Item2;
                        _player.money = money;
                        _player.client.triggerEvent(Events.MenuManager_CloseMenu);
                        firstcolor();
                    }
                    else
                    {
                        _player.client.sendNotificationError("Vous n'avez pas assez d'argent!");
                        return;
                    }
                });
            });
            mainmenu.Finalizer = Finalizer;
            MenuManager.OpenMenu(client, mainmenu);
        }

        private void Finalizer(Client client, Menu menu)
        {
            Console.Write("test");
           if (VehTempo != null) VehTempo.delete();
        }

        private void firstcolor()
        {
            Menu primarycolor = new Menu("Color", "Peinture", "Choisissez une peinture:", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true, noExit: false);
            primarycolor.BannerSprite = Banner.Garage;
            int i = 0;
            foreach (GrandTheftMultiplayer.Shared.Gta.Vehicle.VehicleColor color in Enum.GetValues(typeof(GrandTheftMultiplayer.Shared.Gta.Vehicle.VehicleColor)))
            {
                MenuItem peinture = new MenuItem(color.ToString(), id: i.ToString())
                {
                    ExecuteCallback = true,
                    ExecuteCallbackIndexChange = true
                };
                i++;
                primarycolor.Add(peinture);
            }

            primarycolor.CallbackCurrentItem = ((_client, _menu, _menuitem, _itemindex, _data) => {
                primarycolorselect = _itemindex;
                VehTempo.primaryColor = primarycolorselect;
            });

            primarycolor.Callback = ((_client, _menu, _menuitem, _itemindex, _forced, _data) => {
                primarycolorselect = _itemindex;
                VehTempo.primaryColor = primarycolorselect;
                _player.client.triggerEvent(Enums.Events.MenuManager_CloseMenu);
                secondcolor();
            });

            MenuManager.OpenMenu(_player.client, primarycolor);
        }

        private void secondcolor()
        {
            Menu secondcolor = new Menu("Color", "Peinture", "Choisissez une peinture:", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true, noExit: false);
            secondcolor.BannerSprite = Banner.Garage;
            int i = 0;
            foreach (GrandTheftMultiplayer.Shared.Gta.Vehicle.VehicleColor color in Enum.GetValues(typeof(GrandTheftMultiplayer.Shared.Gta.Vehicle.VehicleColor)))
            {
                MenuItem peinture = new MenuItem(color.ToString(), id: i.ToString())
                {
                    ExecuteCallback = true,
                    ExecuteCallbackIndexChange = true
                };
                i++;
                secondcolor.Add(peinture);
            }

            secondcolor.CallbackCurrentItem = ((_client, _menu, _menuitem, _itemindex, _data) => {
                secondcolorselect = _itemindex;
                VehTempo.secondaryColor = secondcolorselect;
            });

            secondcolor.Callback = ((_client, _menu, _menuitem, _itemindex, _forced, _data) => {
                secondcolorselect = _itemindex;
                VehTempo.secondaryColor = secondcolorselect;
                _player.client.triggerEvent(Enums.Events.MenuManager_CloseMenu);
                VehTempo.delete();
                Task.Run(() =>
                {
                    API.shared.sleep(100);
                    if (_player.money >= vehiclechoised.Item2)
                    {
                        int money = _player.money;
                        money -= vehiclechoised.Item2;
                        _player.money = money;

                        VehicleHandler veh = new VehicleHandler(_player.socialClubName, vehiclechoised.Item1, _spawnpos, new Vector3(), primarycolorselect, secondcolorselect, 100, 100, vehiclechoised.Item3, engineStatus: true);
                        _player.client.setIntoVehicle(veh.vehicle, -1);
                    }
                    else
                    {
                        _player.client.sendNotificationError("Vous n'avez pas assez d'argent!");
                        return;
                    }
                });
            });

            MenuManager.OpenMenu(_player.client, secondcolor);
        }
        private void vehPreview(VehicleHash hash)
        {
            if (VehTempo != null) VehTempo.delete();
            API.shared.sleep(100);
            VehTempo = API.shared.createVehicle(hash, _spawnpos, new Vector3(), 0, 0);
            VehTempo.freezePosition = true;
            VehTempo.locked = true;
            _player.client.setIntoVehicle(VehTempo, -1);
        }
    }
}