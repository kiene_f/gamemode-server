﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using System.Collections.Generic;

namespace ResurrectionRP.Menus
{
    class GasPompMenu : Script
    {
        private List<Vehicle> _vehicles;

        public GasPompMenu()
        {
            API.onClientEventTrigger += openGasPompMenu;
        }

        private void openGasPompMenu(Client client, string eventName, object[] arguments)
        {
            if (eventName != Events.OpenGasPompMenu) return;

            _vehicles = VehicleHandler.getNearestVehicles(client);

            Menu mainmenu = new Menu("sell", "Station d'essence", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true);

            foreach (Vehicle vehicle in _vehicles)
            {
                MenuItem item = new MenuItem(vehicle.displayName + " " + vehicle.fuelLevel, "", "ID_sell");
                item.ExecuteCallback = true;
                mainmenu.Add(item);
            }

            /*
            mainmenu.Callback = ((sender, menu, menuitem, itemindex, forced, data) =>
            {
                if (menuitem.Id == "ID_sell")
                {
                    _business.owner = null;
                    _player.client.sendNotificationSuccess("Vous venez de vendre votre affaire pour " + Convert.ToInt32(_business.businessPrice / 2) + " $ !");
                } else if (menuitem.Id == "ID_add")
                {
                    _business.employees.Add(data["ID_add"]["Value"].ToString());
                } else if (menuitem.Id == "ID_delete")
                {
                    _business.employees.Remove(data["ID_delete"]["Value"].ToString());
                }
                MenuManager.CloseMenu(sender);
            });
            */
            MenuManager.OpenMenu(client, mainmenu);
        }
    }
}
