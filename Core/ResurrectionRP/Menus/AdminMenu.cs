﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class AdminMenu : Script
    {
        private Menu savedMenu = null;
        private static CheckboxItem GodModeItem = null;
        private ListItem _playerSelected;
        private MenuItem _tpToWaypoint;
        private ListItem _moneySelected;
        private MenuItem _tpToPlayer;
        private ListItem _rangSelected;
        private MenuItem _spectaclePlayer;
        private MenuItem _spawncar;
        private MenuItem _kickPlayer;
        private MenuItem _banPlayer;
        private MenuItem _msgglobal;
        private MenuItem _pedtransform;
        private MenuItem _killPlayer;
        private MenuItem _restartRessource;
        private Client PlayerSelected;
        private MenuItem _giveweapon;

        public AdminMenu()
        {
            API.onClientEventTrigger += OpenAdminMenu;
        }

        private void OpenAdminMenu(Client client, string eventName, params object[] arguments)
        {
            if (eventName != Events.OpenAdminMenu) return;
            if (Players.getPlayerByClient(client) == null) return;
            if (Players.getPlayerByClient(client).staffRank <= AdminRank.Player) return;
            Menu menu = savedMenu;

            menu = new Menu("ID_AdminMenu", "Admin Menu", "Choisissez une option:", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
            menu.BannerColor = new Color(255, 0, 0, 1);
            menu.Callback = AdminMenuManager;

            List<string> _playerlist = new List<string>();
            foreach (PlayerHandler player in Players.players) { _playerlist.Add(player.name); };
            _playerSelected = new ListItem("Player:", "Choix de la cible", "PlayerSelected", _playerlist, _playerlist.FindIndex(x => x == client.name));
            _playerSelected.ExecuteCallback = true;
            menu.Add(_playerSelected);

            if (Players.getPlayerByClient(client).staffRank >= AdminRank.Moderator)
            {
                List<string> _moneylist = new List<string> { "1", "10", "100", "1000", "10000", "100000", "1000000" };
                _moneySelected = new ListItem("Give Money:", "Choix de la somme à donner", "ID_GiveMoney", _moneylist, 0);
                _moneySelected.ExecuteCallback = true;
                menu.Add(_moneySelected);
            }

            if (Players.getPlayerByClient(client).staffRank >= AdminRank.Developer)
            {
                List<string> _adminlistrang = new List<string>();
                foreach (int value in Enum.GetValues(typeof(AdminRank)))
                {
                    if ((int)Players.getPlayerByClient(client).staffRank > value){
                        _adminlistrang.Add(((AdminRank)value).ToString());
                    }
                }
                _rangSelected = new ListItem("Staff Rang:", "Choix du rang à donner", "ID_Rang", _adminlistrang, (int)Players.getPlayerByClient(client).staffRank);
                _rangSelected.ExecuteCallback = true;
                menu.Add(_rangSelected);


                _restartRessource = new MenuItem("Restart Dev Ressource", "", "ID_RestartRessource");
                _restartRessource.ExecuteCallback = true;
                menu.Add(_restartRessource);
            }

            GodModeItem = new CheckboxItem("God Mode", "", "ID_Godmode", API.getEntityInvincible(client));
            GodModeItem.ExecuteCallback = true;
            menu.Add(GodModeItem);

            _tpToWaypoint = new MenuItem("Teleport to WayPoint", "", "ID_Waypoint");
            _tpToWaypoint.ExecuteCallback = true;
            menu.Add(_tpToWaypoint);

            _tpToPlayer = new MenuItem("Teleport to Player", "", "ID_TpToPlayer");
            _tpToPlayer.ExecuteCallback = true;
            menu.Add(_tpToPlayer);

            _spectaclePlayer = new MenuItem("Spectacle", "", "ID_Spectacle");
            _spectaclePlayer.ExecuteCallback = true;
            menu.Add(_spectaclePlayer);

            _spawncar = new MenuItem("Spawn Car", "", "ID_SpawnCar");
            _spawncar.SetInput("", 30, InputType.Text);
            _spawncar.ExecuteCallback = true;
            menu.Add(_spawncar);

            _giveweapon = new MenuItem("Give Weapon", "", "ID_Weapon");
            _giveweapon.SetInput("", 30, InputType.Text);
            _giveweapon.ExecuteCallback = true;
            menu.Add(_giveweapon);

            _pedtransform = new MenuItem("Transform to Ped", "", "ID_Ped");
            _pedtransform.SetInput("", 250, InputType.Text);
            _pedtransform.ExecuteCallback = true;
            menu.Add(_pedtransform);

            _killPlayer = new MenuItem("Kill", "", "ID_Kill");
            _killPlayer.ExecuteCallback = true;
            menu.Add(_killPlayer);

            _kickPlayer = new MenuItem("Kick", "", "ID_Kick");
            _kickPlayer.ExecuteCallback = true;
            menu.Add(_kickPlayer);

            _banPlayer = new MenuItem("Ban", "", "ID_Ban");
            _banPlayer.ExecuteCallback = true;
            menu.Add(_banPlayer);

            _msgglobal = new MenuItem("Message Global", "", "ID_MSGGlobal");
            _msgglobal.SetInput("", 250, InputType.Text);
            _msgglobal.ExecuteCallback = true;
            menu.Add(_msgglobal);

            MenuManager.OpenMenu(client, menu);
        }

        private void AdminMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            try
            {
                PlayerHandler _playerdistant = Players.getPlayerByName(Convert.ToString(data["PlayerSelected"]["Value"]).Replace("{", "").Replace("}", ""));
                PlayerSelected = _playerdistant.client;
            }
            catch
            {
                PlayerHandler _playerdistant = Players.players[0];
                PlayerSelected = _playerdistant.client;
            }

            if (menuItem == GodModeItem)
            {
                bool _Invincible = Convert.ToBoolean(data["ID_Godmode"]);
                if (_Invincible)
                {    
                    if (PlayerSelected != client)
                        client.sendNotification("", $"~r~[ADMIN]~w~ {PlayerSelected.name} est invincible.");
                    else
                        PlayerSelected.sendNotification("", "~r~[ADMIN]~w~ Vous êtes invincible.");
                } else if (!_Invincible)
                {
                    if (PlayerSelected != client)
                        client.sendNotification("", $"~r~[ADMIN]~w~ {PlayerSelected.name} n'est plus invincible.");
                    else
                        PlayerSelected.sendNotification("", "~r~[ADMIN]~w~ Vous n'êtes plus invincible.");
                }
                API.setEntityInvincible(PlayerSelected, _Invincible);
            }
            else if (menuItem == _tpToWaypoint) {  PlayerSelected.triggerEvent(Events.SetToWayPoint); }
            else if (menuItem == _moneySelected)
            {
                try
                {
                    PlayerHandler player = Players.getPlayerByClient(PlayerSelected);
                    player.money += Convert.ToInt32(data["ID_GiveMoney"]["Value"]);
                    _moneySelected.Description = "Argent du joueur: " + player.money.ToString();
                }
                catch { }

            }
            else if (menuItem == _tpToPlayer)
            {
                if (client == PlayerSelected) return;
                client.position = PlayerSelected.position;
            }
            else if (menuItem == _rangSelected)
            {
                foreach (int value in Enum.GetValues(typeof(AdminRank)))
                    if (Convert.ToString(data["ID_Rang"]["Value"]) == ((AdminRank)value).ToString())
                        Players.getPlayerByClient(client).staffRank = ((AdminRank)value);     
            }
            else if (menuItem == _spectaclePlayer)
            {
                if (client == PlayerSelected) return;
                API.setPlayerToSpectatePlayer(client, PlayerSelected);
            }
            else if (menuItem == _spawncar)
            {
                try
                {
                    client.sendChatMessage(Convert.ToString(data["ID_SpawnCar"]));
                    VehicleHandler veh = new VehicleHandler(PlayerSelected.socialClubName, (VehicleHash)API.getHashKey(Convert.ToString(data["ID_SpawnCar"])), PlayerSelected.position, PlayerSelected.rotation, fuel: 100, fuelMax: 100);
                    API.setPlayerIntoVehicle(client, veh.vehicle, -1);
                }
                catch { }

            }
            else if (menuItem == _giveweapon)
            {
                string test = Convert.ToString(data["ID_Weapon"]);
                API.consoleOutput(test);
                WeaponHash weapon = (WeaponHash)API.getHashKey(test);
                PlayerSelected.giveWeapon(weapon, 200, true, true);
            }
            else if (menuItem == _kickPlayer)
            {
                API.kickPlayer(PlayerSelected);
            }
            else if (menuItem == _banPlayer)
            {
                API.banPlayer(PlayerSelected);
            }
            else if (menuItem == _msgglobal)
            {
                string _msg = Convert.ToString(data["ID_MSGGlobal"]);
                if (string.IsNullOrEmpty(_msg)) return;
                //API.sendPictureNotificationToAll(_msg, "CHAR_MULTIPLAYER", 0, 0, "Message à toute la population", "");
                foreach(Client player in API.getAllPlayers())
                {
                    player.triggerEvent(Events.AnnonceGlobal, "Message à toute la population", _msg);
                }
            }
            else if (menuItem == _pedtransform)
            {
                try {
                    PlayerSelected.setSkin(API.pedNameToModel(Convert.ToString(data["ID_Ped"]).Replace("{", "").Replace("}", "")));
                } catch
                {
                    client.sendNotification("", $"~r~[ADMIN]~w~ skin invalide.");
                }    
            }
            else if (menuItem == _killPlayer)
            {
                API.setPlayerHealth(PlayerSelected, -1);
            }
            else if (menuItem == _restartRessource)
            {
                //API.restartResource(Main._changresourceName);
                API.restartResource("dev");
            }
        }
    }
}
