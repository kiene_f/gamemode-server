﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Elements;
using Enums;
using MenuManagement;
using GrandTheftMultiplayer.Server.Constant;

namespace ResurrectionRP
{
    class ATMMenu : Script
    {
        private Menu menu;
        private MenuItem _retrait;
        private MenuItem _depot;
        private MenuItem _transfert;
        private MenuItem _balance;

        public ATMMenu()
        {
            API.onClientEventTrigger += OpenAtmMenu;
        }

        private void OpenAtmMenu(Client client, string eventName, object[] arguments)
        {
            if (eventName != Events.OpenATMMenu) return;

            menu = new Menu("ID_ATMMenu", "", "", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
            menu.SubTitle = "Votre solde bancaire est de $" + Players.getPlayerByClient(client).bank.ToString();
            menu.BannerColor = new Color(220,0,0,75);
            menu.BannerTexture = "./Client/Pictures/mazebank.png";
            menu.Callback = AtmMenuManager;

            _balance = new MenuItem("Balance", "", "ID_balance");
            _balance.ExecuteCallback = true;
            menu.Add(_balance);

            _retrait = new MenuItem("Retrait d'argent", "", "ID_retrait");
            _retrait.SetInput("", 10, InputType.Number);
            _retrait.ExecuteCallback = true;
            menu.Add(_retrait);

            _depot = new MenuItem("Déposer de l'argent", "", "ID_depot");
            _depot.SetInput("", 10, InputType.Number);
            _depot.ExecuteCallback = true;
            menu.Add(_depot);

            _transfert = new MenuItem("Transférer de l'argent", "", "ID_transfert");
            _transfert.ExecuteCallback = true;
            menu.Add(_transfert);

            MenuManager.OpenMenu(client, menu);
        }

        private async void AtmMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == _retrait)
            {
                int somme = 0;
                if (int.TryParse(Convert.ToString(data["ID_retrait"]), out somme))
                {
                    if (await MoneyAPI.takeBankMoney(client, somme))
                    {
                        API.sendPictureNotificationToPlayer(client, $"Vous avez retirer la somme de {somme.ToString()}", "CHAR_BANK_MAZE", 0, 0, "RETRAIT:", "");
                        client.triggerEvent(Events.MenuManager_CloseMenu);
                    }
                    else
                    {
                        API.sendPictureNotificationToPlayer(client, "~r~FOND INSUFFISANT", "CHAR_BANK_MAZE", 0, 0, "~r~OPERATION ERREUR", "");
                        API.playSoundFrontEnd(client, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    }
                }
                else
                {
                    API.sendPictureNotificationToPlayer(client, "~r~ERREUR DE SAISIE", "CHAR_BANK_MAZE", 0, 0, "~r~OPERATION ERREUR", "");
                    API.playSoundFrontEnd(client, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                }
            }
            else if (menuItem == _depot)
            {
                int somme = 0;
                if (int.TryParse(Convert.ToString(data["ID_depot"]), out somme))
                {
                    if (MoneyAPI.giveBankMoney(client, somme))
                    {
                        API.sendPictureNotificationToPlayer(client, $"Vous avez déposer la somme de {somme.ToString()}", "CHAR_BANK_MAZE", 0, 0, "RETRAIT:", "");
                        client.triggerEvent(Events.MenuManager_CloseMenu);
                    }
                    else
                    {
                        API.sendPictureNotificationToPlayer(client, "~r~FOND INSUFFISANT", "CHAR_BANK_MAZE", 0, 0, "~r~OPERATION ERREUR", "");
                        API.playSoundFrontEnd(client, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    }
                }
                else
                {
                    API.sendPictureNotificationToPlayer(client, "~r~ERREUR DE SAISIE", "CHAR_BANK_MAZE", 0, 0, "~r~OPERATION ERREUR", "");
                    API.playSoundFrontEnd(client, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                }

            }
            else if (menuItem == _transfert)
            {
                API.sendNotificationToPlayer(client, "Ici y'aura quelque chose... devine quoi ");
            }
            else if (menuItem == _balance)
            {
                API.sendPictureNotificationToPlayer(client, $"           $" + Players.getPlayerByClient(client).bank.ToString(), "CHAR_BANK_MAZE", 0, 0, "SOLDE BANCAIRE:", "");
            }
        }
    }
}
