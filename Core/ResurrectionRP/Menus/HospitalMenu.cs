﻿using Enums;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Menus
{
    class HospitalMenu
    {
        public HospitalMenu(Client client, NPC ped)
        {
            Menu _menu = new Menu("Hospital", "Hôpital", "", 0, 0, Menu.MenuAnchor.MiddleRight);
            _menu.BannerColor = new Color(255, 0, 0, 1);
            _menu.Callback = HopitalManager;
            _menu.BackCloseMenu = true;
            _menu.NoExit = false;
            
            MenuItem soinsui = new MenuItem("Se Soigner","","soins");
            soinsui.RightLabel = Economy.Price_Soins.ToString() + "$";
            soinsui.ExecuteCallback = true;
            _menu.Add(soinsui);
            MenuItem quitter = new MenuItem("Quitter", "", "quitter");
            quitter.ExecuteCallback = true;
            _menu.Add(quitter);
             
            MenuManager.OpenMenu(client, _menu);
        }

        public void HopitalManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {

            if (itemIndex == 0)
            {
                if (Players.getPlayerByClient(client).money >= Economy.Price_Soins)
                {
                    client.health = 100;
                    Players.getPlayerByClient(client).money -= Economy.Price_Soins;
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
                else
                {
                    client.sendNotificationError("Vous n'avez pas assez d'argent sur vous!");
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
            }
            else if (itemIndex == 1)
            {
                client.triggerEvent(Events.MenuManager_CloseMenu);
            }
        }
    }
}
