﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Menus
{
    class VehicleMenu : Script
    {
        private PlayerHandler _player;
        private VehicleHandler _vehicle;

        public VehicleMenu()
        {
            API.onClientEventTrigger += openVehicleMenu;
        }

        private void openVehicleMenu(Client client, string eventName, object[] arguments)
        {
            if (eventName == Events.UnlockVehicle)
            {
                _vehicle = Vehicles.getVehicleByVehicle(VehicleHandler.getNearestVehicle(client));
                if (_vehicle == null) return;
                if (_vehicle.pid == client.socialClubName)
                {
                    if (!_vehicle.vehicle.locked)
                        API.shared.sendNotificationToPlayer(client, "Vous avez ~r~fermé ~w~le véhicule", 27);
                    else
                        API.shared.sendNotificationToPlayer(client, "Vous avez ~g~ouvert ~w~le véhicule", 25);
                    _vehicle.vehicle.locked = !_vehicle.vehicle.locked;
                }
            }
            if (eventName != Events.OpenVehicleMenu) return;
            _player = Players.getPlayerByClient(client);
            _vehicle = Vehicles.getVehicleByVehicle(VehicleHandler.getNearestVehicle(client));
            if (_vehicle != null)
            {
                Menu menu = new Menu("vehicle", "Véhicle", "", 0, 0, Menu.MenuAnchor.MiddleRight,enableBanner: true);
                menu.BannerColor = new Color(0, 255, 255, 0);
                menu.Callback = vehicleMenuManager;
                menu.BackCloseMenu = true;
                MenuItem item;
                // Driver
                if (API.getPlayerVehicleSeat(_player.client) == -1)
                {
                    if (!_vehicle.vehicle.engineStatus)
                        item = new MenuItem("Démarrer le véhicule", "", "ID_start");
                    else
                        item = new MenuItem("Eteindre le véhicule", "", "ID_stop");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                }
                item = new MenuItem("Voir l'inventaire du véhicule", "", "ID_inventory");
                item.ExecuteCallback = true;
                menu.Add(item);
                item = new MenuItem("Gestion des portes", "", "ID_doors");
                item.ExecuteCallback = true;
                menu.Add(item);

                MenuManager.OpenMenu(client, menu);
            }
        }

        private void vehicleMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            switch (menuItem.Id)
            {
                case "ID_start":
                    API.shared.setVehicleEngineStatus(_vehicle.vehicle, true);
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                    break;
                case "ID_stop":
                    API.shared.setVehicleEngineStatus(_vehicle.vehicle, false);
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                    break;
                case "ID_inventory":
                    if (_vehicle.vehicle.locked)
                    {
                        _player.client.sendNotification("", "Le coffre du véhicule est ~r~fermé");
                        return;
                    }
                    //API.setVehicleDoorState(_vehicle.vehicle, 5, false);
                    menu = new Menu("vInventory", "Inventaire", "Emplacements: " + _vehicle.inventory.currentSize() + " / " + _vehicle.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight, enableBanner: true);
                    menu.BannerColor = new Color(0, 255, 255, 0);
                    menu.Callback = vehiculeInventoryMenuManager;
                    menu.BackCloseMenu = true;
                    MenuItem item;
                    foreach (ItemStack inv in _vehicle.inventory.inventory)
                    {
                        List<string> values = new List<string>();
                        for (int i = 1; i <= inv.quantity; i++) values.Add(i.ToString());
                        item = new ListItem(inv.item.name, inv.item.description, "ID_item", values, 0);
                        item.ExecuteCallback = true;
                        menu.Add(item);
                    }
                    if (_vehicle.inventory.inventory.Count <= 0)
                    {
                        _player.client.sendNotification("", "Le coffre du véhicule est vide.");
                        return;
                    }
                    API.shared.setVehicleDoorState(_vehicle.vehicle, 5, !API.shared.getVehicleDoorState(_vehicle.vehicle, 5));

                    MenuManager.OpenMenu(client, menu);
                    break;
                case "ID_doors":
                    if (_vehicle.vehicle.locked)
                    {
                        _player.client.sendNotification("", "Le coffre du véhicule est ~r~fermé");
                        return;
                    }
                    menu = new Menu("vInventory", "Portes", "", 0, 10, Menu.MenuAnchor.MiddleRight, enableBanner: true);
                    menu.BannerColor = new Color(0, 255, 255, 0);
                    menu.Callback = vehiculeDoorsMenuManager;
                    menu.BackCloseMenu = true;
                    item = new MenuItem("Porte avant gauche", "", "ID_frontLeft");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    item = new MenuItem("Porte avant droite", "", "ID_frontRight");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    item = new MenuItem("Porte arrière gauche", "", "ID_backLeft");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    item = new MenuItem("Porte arrière droite", "", "ID_backRight");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    item = new MenuItem("Capot", "", "ID_hood");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    item = new MenuItem("Coffre", "", "ID_trunk");
                    item.ExecuteCallback = true;
                    menu.Add(item);
                    MenuManager.OpenMenu(client, menu);
                    break;
            }
        }

        private void vehiculeDoorsMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            int door = 0;
            switch (menuItem.Id)
            {
                case "ID_frontLeft":
                    door = 0;
                    break;
                case "ID_frontRight":
                    door = 1;
                    break;
                case "ID_backLeft":
                    door = 2;
                    break;
                case "ID_backRight":
                    door = 3;
                    break;
                case "ID_hood":
                    door = 4;
                    break;
                case "ID_trunk":
                    door = 5;
                    break;
            }
            API.shared.setVehicleDoorState(_vehicle.vehicle, door, !API.shared.getVehicleDoorState(_vehicle.vehicle, door));
        }

        private void vehiculeInventoryMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            ItemStack itemStack = _vehicle.inventory.inventory[itemIndex];
            _vehicle.inventory.delete(_player.client, itemStack.item, Convert.ToInt32(data["ID_item"]["Value"]));
            _player.inventory.add(_player.client, itemStack.item, Convert.ToInt32(data["ID_item"]["Value"]));
            client.triggerEvent(Events.MenuManager_CloseMenu);
        }
    }
}
