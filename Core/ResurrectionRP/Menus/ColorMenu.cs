﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Gta.Vehicle;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Menus
{
    class ColorMenu
    {
        public ColorMenu(Client client, VehicleHandler veh)
        {
            Menu primarycolor = new Menu("Color", "Peinture", "Choisissez une peinture:", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true, noExit:false);
            primarycolor.BannerSprite = Banner.Garage;
            int i = 0;
            foreach (VehicleColor color in Enum.GetValues(typeof(VehicleColor)))
            {
                MenuItem peinture = new MenuItem(color.ToString(), id: i.ToString())
                {
                    ExecuteCallback = true,
                    ExecuteCallbackIndexChange = true
                };
                i++;
                primarycolor.Add(peinture);
            }

            primarycolor.CallbackCurrentItem = ((_client, _menu, _menuitem, _itemindex, _data) => {
                veh.vehicle.primaryColor = _itemindex;
            });

            primarycolor.Callback = ((_client, _menu, _menuitem, _itemindex, _forced, _data) => {
                client.triggerEvent(Enums.Events.MenuManager_CloseMenu);
            });

            MenuManager.OpenMenu(client, primarycolor);

        }
    }
}
