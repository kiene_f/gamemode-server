﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class InventaireMenu
    {
        public InventaireMenu()
        {
        }


        private static MenuItem _utiliser;
        private static ListItem _jeter;
        private static ListItem _donner;
        private static ListItem _coffreveh;

        public static void OpenMenuInventory(Client client)
        {
            PlayerHandler player = Players.getPlayerByClient(client);

            Menu menu = new Menu("Inventaire", "Inventaire", "Emplacements: "+ player.inventory.currentSize()+ "/"+ player.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
            menu.BannerColor = new Color(0, 255, 255, 64);
            menu.Callback = InventaireMenuManager;
            menu.BackCloseMenu = true;
      
            foreach(ItemStack inv in player.inventory.inventory)
            {
                MenuItem _item = new MenuItem(inv.item.name, inv.item.description);
                _item.RightLabel = inv.quantity.ToString();
                _item.ExecuteCallback = true;
                menu.Add(_item);
            }

            MenuManager.OpenMenu(client, menu);
        }

        private static void InventaireMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            try
            {
                ItemStack _itemstack = Players.getPlayerByClient(client).inventory.inventory[itemIndex];
                client.setData("INVItemSelected", _itemstack);
                Menu submenu = new Menu(_itemstack.item.name, _itemstack.item.name, "", 0, 0, Menu.MenuAnchor.MiddleRight);
                submenu.SubTitle = $"Quantité: {_itemstack.quantity} Poids: {_itemstack.item.weight} Poids Total: {_itemstack.item.weight * _itemstack.quantity} ";
                submenu.BannerColor = new Color(0, 255, 255, 64);
                submenu.Callback = SelectedMenuManager;
                submenu.BackCloseMenu = true;

                List<string> _values = new List<string>();
                for (int i = 1; i <= _itemstack.quantity; i++) _values.Add(i.ToString()); 

                _utiliser = new MenuItem("Utiliser");
                _utiliser.ExecuteCallback = true;
                submenu.Add(_utiliser);

                _jeter = new ListItem("Jeter","","ID_jeter", _values,0);
                _jeter.ExecuteCallback = true;
                submenu.Add(_jeter);

                _donner = new ListItem("Donner", "", "ID_donner", _values, 0);
                _donner.ExecuteCallback = true;
                submenu.Add(_donner);

                _coffreveh = new ListItem("Mettre dans le véhicule", "", "ID_voiture", _values, 0);
                _coffreveh.ExecuteCallback = true;
                submenu.Add(_coffreveh);

                MenuManager.OpenMenu(client, submenu);
            }
            catch
            {
                //404 error not found
            }

        }

        private static void SelectedMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            try
            {
                ItemStack _itemstack = (ItemStack)client.getData("INVItemSelected");
                if (menuItem == _utiliser)
                {
                    Console.WriteLine("utiliser");
                    _itemstack.item.use(client);
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
                else if (menuItem == _jeter)
                {
                    Console.WriteLine("jeter");
                    Players.getPlayerByClient(client).inventory.delete(_itemstack.item, Convert.ToInt32(data["ID_jeter"]["Value"]));
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
                else if (menuItem == _donner)
                {
                    Console.WriteLine("Donner");
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
                else if (menuItem == _coffreveh)
                {
                    Console.WriteLine("Coffre");
                    VehicleHandler vehicle = Vehicles.getVehicleByVehicle(VehicleHandler.getNearestVehicle(client));
                    if (vehicle != null && !vehicle.vehicle.locked)
                    {
                        PlayerHandler player = Players.getPlayerByClient(client);
                        player.inventory.delete(player.client, _itemstack.item, Convert.ToInt32(data["ID_voiture"]["Value"]));
                        vehicle.inventory.add(player.client, _itemstack.item, Convert.ToInt32(data["ID_voiture"]["Value"]));
                    }
                    else if (!vehicle.vehicle.locked)
                        API.shared.sendNotificationToPlayer(client, "Le véhicule est fermé.", 6);
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                }
                OpenMenuInventory(client);
            }
            catch
            {
                //404 error not found
            }
        }
    }
}
