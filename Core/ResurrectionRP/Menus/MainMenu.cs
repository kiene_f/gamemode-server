﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Elements;
using Enums;
using MenuManagement;
using GrandTheftMultiplayer.Server.Constant;

namespace ResurrectionRP
{
    class MainMenu : Script
    {
        private Menu menu;
        private MenuItem _inventairemenu;
        private MenuItem _animationmenu;
        private MenuItem _papiersmenu;
        private MenuItem _walkstylemenu;
        private MenuItem _emsmenu;
        private MenuItem _lspdmenu;

        public MainMenu()
        {
            API.onClientEventTrigger += OpenInventaireMenu;
        }

        private void OpenInventaireMenu(Client player, string eventName, object[] arguments)
        {
            if (eventName != Events.OpenMainMenu) return;

            menu = new Menu("ID_MainMenu", Players.getPlayerByClient(player).name, "Choisissez une option:", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
            menu.BannerColor = new Color(0, 255, 255, 64);
            menu.Callback = MainMenuManager;
            menu.BackCloseMenu = true;

            _inventairemenu = new MenuItem("Inventaire", "", "ID_Inventaire");
            _inventairemenu.ExecuteCallback = true;
            menu.Add(_inventairemenu);

            _animationmenu = new MenuItem("Animations", "", "ID_Animations");
            _animationmenu.ExecuteCallback = true;
            menu.Add(_animationmenu);

            _walkstylemenu = new MenuItem("Styles de Marche", "", "ID_WalkingStyles");
            _walkstylemenu.ExecuteCallback = true;
            menu.Add(_walkstylemenu);

            _papiersmenu = new MenuItem("Montrer vos papiers", "", "ID_Papiers");
            _papiersmenu.ExecuteCallback = true;
            menu.Add(_papiersmenu);

            if (Players.getPlayerByClient(player).getRangFaction(FactionType.EMS) >= 1)
            {
                _emsmenu = new MenuItem("EMS Menu");
                _emsmenu.ExecuteCallback = true;
                menu.Add(_emsmenu);
            }

            if (Players.getPlayerByClient(player).getRangFaction(FactionType.LSPD) >= 1)
            {
                _lspdmenu = new MenuItem("LSPD Menu");
                _lspdmenu.ExecuteCallback = true;
                menu.Add(_lspdmenu);
            }

            MenuManager.OpenMenu(player, menu);
        }

        private void MainMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == _inventairemenu)
            {
                client.triggerEvent(Events.MenuManager_CloseMenu);
                InventaireMenu.OpenMenuInventory(client);
            }
            else if (menuItem == _animationmenu)
            {

            }
            else if (menuItem == _walkstylemenu)
            {
                client.triggerEvent(Events.MenuManager_CloseMenu);
                client.triggerEvent("ReceiveWalkingStyles", API.toJson(WalkingStyleMenu.WalkingStyles.Select(w => w.Name)));

            }
            else if (menuItem == _papiersmenu)
            {
                List<PlayerHandler>players = Players.getNearestPlayer(client, 5f);
                foreach(PlayerHandler player in players)
                    player.client.sendNotification("", Players.getPlayerByClient(client).name);
            }
            #region EMSMENU
            else if (menuItem == _emsmenu)
            {
                menu = new Menu("ID_MainMenu","EMS Menu", "", 0, 0, Menu.MenuAnchor.MiddleRight, false, true, true);
                menu.BannerColor = new Color(0, 255, 255, 64);

                MenuItem soigner = new MenuItem("Soigner un patient");
                soigner.ExecuteCallback = true;
                menu.Add(soigner);

                MenuItem reanimer = new MenuItem("Réanimer un patient");
                reanimer.ExecuteCallback = true;
                menu.Add(reanimer);

                menu.Callback += ((_client, _menu, _menuitem, _itemindex, _forced, _data) =>
                {
                    if (_menuitem == soigner)
                    {

                    }
                    else if(_menuitem == reanimer)
                    {

                    }
                });

                MenuManager.OpenMenu(client, menu);
                
            }
            #endregion

        }
    }
}
