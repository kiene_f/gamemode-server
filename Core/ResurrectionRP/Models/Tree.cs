﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using System.Collections.Generic;

namespace ResurrectionRP
{
    public enum TreeHeight
    {
        Small = 0,
        Medium = 1,
        Big = 2
    }
    public class Tree
    {
        public Tree(Vector3 pos, Vector3 rot, TreeHeight height)
        {
            Position = pos;
            Rotation = rot;
            RemainingWood = height == TreeHeight.Big ? 20 + API.shared.RandomNumber(-5, 5) : height == TreeHeight.Medium ? 10 + API.shared.RandomNumber(-3, 3) : 5 + API.shared.RandomNumber(-1, 1);
            StrikesRequired = height == TreeHeight.Big ? 5 + API.shared.RandomNumber(-2, 2) : height == TreeHeight.Medium ? 6 + API.shared.RandomNumber(-1, 1) : 3 + API.shared.RandomNumber(-1, 1);
            CurrentStrikes = 0;
            SpawnedLogs = new List<Object>();
            TreeHandle = API.shared.createObject(-1279773008, new Vector3(pos.X,pos.Y,pos.Z -1f), Rotation);
        }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public int RemainingWood { get; set; }
        public int StrikesRequired { get; set; }
        public int CurrentStrikes { get; set; }
        public Object TreeHandle { get; }
        public void delete()
        {
            API.shared.deleteEntity(TreeHandle);
        }
        public List<Object> SpawnedLogs { get; set; }

    }
}
