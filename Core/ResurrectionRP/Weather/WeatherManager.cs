﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class WeatherManager : Script
    {
        private bool _WinterMod;

        public WeatherManager()
        {
            //API.onResourceStart += OnResourceStart;
            API.onPlayerFinishedDownload += onPlayerFinishedDownload;

        }

        private void onPlayerFinishedDownload(Client player)
        {
            if (_WinterMod)
            {
                player.triggerEvent(Events.merrychristmas);
            }
                
        }

        private void OnResourceStart()
        {
            API.consoleOutput("[SERVER] Initialisation de la météo!");
            _WinterMod = API.getSetting<bool>("WinterMod");
            if (_WinterMod)
                API.consoleOutput("[Weather] WinterMod activated!");

            Random rnd = new Random();
            Task.Run(async () =>
            {
                int actualweather = 0;
                int oldweather = 0;
                int _sleep = 1800000; //18minutes
                while (true)
                {
                    oldweather = actualweather;
                    actualweather = GetRandomWeather();

                    while ((oldweather == actualweather || actualweather == 9 || actualweather == 7 || actualweather == 6)) // check que la météo ne soit pas la même que la précedente
                    {
                        actualweather = GetRandomWeather();
                    }

                    if (!_WinterMod)
                    {
                        while ((actualweather >= 10 && actualweather <= 13)) // n'est pas égal à la neige
                        {
                            actualweather = GetRandomWeather();
                        }

                        if (actualweather >= 5 && actualweather <= 8) // Pluie
                        {
                            _sleep = rnd.Next(300000, 600000); // 5 à 10 minutes
                        }
                        else if (actualweather == 7) // Tempete 
                        {
                            _sleep = 300000; // 5 minutes
                        }
                        else
                        {
                            _sleep = rnd.Next(900000, 2700000); // 15 à 45 minutes
                        }

                    }
                    else
                    {
                        while (actualweather <= 9)
                        {
                            actualweather = GetRandomWeather();
                        }
                        _sleep = 1800000; // 30 minutes
                    }

                    Console.WriteLine($"[METEO] {((Weather)actualweather).ToString()} pendant {TimeSpan.FromMilliseconds(_sleep).Minutes} minutes.   {actualweather}");

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.125f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.25f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.375f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.5f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.625f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.75f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 0.875f);
                    await Task.Delay(_sleep / 8);

                    API.sendNativeToAllPlayers(Hash._SET_WEATHER_TYPE_TRANSITION, API.getHashKey(((Weather)oldweather).ToString()), API.getHashKey(((Weather)actualweather).ToString()), 1f);
                    await Task.Delay(_sleep / 8);

                }
            });
        }


        private static int GetRandomWeather()
        {
            Random rnd = new Random();
            return rnd.Next(0, 12);
        }
    }
}
