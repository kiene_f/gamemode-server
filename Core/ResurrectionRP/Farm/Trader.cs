﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Trader : Script
    {
        protected string _name;
        protected int _blipSprite;
        protected BlipColor _blipColor;
        protected Vector3 _position;
        private Vector3 _rotation;
        protected int _itembuy;
        private int _itemPrice;
        private PedHash _pedhash;

        public Trader() { }

        public static async Task startTraid(Client sender, NPC npc)
        {
            PlayerHandler player = Players.getPlayerByClient(sender);
            if (player.isOnProgress || sender.isInVehicle) return;
            Item _itemBuy = Inventory.itemByID(npc.getData("ItemBuy"));
            int _itemPrice = npc.getData("ItemPrice");

            if (!player.inventory.checkItem(_itemBuy))
            {
                sender.sendSubTitle("~r~ERREUR ~s~Vous n'avez rien à vendre", 5000);
                return;
            }
            Players.getPlayerByClient(sender).isOnProgress = true;
            player.client.sendSubTitle($"Vous commencez à vendre vos ~r~{_itemBuy.name}(s)", 5000);
            await Task.Run(async () =>
            {
                await Task.Delay(1000 * player.inventory.countItem(_itemBuy));
                int itemcount = player.inventory.countItem(_itemBuy);
                player.inventory.delete(_itemBuy,itemcount);
                sender.sendSubTitle($"Vente terminée: Vous avez vendu ~r~ {itemcount} {_itemBuy.name}(s)", 60000);
                player.money = player.money + (_itemPrice * itemcount);
                player.isOnProgress = false;
            });
            await player.updatePlayerInfo();
            await Task.FromResult(0);
        }

        public Trader(string name, int blipSprite, BlipColor blipcolor, Vector3 position, Vector3 rotation, int itembuy, PedHash pedhash ,int itemPrice)
        {
            _name = name;
            _blipSprite = blipSprite;
            _blipColor = blipcolor;
            _position = position;
            _rotation = rotation;
            _itembuy = itembuy;
            _itemPrice = itemPrice;
            _pedhash = pedhash;

            Blip blip = API.createBlip(_position);
            blip.name = _name;
            blip.sprite = _blipSprite;
            blip.color = (int)_blipColor;
            blip.shortRange = true;

            NPC npc = new NPC(_pedhash, "", _position, (int)_rotation.Z, 0);
            npc.setData("Interaction", "Trader");
            npc.setData("ItemBuy", _itembuy);
            npc.setData("ItemPrice", _itemPrice);
        }
    }
}
