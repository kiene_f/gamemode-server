﻿using Enums;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared.Gta.Blip;

namespace ResurrectionRP
{
    class Vin : Script
    {
        public Vin()
        {
            string _processname = "Distillerie Agricole";
            string _tradername = "Vin et Spiritueux";

            int _blipprocessSprite = 499;
            int _bliptraderSprite = 500;

            Vector3 _traitementposition = new Vector3(2569.418f, 4668.503f, 34.07677f);
            Vector3 _traitementrotation = new Vector3(0f, 0f, 131.3166f);

            Vector3 _traiderposition = new Vector3(-1257.41f, -1149.773f, 7.603687f);
            Vector3 _traiderrotation = new Vector3(0f, 0f, -78.78485f);

            Enums.BlipColor _color = Enums.BlipColor.LightGreen2;

            int _itembrute = Enums.Items.Raisin;
            int _itembrute2 = Enums.Items.Bouteille;
            int _itemprocess = Enums.Items.Vin;
            int price = 1000;

            API.onResourceStart += (() => {
                new DoubleProcess(_processname, _blipprocessSprite, _color, _traitementposition, _traitementrotation, _itembrute, _itembrute2, _itemprocess, PedHash.Floyd);
                new Trader(_tradername, _bliptraderSprite, _color, _traiderposition, _traiderrotation, _itemprocess, PedHash.Busboy01SMY, price);
            });
        }
    }
}
