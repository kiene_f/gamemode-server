﻿using Enums;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared.Gta.Blip;

namespace ResurrectionRP
{
    class Lumber : Script
    {
        public Lumber()
        {
            string _processname = "Scierie";
            string _tradername = "Exportation de Planche";
            int _blipprocessSprite = 499;
            int _bliptraderSprite = 500;
            Vector3 _farmposition = new Vector3(-1791f, 2146f, 30f);
            Vector3 _traitementposition = new Vector3(-168.802, -2659.57, 6.00103);
            Vector3 _traitementrotation = new Vector3(0, 0, -93.61317);
            Vector3 _traiderposition = new Vector3(-552.5037, 5325.934, 73.59962);
            Vector3 _traiderrotation = new Vector3(0, 0, 141.995);
            Enums.BlipColor _color = Enums.BlipColor.LightBrown;

            int _itembrute = Enums.Items.RodinDeBois;
            int _itemprocess = Enums.Items.PlancheBois;
            int price = 3000;

            API.onResourceStart += (() => {
                List<Location> spawns = new List<Location>();
                spawns.Add(Location.fromVector3(new Vector3(-577.4836f, 5328.449f, 70.25906f), new Vector3(0, 0, -20.12781f)));
                spawns.Add(Location.fromVector3(new Vector3(-598.9672f, 5294.208f, 70.45018f), new Vector3(-0.4795532f, 0.01045155f, -174.2935f)));

                Lumberman lumberman = new Lumberman("Scierie de Paleto", new Vector3(-567.9764, 5253.088, 70.48751), spawns, new Vector3(68.18311, 2965.881, 52.02333));
                new Processing(_processname, _blipprocessSprite, _color, _traitementposition, _traitementrotation, _itembrute, _itemprocess, PedHash.Floyd);
                new Trader(_tradername, _bliptraderSprite, _color, _traiderposition, _traiderrotation, _itemprocess, PedHash.Busboy01SMY, price);
            });


        }
    }
}
