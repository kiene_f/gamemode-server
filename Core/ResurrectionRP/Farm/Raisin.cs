﻿using Enums;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared.Gta.Blip;

namespace ResurrectionRP
{
    class Raisin : Script
    {
        public Raisin()
        {
            string _farmname = "Vignes";
            string _processname = "Traitement du Raisin";
            string _tradername = "Revendeur de Raisin";
            int _blipfarmSprite = 85;
            int _blipprocessSprite = 499;
            int _bliptraderSprite = 500;
            Vector3 _farmposition = new Vector3(-1791f, 2146f, 30f);
            Vector3 _traitementposition = new Vector3(1545.714f, 2175.634f, 78.80173f);
            Vector3 _traitementrotation = new Vector3(0f, 0f, 66.47958f);
            Vector3 _traiderposition = new Vector3(-1341.587, -1078.23, 6.938033);
            Vector3 _traiderrotation = new Vector3(0, 0, -155.4158f);
            float _range = 50f;
            Enums.BlipColor _color = Enums.BlipColor.LightGreen2;

            int _itembrute = Enums.Items.GrappeRaisin;
            int _itemprocess = Enums.Items.Raisin;
            int price = 1000;

            API.onResourceStart += (() => {
                new Farm(_farmname, _blipfarmSprite, _color, _farmposition, _range, _itembrute);
                new Processing(_processname, _blipprocessSprite, _color, _traitementposition, _traitementrotation, _itembrute, _itemprocess, PedHash.Floyd);
                new Trader(_tradername,_bliptraderSprite, _color, _traiderposition, _traiderrotation, _itemprocess, PedHash.Busboy01SMY, price);
            });
            

        }
    }
}
