﻿using Enums;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared.Gta.Blip;

namespace ResurrectionRP
{
    class Cuivre : Script
    {
        public Cuivre()
        {
            string _farmname = "Mine de cuivre";
            string _processname = "Traitement de cuivre";
            string _tradername = "Revendeur de cuivre";
            int _blipfarmSprite = 85;
            int _blipprocessSprite = 499;
            int _bliptraderSprite = 500;
            Vector3 _farmposition = new Vector3(2684.724f, 2866.268f, 33f);
            Vector3 _traitementposition = new Vector3(741.797f, -972.2991f, 24.50507f);
            Vector3 _traitementrotation = new Vector3(0f, 0f, -94.82858f);
            Vector3 _traiderposition = new Vector3(606, -3073.102, 6.06);
            Vector3 _traiderrotation = new Vector3(0, 0, -11.52882f);
            float _range = 50f;
            Enums.BlipColor _color = Enums.BlipColor.Complexion;

            int _itembrute = Enums.Items.MineraiCuivre;
            int _itemprocess = Enums.Items.Cuivre;
            int price = 1000;

            API.onResourceStart += (() => {
                new Farm(_farmname, _blipfarmSprite, _color, _farmposition, _range, _itembrute);
                new Processing(_processname, _blipprocessSprite, _color, _traitementposition, _traitementrotation, _itembrute, _itemprocess, PedHash.Floyd);
                new Trader(_tradername, _bliptraderSprite, _color, _traiderposition, _traiderrotation, _itemprocess, PedHash.Busboy01SMY, price);
            });


        }
    }
}
