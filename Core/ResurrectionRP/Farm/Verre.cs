﻿using Enums;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared.Gta.Blip;

namespace ResurrectionRP
{
    class Verre : Script
    {
        public Verre()
        {
            string _farmname = "Carrière de Sable";
            string _processname = "Souffleur de Verre";
            string _tradername = "Revendeur de Bouteille";
            int _blipfarmSprite = 85;
            int _blipprocessSprite = 499;
            int _bliptraderSprite = 500;
            Vector3 _farmposition = new Vector3(232.136f, 7009.544f, 1.883199f);
            Vector3 _traitementposition = new Vector3(973.6821f, -1942.985f, 31.06884f);
            Vector3 _traitementrotation = new Vector3(0f, 0f, -178f);
            Vector3 _traiderposition = new Vector3(575.4752, 137.3664, 99.47485);
            Vector3 _traiderrotation = new Vector3(0, 0, -179);
            float _range = 50f;
            Enums.BlipColor _color = Enums.BlipColor.GulfStream;

            int _itembrute = Enums.Items.MineraiCuivre;
            int _itemprocess = Enums.Items.Cuivre;
            int price = 1000;

            API.onResourceStart += (() => {
                new Farm(_farmname, _blipfarmSprite, _color, _farmposition, _range, _itembrute);
                new Processing(_processname, _blipprocessSprite, _color, _traitementposition, _traitementrotation, _itembrute, _itemprocess, PedHash.Floyd);
                new Trader(_tradername, _bliptraderSprite, _color, _traiderposition, _traiderrotation, _itemprocess, PedHash.Busboy01SMY, price);
            });


        }
    }
}
