﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Farm : Script
    {

        protected string _name;
        protected int _blipSprite;
        protected BlipColor _blipcolor;
        protected Vector3 _position;
        protected float _range;
        protected CylinderColShape _colshape;
        protected int _itembrute;
        private Random _random = new Random();

        public Farm() {
        }


        public async static Task startFarming(Client sender)
        {
            PlayerHandler player = Players.getPlayerByClient(sender);
            if (player.isOnProgress || sender.isInVehicle || !player.client.hasData("FarmZone")) return;
            CylinderColShape colshape = player.client.getData("FarmZone");
            if (!colshape.containsEntity(sender)) return;
            if (!player.client.hasData("ItemFarmZone")) return;
            if (player.inventory.isFull())
            {
                sender.sendSubTitle("Vous inventaire est plein!", 60000);
                return;
            }
            Players.getPlayerByClient(sender).isOnProgress = true;
            Item item = Inventory.itemByID(player.client.getData("ItemFarmZone"));
            sender.sendSubTitle("Vous commencez à ramasser de(s) ~r~" + item.name, 5000);
            await Task.Run(async () =>
             {
                 bool _exit = false;
                 for (int i = 1; !player.inventory.isFull() && !_exit; i++)
                 {
                     await Task.Delay(10000);
                     if (sender.isInVehicle)
                     {
                         sender.sendSubTitle($"~r~Récolte interrompue: ~s~Vous ne pouvez pas récolter depuis le véhicule.", 5000);
                         _exit = true;
                     }
                     if (!colshape.containsEntity(sender))
                     {
                         sender.sendSubTitle($"~r~Récolte interrompue: ~s~Vous devez rester dans la zone.", 5000);
                         _exit = true;
                     }
                     if (_exit)
                     {
                         player.isOnProgress = false;
                         return;
                     }

                     sender.sendSubTitle($"~r~Récolte en cours: ~s~Vous venez de ramasser 1 {item.name}(s)", 5000);
                     player.inventory.add(item, 1);
                     if (player.inventory.isFull())
                         sender.sendSubTitle($"Récolte terminée: Vous avez ramassé ~r~ {i} {item.name}", 60000);
                 }
                 await player.updatePlayerInfo();
                 player.isOnProgress = false;
             });

            await Task.FromResult(0);
        }

        public Farm(string name, int blipSprite, BlipColor blipcolor, Vector3 position, float range, int itembrute)
        {
            _name = name;
            _blipSprite = blipSprite;
            _blipcolor = blipcolor;
            _position = position;
            _range = range;
            _itembrute = itembrute;

            Blip blip = API.createBlip(_position);
            blip.name = _name;
            blip.sprite = _blipSprite;
            blip.color = (int)_blipcolor;
            blip.shortRange = true;

            _colshape = API.createCylinderColShape(_position, _range, 100f);
            _colshape.onEntityEnterColShape += ((colshape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                if (client == null) return;
                client.setData("FarmZone", colshape);
                client.setData("ItemFarmZone", _itembrute);
            });

            _colshape.onEntityExitColShape += ((colshape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                if (client == null) return;
                client.resetData("FarmZone");
                client.resetData("ItemFarmZone");
            });
        }
    }
}
