﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Server.Constant;

namespace ResurrectionRP
{
    class Processing : Script
    {
        private string _name;
        private int _blipSprite;
        private BlipColor _blipcolor;
        private Vector3 _position;
        private int _itembrute;
        private CylinderColShape _colshape;
        private int _itemprocess;
        private Vector3 _rotation;
        private PedHash _pedhash;

        public Processing(){
        }

        public async static Task startProcessing(Client sender, NPC npc)
        {
            PlayerHandler player = Players.getPlayerByClient(sender);
            if (player.isOnProgress || sender.isInVehicle) return;
            if (!player.client.hasData("ProcessingZone")) return;   
            CylinderColShape colshape = player.client.getData("ProcessingZone");
            if (!colshape.containsEntity(sender)) return;
            Item _itemNoTraite = Inventory.itemByID(npc.getData("ItemNoTraite"));
            Item _itemTraite = Inventory.itemByID(npc.getData("ItemTraite"));

            if (!player.inventory.checkItem(_itemNoTraite))
            {
                sender.sendSubTitle("~r~ERREUR ~s~Vous n'avez rien à traiter", 5000);
                return;
            }
            Players.getPlayerByClient(sender).isOnProgress = true;
            player.client.triggerEvent(Events.display_subtitle, $"Vous commencez à traiter vos ~r~{_itemNoTraite.name}(s)", 5000);
            await Task.Run(async () =>
            {
                bool _exit = false;
                for (int i = 1; player.inventory.checkItem(_itemNoTraite) && !_exit; i++)
                {
                    await Task.Delay(10000);
                    if (sender.isInVehicle)
                    {
                        sender.sendSubTitle($"~r~Traitement interrompue: ~s~Vous ne pouvez pas traiter depuis le véhicule.", 5000);
                        _exit = true;
                    }
                    if (!colshape.containsEntity(sender))
                    {
                        sender.sendSubTitle($"~r~Traitement interrompue: ~s~Vous devez rester dans la zone.", 5000);
                        _exit = true;
                    }
                    if (_exit)
                    {
                        player.isOnProgress = false;
                        return;
                    }
                    sender.sendSubTitle($"~r~Traitement en cours: ~s~+1 {_itemTraite.name}", 5000);
                    player.inventory.delete(_itemNoTraite, 1);
                    player.inventory.add(_itemTraite, 1);
                    if (!player.inventory.checkItem(_itemNoTraite))
                        sender.sendSubTitle($"Traitement terminée: Vous avez traité ~r~ {i} {_itemTraite.name}(s)", 60000);
  
                }
                await player.updatePlayerInfo();
                player.isOnProgress = false;
            });
            await Task.FromResult(0);
        }

        public Processing(string name, int blipSprite, BlipColor blipcolor, Vector3 position, Vector3 rotation, int itembrute, int itemprocess, PedHash pedhash)
        {
            _name = name;
            _blipSprite = blipSprite;
            _blipcolor = blipcolor;
            _position = position;
            _rotation = rotation;
            _itembrute = itembrute;
            _itemprocess = itemprocess;
            _pedhash = pedhash;

            Blip blip = API.createBlip(_position);
            blip.name = _name;
            blip.sprite = _blipSprite;
            blip.color = (int)_blipcolor;
            blip.shortRange = true;

            NPC npc = new NPC(_pedhash, "", _position, (int)_rotation.Z, 0);
            npc.setData("Interaction", "Traitement");
            npc.setData("ItemNoTraite", _itembrute);
            npc.setData("ItemTraite", _itemprocess);

            _colshape = API.createCylinderColShape(_position, 30f, 30f);
            _colshape.onEntityEnterColShape += ((colshape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                if (client == null) return;
                client.setData("ProcessingZone", colshape);
            });

            _colshape.onEntityExitColShape += ((colshape, entity) =>
            {
                Client client = API.shared.getPlayerFromHandle(entity);
                if (client == null) return;
                client.resetData("ProcessingZone");
            });
        }
    }
}
