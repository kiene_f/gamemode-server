﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace ResurrectionRP.Items
{
    class FishingRod : Item
    {
        private bool isFishing;
        private Random _random = new Random();

        public FishingRod(int id, string name, string description, int weight = 0, bool isGiven = false, bool isUsable = true, bool isStackable = false, int drink = 0, int food = 0, int life = 0) : base(id, name, description, weight, isGiven, isUsable, isStackable, drink, food, life)
        {
            isFishing = false;
        }

        public override void use(Client client)
        {
            if (!isUsable) return;
            PlayerHandler player = Players.getPlayerByClient(client);
            if (player.job == null || player.job.GetType() != typeof(ResurrectionRP.Player.Job.Fisher) || player.job.canFarm == false)
                player.client.sendNotification("Server", "Vous ne pouvez pas l'utiliser ici.");
            else
            {
                if (isFishing == false)
                {
                    API.shared.playPlayerScenario(player.client, "WORLD_HUMAN_STAND_FISHING");
                    startFishingAsync(player);
                    isFishing = true;
                } else
                    isFishing = false;

            }
        }

        private async void startFishingAsync(PlayerHandler player)
        {
            await Task.Run(async () =>
            {
                for (int i = 1; isFishing && !player.inventory.isFull(); i++)
                {
                    await Task.Delay(_random.Next(5, 15) * 1000);
                    player.inventory.add(Inventory.itemByID(Enums.Items.Sardine), 1);
                    player.client.sendSubTitle($"Vous venez de pêcher ~r~ {i} sardine(s)", 5000);
                }
                API.shared.stopPlayerAnimation(player.client);
                isFishing = false;
                await Task.FromResult(0);
            });
        }
    }
}
