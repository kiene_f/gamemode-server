﻿using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Items
{
    class GasJerrycan : Item
    {
        public GasJerrycan(int id, string name, string description, int weight = 0, bool isGiven = false, bool isUsable = false, bool isStackable = true, int drink = 0, int food = 0, int life = 0) : base(id, name, description, weight, isGiven, isUsable, isStackable, drink, food, life)
        {
        }

        public override void use(Client client)
        {
            VehicleHandler _vehicle = Vehicles.getVehicleByVehicle(VehicleHandler.getNearestVehicle(client,5f));
            if (_vehicle != null)
            {
                _vehicle.fuel += 20;
                client.sendNotificationSuccess("Vous avez remis de l'essence dans le véhicule");
            }
            else
                client.giveWeapon(WeaponHash.BZGas, 200, true,true);

            ItemStack _itemstack = Players.getPlayerByClient(client).inventory.inventory[id];
            Players.getPlayerByClient(client).inventory.delete(_itemstack.item, 1);

        }
    }
}
