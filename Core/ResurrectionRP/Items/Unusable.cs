﻿using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Unusable : Item
    {
        public Unusable(int id, string name, string description, int weight = 0, bool isGiven = false, bool isUsable = false, bool isStackable = true, int drink = 0, int food = 0, int life = 0) : base(id, name, description, weight, isGiven, isUsable, isStackable, drink, food, life)
        {
            
        }

        public override void use(Client client)
        {
            client.sendNotification("", "Item inutilisable");
        }
    }
}
