﻿using Enums;
using GrandTheftMultiplayer.Server.Elements;
using System.Collections.Generic;

namespace ResurrectionRP
{
    public class Item
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int weight { get; set; }
        public bool isGiven { get; set; }
        public bool isUsable { get; set; }
        public bool isStackable { get; set; }
        public int drink { get; set; }
        public int food { get; set; }
        public int life { get; set; }

        public Item(int id, string name, string description, int weight = 0, bool isGiven = false, bool isUsable = false, bool isStackable = true, 
            int drink = 0, int food = 0, int life = 0)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.weight = weight;
            this.isGiven = isGiven;
            this.isUsable = isUsable;
            this.isStackable = isStackable;
            this.drink = drink;
            this.life = life;
        }

        public virtual void use(Client c)
        {
            if (!isUsable) return;
        }
    }
}