﻿using GrandTheftMultiplayer.Server.Elements;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Eat : Item
    {
        public Eat(int id, string name, string description, int weight = 0, bool isGiven = false, bool isUsable = false, bool isStackable = true, int drink = 0, int food = 0, int life = 0) : base(id, name, description, weight, isGiven, isUsable, isStackable, drink, food, life)
        {
            Food = food;
        }

        public int Food { get; private set; }

        public override void use(Client client)
        {
            ItemStack _itemstack = Players.getPlayerByClient(client).inventory.inventory[id];
            Players.getPlayerByClient(client).inventory.delete(_itemstack.item, 1);

        }
    }
}
