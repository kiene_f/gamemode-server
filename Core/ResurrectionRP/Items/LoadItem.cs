﻿using GrandTheftMultiplayer.Server.API;
using ResurrectionRP.Items;
using System.Collections.Generic;
using ItemID = Enums.Items;

namespace ResurrectionRP
{
    class LoadItem : Script
    {
        public static List<Item> ItemsList = new List<Item>();
        public LoadItem()
        {
            API.onResourceStart += (() =>
            {
                ItemsList = new List<Item>()
                {
                    new Eat(ItemID.Fromage, "Fromage de chèvre", "Fromage de chèvre bien odorant", 1, true, true,true,0,30,0),
                    new Eat(ItemID.Pain, "Pain", "Du pain complet au levain bio", 1, true, true, true, 0, 30, 0),
                    new FishingRod(ItemID.Canne, "Canne à pêche", "La canne à pêche est l'outil principal pour la pêche"),
                    new Eat(ItemID.Sardine, "Sardine", "La sardine est une espèce de poisson de la famille des Clupeidae", 1, food:1),
                    new Unusable(ItemID.GrappeRaisin, "Grappe de raisin", "Une grappe de raisin vert", 2),
                    new Eat(ItemID.Raisin, "Raisin", "Du raisin vert comestible", 1, food: 2),
                    new Unusable(ItemID.MineraiCuivre, "Minerai de Cuivre", "Un morceau de roche", 2),
                    new Unusable(ItemID.Cuivre, "Bobine de Cuivre", "Une bobine de Cuivre.", 1),
                    new Unusable(ItemID.Sable, "Sable", "Du sable qui sent la pisse de chat.", 2),
                    new Unusable(ItemID.Bouteille, "Bouteille en Verre", "Une bouteille en verre vide.", 1),
                    new Eat(ItemID.Vin, "Une Bouteille de Vin", "Une bouteille de Vin.", 1 , drink:20),
                    new Unusable(ItemID.RodinDeBois, "Rodin de bois", "Un rodin de bois humide.", 3),
                    new GasJerrycan(ItemID.Jerrycan,"Jerrican d'essence","Un jerrycan d'essence, utilisé le près d'un véhicule pour le rationner.",3)
                };
            });
        }

    }
}
