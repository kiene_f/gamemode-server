using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TeamSpeak3QueryApi.Net;

namespace LipSync
{
    public class LipSync : Script
    {
        private static string TeamspeakQueryAddress { get; set; }
        private static short TeamspeakQueryPort { get; set; }
        private static string TeamspeakPort { get; set; }
        private static string TeamspeakLogin { get; set; }
        private static string TeamspeakPassword { get; set; }
        private static string TeamspeakChannel { get; set; }

        public LipSync()
        {
            API.onResourceStart += API_onResourceStart;
        }

        private void API_onResourceStart()
        {
            TeamspeakQueryAddress = API.getSetting<string>("teamspeak_query_address");
            TeamspeakQueryPort = API.getSetting<short>("teamspeak_query_port");
            TeamspeakPort = API.getSetting<string>("teamspeak_port");
            TeamspeakLogin = API.getSetting<string>("teamspeak_login");
            TeamspeakPassword = API.getSetting<string>("teamspeak_password");
            TeamspeakChannel = API.getSetting<string>("teamspeak_channel");

            while (true)
            {
                API.consoleOutput("(Re-)Connected to Teamspeak3.");
                try
                {
                    CheckSpeakingClients().Wait();
                }
                catch(QueryProtocolException ex) { Console.Write(ex.ToString()); }
                
            }
        }
        
        private static async Task CheckSpeakingClients()
        {
            var qc = new QueryClient(TeamspeakQueryAddress, TeamspeakQueryPort);
            await qc.Connect();
            await qc.Send("login", new Parameter("client_login_name", TeamspeakLogin), new Parameter("client_login_password", TeamspeakPassword));
            await qc.Send("use", new Parameter("port", TeamspeakPort));

            while (qc.IsConnected)
            {
                List<String> clients = new List<String>();

                var channel = await qc.Send("channelfind", new Parameter("pattern", TeamspeakChannel));
                String channelId = channel.First()["cid"].ToString();

                var clientlist = await qc.Send("clientlist", new Parameter("-voice", ""));
                foreach (var client in clientlist)
                {
                    if (client["client_flag_talking"].ToString() == "1" && client["cid"].ToString() == channelId)
                    {
                        clients.Add(ReplaceStr(client["client_nickname"].ToString()));
                    }
                }

                List<Client> players = API.shared.getAllPlayers();

                foreach (string client in clients)
                {
                    var playersTmp = players.Where(p => p.name == client);
                    if (playersTmp.Count() == 0)
                    {
                        continue;
                    }
                    
                    var player = playersTmp.First();
                    if (!(API.shared.hasEntityData(player, "IS_SPEAKING") && API.shared.getEntityData(player, "IS_SPEAKING")))
                    {
                        API.shared.sendNativeToPlayersInRange(player.position, 50f, Hash.TASK_PLAY_ANIM, player.handle, "mp_facial", "mic_chatter", 8f, -4f, -1, 33, 0.0f, false, false, false);
                        API.shared.setEntityData(player, "IS_SPEAKING", true);
                    }
                }

                foreach (var player in players)
                {
                    var client = clients.Where(ic => ic.Contains(player.name));
                    if (client.Count() > 0)
                    {
                        continue;
                    }
                    
                    if (API.shared.hasEntityData(player, "IS_SPEAKING") && API.shared.getEntityData(player, "IS_SPEAKING"))
                    {
                        API.shared.sendNativeToPlayersInRange(player.position, 50f, Hash.TASK_PLAY_ANIM, player.handle, "mp_facial", "mic_chatter1", 8f, -4f, -1, 33, 0.0f, false, false, false);
                        API.shared.setEntityData(player, "IS_SPEAKING", false);
                    }
                }
                
                Thread.Sleep(100);
            }
        }

        public static string ReplaceStr(string str)
        {
            str = str.Replace("\\\\", "\\");
            str = str.Replace("\\/", "/");
            str = str.Replace("\\s", " ");
            str = str.Replace("\\p", "|");
            str = str.Replace("\\a", "\a");
            str = str.Replace("\\b", "\b");
            str = str.Replace("\\f", "\f");
            str = str.Replace("\\n", "\n");
            str = str.Replace("\\r", "\r");
            str = str.Replace("\\t", "\t");
            str = str.Replace("\\v", "\v");
            return str;
        }
    }
}
