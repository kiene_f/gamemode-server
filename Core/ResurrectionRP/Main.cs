﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public class Main : Script
    {
        public static string _serverName = "~r~[FR] ∑ ~y~ResurrectionRP~r~";
        public static string _changresourceName = "resurrection";  // Doit porter le même nom que le dossier qu'il le contient

        public Main()
        {
            Console.WriteLine("Starting ResurrectionRP resource...");
            API.onResourceStart += onResourceStart;
            API.onResourceStop += onResourceStop;
        }

        private void onResourceStop()
        {
            Console.WriteLine("Ending ResurrectionRP resource...");
        }

        private async void onResourceStart()
        {
            await DiscordBot.startBot();
            ServerNameChanger();
            RessourceNameChanger();
            IPLManager();
        }

        private void ServerNameChanger()
        {
            API.setServerName(_serverName);
        }

        private void RessourceNameChanger()
        {
            API.setGamemodeName(_changresourceName);
        }

        private void IPLManager()
        {
            API.consoleOutput("Chargements des IPLS en cours ...");
            API.requestIpl("hei_yacht_heist");
            API.requestIpl("hei_yacht_heist_bar");
            API.requestIpl("hei_yacht_heist_bar_lod");
            API.requestIpl("hei_yacht_heist_bedrm");
            API.requestIpl("hei_yacht_heist_bedrm_lod");
            API.requestIpl("hei_yacht_heist_bridge");
            API.requestIpl("hei_yacht_heist_bridge_lod");
            API.requestIpl("hei_yacht_heist_distantlights");
            API.requestIpl("hei_yacht_heist_enginrm");
            API.requestIpl("hei_yacht_heist_enginrm_lod");
            API.requestIpl("hei_yacht_heist_lod");
            API.requestIpl("hei_yacht_heist_lodlights");
            API.requestIpl("hei_yacht_heist_lounge");
            API.requestIpl("hei_yacht_heist_lounge_lod");
            API.requestIpl("hei_yacht_heist_slod");

            API.requestIpl("FIBlobbyfake");
            API.requestIpl("DT1_03_Gr_Closed");
            API.requestIpl("v_tunnel_hole");
            API.requestIpl("TrevorsMP");
            API.requestIpl("TrevorsTrailer");
            API.requestIpl("farm");
            API.requestIpl("farmint");
            API.requestIpl("farmint_cap");
            API.requestIpl("farm_props");
            API.requestIpl("CS1_02_cf_offmission");
            API.requestIpl("shutter_open");
            API.requestIpl("shr_int");
            API.requestIpl("ex_dt1_02_office_01a");
            API.removeIpl("facelobby");
            API.removeIpl("v_carshowroom");
            API.removeIpl("shutter_open");
            API.removeIpl("shutter_closed");
            API.removeIpl("shr_int");
            API.removeIpl("csr_inMission");
            API.removeIpl("fakeint");
            API.requestIpl("shr_int");

            //Morgue: 239.75195, -1360.64965, 39.53437
            API.requestIpl("coronertrash");
            API.requestIpl("Coroner_Int_on");
            //Pillbox hospital:
            API.requestIpl("rc12b_default");
            API.consoleOutput("IPL Charger!");
            try
            {
                int door = API.shared.exported.doormanager.registerDoor(100, new Vector3(82.38156, -1390.476, 29.52609));
                API.shared.exported.doormanager.setDoorState(door, false, 0);
                door = API.shared.exported.doormanager.registerDoor(101, new Vector3(82.38156, -1390.752, 29.52609));
                API.shared.exported.doormanager.setDoorState(door, false, 0);
            }
            catch { }

        }

    }
}
