﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ResurrectionRP
{
    public class VehicleHandler : Script
    {
        private PlayerHandler _player;
        public string pid { get; } // SocialClubName
        public Vehicle vehicle { get; }
        public VehicleHash model { get; }
        public Location location { get; }
        public int primaryColor { get; }
        public int secondaryColor { get; }
        public float _fuel { get; }
        private int _fuelMax;
        private bool _locked;
        private int _inventorySize;
        public string plate { get; }
        private bool _engineStatus;
        public Dictionary<int, int> mods { get; }
        public Vector3 neon { get; } // RGB
        public Inventory inventory { get; }
        public bool spawnVeh { get; }

        public float fuel
        {
            set
            {
                if (vehicle != null)
                    API.shared.setVehicleFuelLevel(vehicle, value);
            }
            get =>_fuel;
        }

        public VehicleHandler()
        {

        }

        public VehicleHandler(string socialClubName, VehicleHash model, Vector3 position, Vector3 rotation, int primaryColor = 0, int secondaryColor = 0,
            int fuel = 100, int fuelMax = 100, int inventorySize = 0, string plate = null, bool engineStatus = false, bool locked = true,
            Client client = null, Dictionary<int, int> mods = null, Vector3 neon = null, bool spawnVeh = false, int dimension = 0, Inventory inventory = null)
        {
            _player = Players.getPlayerByClient(client);
            pid = socialClubName;
            this.model = model;
            if (this.model == 0) return;
            location = Location.fromVector3(position, rotation);
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
            this.fuel = fuel;
            _fuelMax = fuelMax;
            _inventorySize = (inventorySize == 0) ? getVehicleInventorySize(model) : inventorySize;
            _locked = locked;
            this.plate = plate ?? generateRandomPlate();
            _engineStatus = engineStatus;
            this.mods = mods;
            this.neon = neon;
            this.spawnVeh = spawnVeh;
            this.inventory = (inventory == null) ? new Inventory(_inventorySize) : inventory;
            vehicle = API.createVehicle(this.model, position, rotation, this.primaryColor, this.secondaryColor, dimension);

            API.shared.setVehicleLocked(vehicle, _locked);
            API.shared.setVehicleEngineStatus(vehicle, _engineStatus);
            API.shared.setVehicleNumberPlate(vehicle, this.plate);

            
            if (this.mods != null && this.mods.Count > 0)
                foreach (KeyValuePair<int, int> mod in this.mods)
                    API.shared.setVehicleMod(vehicle, mod.Key, mod.Value);
            else
                this.mods = new Dictionary<int, int>();


            if (this.neon != null && this.neon != new Vector3())
            {
                API.shared.setVehicleNeonColor(vehicle, (int)this.neon.X, (int)this.neon.Y, (int)this.neon.Z);
                setVehicleNeonState(true);
            }
            Task.Run(async () => {  await save(); });

            Vehicles.vehicles.Add(this);
        }

        private int getVehicleInventorySize(VehicleHash vehicleHash)
        {
            if (vehicleHash.ToString() == null)
            {
                return (0);
            }

            return (Vehicles.getVehicleInventorySize(vehicleHash));
        }

        public void setVehicleNeonState(bool state)
        {
            // left
            API.shared.setVehicleNeonState(vehicle, 0, state);
            // right
            API.shared.setVehicleNeonState(vehicle, 1, state);
            // front
            API.shared.setVehicleNeonState(vehicle, 2, state);
            // back
            API.shared.setVehicleNeonState(vehicle, 3, state);
        }

        public static string generateRandomPlate()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            char[] stringChars = new char[8];
            Random random = new Random();
            string generatedPlate;

            do
            {
                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }
                generatedPlate = new string(stringChars);
            } while (!Vehicles.isPlateUnique(generatedPlate));

            return generatedPlate;
        }

        public static Vehicle getNearestVehicle(Vector3 position, float distance = 3.0f)
        {
            NetHandle vehicle = new NetHandle();
            foreach (NetHandle veh in API.shared.getAllVehicles())
            {
                Vector3 vehPos = API.shared.getEntityPosition(veh);
                float distanceVehicleToPlayer = position.DistanceTo(vehPos);
                if (distanceVehicleToPlayer < distance)
                {
                    distance = distanceVehicleToPlayer;
                    vehicle = veh;
                }
            }
            return (vehicle != new NetHandle() ? API.shared.getEntityFromHandle<Vehicle>(vehicle) : null);
        }

        public static Vehicle getNearestVehicle(Client sender, float distance = 3.0f)
        {
            NetHandle vehicle = new NetHandle();
            foreach (NetHandle veh in API.shared.getAllVehicles())
            {
                Vector3 vehPos = API.shared.getEntityPosition(veh);
                float distanceVehicleToPlayer = sender.position.DistanceTo(vehPos);
                if (distanceVehicleToPlayer < distance)
                {
                    distance = distanceVehicleToPlayer;
                    vehicle = veh;
                }
            }
            return (vehicle != new NetHandle() ? API.shared.getEntityFromHandle<Vehicle>(vehicle) : null);
        }

        public static List<Vehicle> getNearestVehicles(Client sender, float distance = 3.0f)
        {
            NetHandle vehicle = new NetHandle();
            List<Vehicle> vehicles = new List<Vehicle>();
            foreach (NetHandle veh in API.shared.getAllVehicles())
            {
                Vector3 vehPos = API.shared.getEntityPosition(veh);
                float distanceVehicleToPlayer = sender.position.DistanceTo(vehPos);
                if (distanceVehicleToPlayer < distance)
                    vehicles.Add(API.shared.getEntityFromHandle<Vehicle>(vehicle));
            }
            return (vehicles);
        }

        public void delete()
        {
            Vehicles.remove(this);
            vehicle.delete();
        }

        public async Task insert(bool active)
        {
            await Database.insertQuery(string.Format("INSERT INTO vehicles (active, inPound, pid, name, plate, primaryColor, secondaryColor, inventory, fuel, location, mods, neon) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','0','{7}','{8}','{9}','{10}','{11}')",
            Convert.ToInt32(active),
            0,
            pid,
            model,
            plate,
            primaryColor,
            secondaryColor,
            inventory.toJson(),
            fuel,
            Serialize.toJson(location),
            mods != null ? JsonConvert.SerializeObject(mods) : null,
            neon != null ? JsonConvert.SerializeObject(neon) : null
            ));
        }

        public async Task update(bool active)
        {
            await Database.insertQuery(string.Format("UPDATE vehicles SET active='{0}', inPound='{1}', pid='{2}', name='{3}', plate='{4}', primaryColor='{5}', secondaryColor='{6}', inventory='{7}', fuel='{8}', location='{9}', mods='{10}', neon='{11}' WHERE plate='{4}'",
            Convert.ToInt32(active),
            0,
            this.pid,
            this.model,
            this.plate,
            this.primaryColor,
            this.secondaryColor,
            this.inventory.toJson(),
            this.fuel,
            Serialize.toJson(this.location),
            this.mods != null ? JsonConvert.SerializeObject(this.mods) : null,
            this.neon != null ? JsonConvert.SerializeObject(this.neon) : null
            ));
        }

        public async Task save()
        {
            if (spawnVeh == false)
            {
                if (Vehicles.isPlateUnique(plate))
                    await insert(true);
                else
                    await update(true);
            }
        }
    }
}