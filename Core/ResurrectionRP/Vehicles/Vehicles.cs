﻿using GrandTheftMultiplayer.Server.API;
using System.Collections.Generic;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using System.Threading.Tasks;
using System.Data;
using System;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using GrandTheftMultiplayer.Server.Managers;
using Enums;

namespace ResurrectionRP
{
    class Vehicles : Script
    {
        public static List<VehicleHandler> vehicles = new List<VehicleHandler>();

        public Vehicles()
        {
            API.onResourceStart += (async () =>
            {
                await Database.waitMysqlReady();
                await loadAllVehiclesActive();

                API.onClientEventTrigger += OnClientEventTrigger;
                API.onVehicleDeath += OnVehicleDeath;
                API.onPlayerEnterVehicle += OnPlayerEnterVehicle;
                API.onPlayerExitVehicle += OnPlayerExitVehicle;
            });

            API.onResourceStop += (async () => { await saveAllVehiclesActive(); });
        }

        private void OnPlayerEnterVehicle(Client player, NetHandle vehicle, int seat)
        {
            Vehicle veh = API.getEntityFromHandle<Vehicle>(vehicle);

        }

        private void OnClientEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            if (eventName == "EngineStart")
                if (sender.isInVehicle)
                    sender.vehicle.engineStatus = !sender.vehicle.engineStatus;
        }

        private void OnPlayerExitVehicle(Client player, NetHandle vehicle, int seat)
        {
            Vehicle veh = API.getEntityFromHandle<Vehicle>(vehicle);
            Console.WriteLine(veh.numberPlate);
            if (veh != null)
                Task.Run(async () => { await getVehicleByVehicle(veh).update(true); });
        }

        private void OnVehicleDeath(NetHandle entity)
        {
            Vehicle veh = API.getEntityFromHandle<Vehicle>(entity);
            VehicleHandler vhveh = getVehicleByVehicle(veh);
            API.delay(120000, true, () =>
            {
                PlayerHandler ph = Players.getPlayerBySCN(vhveh.pid);
                ph.client.sendNotification("",$"Votre véhicule à était retrouvé détruit, retrouvez le à la ~r~fourrière.");
                vhveh.delete();
            });
        }

        public static VehicleHandler getVehicleByVehicle(Vehicle vehicle) => vehicles.Find(x => x.vehicle == vehicle) ?? null;

        public static void remove(VehicleHandler veh)
        {
            vehicles.Remove(veh);
        }

        public static bool isPlateUnique(string plate)
        {
            DataTable result = Database.getQueryNoAsync($"SELECT plate FROM vehicles WHERE plate='{plate}' LIMIT 1");
            if (result.Rows.Count > 0)
                return false;
            return true;
        }

        public static int getVehicleInventorySize(VehicleHash vehicleHash)
        {
            if (vehicleHash.ToString() == null)
                return (0);

            DataTable result = Database.getQueryNoAsync(string.Format("SELECT poids FROM cardealer WHERE vehiclehash='{0}' LIMIT 1", ((int)vehicleHash).ToString()));
            if (result.Rows != null && result.Rows.Count > 0)
                return ((int)result.Rows[0]["poids"]);
            else
                return (0);
        }

        public static async Task loadAllVehiclesActive()
        {
            Console.WriteLine("--- Start loading all active vehicles ---");
            DataTable vehicles = await Database.getQuery($"SELECT pid, name, plate, primaryColor, secondaryColor, inventory, fuel, location, mods, neon FROM vehicles WHERE active='1'");
            foreach (DataRow vehicle in vehicles.Rows)
            {
                Location location = Location.fromJson(Convert.ToString(vehicle["location"]));
                Inventory inventory = Inventory.fromJson(Convert.ToString(vehicle["inventory"]));
                VehicleHash model = (VehicleHash)Convert.ToDouble(vehicle["name"]);
                Dictionary<int, int> mods = JsonConvert.DeserializeObject<Dictionary<int, int>>(Convert.ToString(vehicle["mods"]));
                Vector3 neon = JsonConvert.DeserializeObject<Vector3>(Convert.ToString(vehicle["neon"]));

                new VehicleHandler(Convert.ToString(vehicle["pid"]), model, location.pos.toVector3(), location.rot.toVector3(),
                plate: Convert.ToString(vehicle["plate"]), primaryColor: Convert.ToInt32(vehicle["primaryColor"]),
                secondaryColor: Convert.ToInt32(vehicle["secondaryColor"]), inventory: inventory,
                mods: mods, neon: neon);
            }
            Console.WriteLine("--- Finish loading all active vehicles ---");
        }

        public static async Task saveAllVehiclesActive()
        {
            foreach (VehicleHandler vehicle in Vehicles.vehicles)
                await vehicle.update(true);
        }

        [Command("getfuel")]
        public void getfuel(Client client)
        {
            if (Players.getPlayerByClient(client).staffRank <= AdminRank.Player) return;
            if (client.isInVehicle)
            {
                client.sendChatMessage(client.vehicle.fuelLevel.ToString());
            }
        }

        [Command("setfuel")]
        public void setfuel(Client client, float fuel)
        {
            if (Players.getPlayerByClient(client).staffRank <= AdminRank.Player) return;
            if (client.isInVehicle)
            {
                client.vehicle.fuelLevel = fuel;
                client.sendChatMessage(client.vehicle.fuelLevel.ToString());
            }
        }
    }
}
