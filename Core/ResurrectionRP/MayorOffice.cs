﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrandTheftMultiplayer.Shared;
using MenuManagement;
using GrandTheftMultiplayer.Server.Constant;
using Enums;

namespace ResurrectionRP
{
    class MayorOffice : Script
    {
        private Vector3 _entermayor = new Vector3(-114.3856f, -607.5189f, 36.28076f);
        private Vector3 _exitMayer = new Vector3(-119.9218f, -590.1616f, 48.22081f);
        private Vector3 _mayorRoomEnter = new Vector3(-140.8429f, -616.7669f, 168.8205f);
        private Vector3 _mayorRoomExit = new Vector3(-141.2628f, -614.3312f, 168.8205f);
        private Vector3 _computer = new Vector3(-125.9467f, -641.6888f, 168.8353f);
        private Menu menu;
        private ListItem essencetaxe;
        private ListItem achattaxe;
        private ListItem farmtaxe;
        private MenuItem valider;
        private MenuItem essenceprice;

        public MayorOffice()
        {
            API.onResourceStart += onResourceStart;
        }

        private void onResourceStart()
        {
            Blip blip = API.createBlip(_exitMayer);
            blip.sprite = 78;
            blip.name = "Mairie";
            blip.shortRange = true;

            GrandTheftMultiplayer.Server.Elements.Marker _markerEnter = API.createMarker(1, _entermayor - new Vector3(0, 0, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            CylinderColShape _colEnterMairie = API.createCylinderColShape(_entermayor, 1f, 1f);

            GrandTheftMultiplayer.Server.Elements.Marker _markerExitMayor = API.createMarker(1, _mayorRoomExit - new Vector3(0, 0, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            CylinderColShape _colExitMairie = API.createCylinderColShape(_mayorRoomExit, 2f, 2f);

            GrandTheftMultiplayer.Server.Elements.Marker _markerComputerMayor = API.createMarker(1, _computer - new Vector3(0, 0, 1f), new Vector3(), new Vector3(), new Vector3(1f, 1f, 1f), 100, 255, 255, 255);
            CylinderColShape _colComputer = API.createCylinderColShape(_computer, 2f, 2f);

            // Enter
            _colEnterMairie.onEntityEnterColShape += (shape, entity) =>
            {
                if (API.getEntityType(entity) != EntityType.Player) return;
                Client player = API.getPlayerFromHandle(entity);
                if (player.isInVehicle) return;
                API.setEntityPosition(player, _mayorRoomEnter);
            };

            _colEnterMairie.onEntityExitColShape += (shape, entity) =>
            {
                if (API.getEntityType(entity) != EntityType.Player) return;
                MenuManager.CloseMenu(API.getPlayerFromHandle(entity));
            };

            // Out
            _colExitMairie.onEntityEnterColShape += (shape, entity) =>
            {
                if (API.getEntityType(entity) != EntityType.Player) return;
                API.setEntityPosition(API.getPlayerFromHandle(entity), _exitMayer);
            };


            _colExitMairie.onEntityExitColShape += (shape, entity) =>
            {
                if (API.getEntityType(entity) != EntityType.Player) return;
                MenuManager.CloseMenu(API.getPlayerFromHandle(entity));
            };
                

            //Computer
            _colComputer.onEntityEnterColShape += (shape, entity) => {
                if (API.getEntityType(entity) != EntityType.Player) return;
                OpenMayorMenu(API.getPlayerFromHandle(entity));
            };

            _colComputer.onEntityExitColShape += (shape, entity) => {
                if (API.getEntityType(entity) != EntityType.Player) return;
                MenuManager.CloseMenu(API.getPlayerFromHandle(entity));
            };
        }

        private void OpenMayorMenu(Client client)
        {
            if (Players.getPlayerByClient(client).getRangFaction(FactionType.Mayor) == 0) return;

            menu = new Menu("ID_MayorMenu", "Bureau du Maire", "Choisissez une option:", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
            menu.BannerColor = new Color(255, 0, 0, 1);
            menu.Callback = MayorMenuManager;

            List<string> doublearray = new List<string> { };
            for (double i = 0.0; i <= 100; i = i + 0.1) { doublearray.Add(i.ToString() + "%"); };
            essencetaxe = new ListItem("Taxe sur l'essence", "", "ID_essencetaxe", doublearray, Convert.ToInt32(Economy.Taxe_Essence));
            achattaxe = new ListItem("Taxe sur l'achat d'objet:", "", "ID_achattaxe", doublearray, Convert.ToInt32(Economy.Taxe_Market));
            farmtaxe = new ListItem("Taxe sur la revente de matière 1ére:", "", "ID_reventetaxe", doublearray, Convert.ToInt32(Economy.Taxe_Exportation));

            List<string> numberarray = new List<string> { };
            for (int i = 0; i <= 10000; i = i + 10) { numberarray.Add("$" + i.ToString()); };
            essenceprice = new ListItem("Prix du parking", "", "ID_parkingprice", numberarray, Economy.Price_Parking);

            valider = new MenuItem("Valider", "", "ID_Valider");
            valider.ExecuteCallback = true;

            menu.Add(valider);
            menu.Add(essencetaxe);
            menu.Add(achattaxe);
            menu.Add(farmtaxe);
            menu.Add(essenceprice);

            MenuManager.OpenMenu(client, menu);
        }

        private void MayorMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == valider)
            {
                Economy.Taxe_Essence = Convert.ToDouble(Convert.ToString(data["ID_essencetaxe"]["Value"]).Replace("%", ""));
                Economy.Taxe_Exportation = Convert.ToDouble(Convert.ToString(data["ID_reventetaxe"]["Value"]).Replace("%", ""));
                Economy.Taxe_Market = Convert.ToDouble(Convert.ToString(data["ID_achattaxe"]["Value"]).Replace("%", ""));
                Economy.Price_Parking = Convert.ToInt32(Convert.ToString(data["ID_parkingprice"]["Value"]).Replace("$", ""));

            }
        }
    }
}
