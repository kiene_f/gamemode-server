﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class HeartManager : Script
    {
        public HeartManager()
        {
            API.onPlayerConnected += ((client) => {
                for (var i =0; i <= 5; i++)
                {
                    API.sendNativeToPlayer(client, Hash.DISABLE_HOSPITAL_RESTART, i, true);
                }
                API.sendNativeToPlayer(client, Hash.ADD_HOSPITAL_RESTART, 290.3369f, -1345.791f, 24.5378f, 1f,0);
                API.sendNativeToPlayer(client, Hash._DISABLE_AUTOMATIC_RESPAWN, true);
            });

            API.onPlayerRespawn += ((client) =>
            {
                API.sendNativeToPlayer(client, Hash._DISABLE_AUTOMATIC_RESPAWN, true);
                API.sendNativeToPlayer(client, Hash.SET_FADE_IN_AFTER_DEATH_ARREST, false);
                API.sendNativeToPlayer(client, Hash.SET_FADE_OUT_AFTER_DEATH, false);

                PlayerHandler ph = Players.getPlayerByClient(client);
                ph.hunger = 100;
                ph.thirst = 100;
                ph.bank -= Economy.Price_RespawnSoins;
                client.sendNotification("", $"~r~[Hôpital] ~s~Vous êtes dorénavant sur pied, vous nous avez payé la somme de  $~r~{Economy.Price_RespawnSoins}");
                if (EMSManager.medicAvailable())
                {
                    ph.inventory.clear(25);
                    ph.money = 0;
                }
                CharacterCreator.ApplyCharacter(client);
                client.freezePosition = false;
            });

            API.onPlayerDeath += ((player, entityKiller, weapon) =>
            {
                API.sendNativeToPlayer(player, Hash._DISABLE_AUTOMATIC_RESPAWN, true);
                API.sendNativeToPlayer(player, Hash.SET_FADE_IN_AFTER_DEATH_ARREST, true);
                API.sendNativeToPlayer(player, Hash.SET_FADE_OUT_AFTER_DEATH, false);

                Client killer = API.getPlayerFromHandle(entityKiller);
                if (killer != null)
                    LogManager.Log($"{Players.getPlayerByClient(player).name} à était tuer par {Players.getPlayerByClient(killer).name} à {((WeaponHash)weapon).ToString()}");
                else
                {
                    LogManager.Log($"{Players.getPlayerByClient(player).name} à était tuer à {((WeaponHash)weapon).ToString()}");
                }
            });
        }

    }
}
