﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Player
{
    class Connection: Script
    {
        private static string salt = "asgzerf569zer852vbc3zer99sdf87zer";
        private static int dimension = 1;

        public Connection()
        {
            API.onPlayerFinishedDownload += (async (player) =>
            {
                if (!player.isCEFenabled)
                    player.kick("Vous n'avez pas les CEF d'activer.");
                player.dimension = dimension++;
                await Database.waitMysqlReady();

                DataTable result = await Database.getQuery($"SELECT id_socialclub FROM whitelist WHERE id_socialclub='{player.socialClubName}' LIMIT 1");

                if (result.Rows.Count != 0)
                    player.triggerEvent("setLoginUiVisible", true, false);
                else
                    API.shared.kickPlayer(player, "[WHITELIST] Vous n'êtes pas whitelisté, + d'info sur resurrectionrp.fr");
            });

            API.onPlayerDisconnected += (async (player, reason) =>
            {
                if (Players.players.Contains(Players.getPlayerByClient(player)))
                {
                    await Players.getPlayerByClient(player).updatePlayerInfo();
                    if (reason == "Quit")
                        Players.delete(player); 
                }
            });

            API.onClientEventTrigger += (async (sender, eventname, args) => {
                if (eventname == "finish_intro")
                {
                    API.setEntityDimension(sender, 0);
                    await Players.getPlayerByClient(sender).updatePlayerInfo();
                }
                else if (eventname == "Login")
                {
                    if (await CheckPassword(sender, args[0].ToString()))
                    {
                        if (Players.getPlayerByClient(sender) == null)
                            Players.add(sender);
                        sender.triggerEvent("loginAccepted", true, true);
                    }
                    else
                        sender.triggerEvent("setLoginUiVisible", true, true);
                }
            });
        }

        private static async Task<bool> CheckPassword(Client client, string password)
        {
            DataTable result = await Database.getQuery($"SELECT password FROM whitelist WHERE id_socialclub='{client.socialClubName}' LIMIT 1");
            if (result.Rows.Count <= 0) return false;
            return API.shared.getHashSHA256(salt + password) == result.Rows[0]["password"].ToString();
        }
    }
}
