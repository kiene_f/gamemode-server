﻿using GrandTheftMultiplayer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Player.Job
{
    class ServiceMan : Job
    {
        private PlayerHandler player;
        private Location spawn;
        public VehicleHandler towtruck { get; set; }

        public ServiceMan() { }

        public ServiceMan(PlayerHandler player, Location spawn, VehicleHash hash)
        {
            this.player = player;
            this.spawn = spawn;
            this.towtruck = new VehicleHandler(player.socialClubName, hash, spawn.pos.toVector3(), spawn.rot.toVector3(),spawnVeh: true);
            if (player.character.Gender == 0)
            {
                API.setPlayerClothes(player.client, 11, 238, 0);
                API.setPlayerClothes(player.client, 4, 36, 2);
                API.setPlayerClothes(player.client, 6, 12, 0);
                API.setPlayerClothes(player.client, 8, 14, 0);
            }
            else
            {
                API.setPlayerClothes(player.client, 11, 236, 0);
                API.setPlayerClothes(player.client, 4, 76, 2);
                API.setPlayerClothes(player.client, 6, 68, 0);
                API.setPlayerClothes(player.client, 8, 2, 0);
            }
        }
    }
}
