﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using ResurrectionRP.Items;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ResurrectionRP.Player.Job
{
    public class LumberMan : Job
    {
        private PlayerHandler player;
        private Location spawn;
        private Vector3 farmSpawn;

        public VehicleHandler truck { get; set; }
        public VehicleHandler trailer { get; set; }
        public bool notification = false;

        public LumberMan() {
            API.onClientEventTrigger += API_onClientEventTrigger;
        }

        public void API_onClientEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            if (eventName == "ValidateLumberjackSwing")
            {
                NetHandle obj = new NetHandle();
                try
                {
                    obj = (NetHandle)arguments[0];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                if (obj.IsNull) return;
                //Tree treeHit = LumberjackManager.CurrentTrees.FirstOrDefault(t => t.Position.DistanceTo(pos) < 1.2);
                Tree treeHit = LumberjackManager.CurrentTrees.Find(t => t.TreeHandle.handle == obj);

                    if (!(treeHit?.RemainingWood >= 1)) return;
                    treeHit.CurrentStrikes++;
                    if (treeHit.CurrentStrikes < treeHit.StrikesRequired) return;
                    treeHit.CurrentStrikes = 0;
                    treeHit.RemainingWood--;
                    if (treeHit.CurrentStrikes >= treeHit.StrikesRequired)
                    {
                        Vector3 _postemp = treeHit.Position;
                        Vector3 _rottemp = treeHit.Rotation;
                        treeHit.delete();
                        API.delay(API.RandomNumber(480000, 900000), true, (() => {
                            new Tree(_postemp, _rottemp, TreeHeight.Big);
                        })); 
                        return;
                    }

                    var log = API.createObject(1366334172, treeHit.Position.Add(new Vector3(0.7, 0.7, 1.3)), new Vector3(83, 0, 0));
                    API.sendNativeToPlayersInRange(sender.position, 30, Hash.FREEZE_ENTITY_POSITION, log.handle, false);
                    API.sendNativeToPlayersInRange(sender.position, 30, Hash.SET_ENTITY_DYNAMIC, log.handle, true);

                    treeHit.SpawnedLogs.Add(log);


            }
            else if (eventName == "PickupLumber")
            {
                NetHandle obj = new NetHandle();
                try
                {
                    obj = (NetHandle)arguments[0];
                }
                catch {}
                if (obj.IsNull) return;
                API.deleteEntity(obj);
                Players.getPlayerByClient(sender).inventory.add(Inventory.itemByID(Enums.Items.RodinDeBois), 1);
            }
        }

        public override void delete()
        {
            player.client.removeWeapon(WeaponHash.Hatchet);
            truck.vehicle.delete();
            trailer.vehicle.delete();
            Vehicles.vehicles.Remove(truck);
            Vehicles.vehicles.Remove(trailer);
            player.client.setSyncedData("LumberZone", false);
            CharacterCreator.ApplyCharacter(player.client);
        }

        public LumberMan(PlayerHandler player, Location spawn, Vector3 farmSpawn)
        {

            player.client.triggerEvent(Enums.Events.setWaypoint, farmSpawn.X, farmSpawn.Y);
            CylinderColShape farmColShape = API.createCylinderColShape(farmSpawn - new Vector3(0f, 0f, 1f), 200.0f, 10.0f);
            player.client.giveWeapon(WeaponHash.Hatchet, 100, true, true);
            this.truck = new VehicleHandler(player.socialClubName, VehicleHash.Hauler, spawn.pos.toVector3(), spawn.rot.toVector3(), primaryColor: 0, secondaryColor: 0, locked: false, engineStatus: true, spawnVeh: true, inventorySize: 0);
            this.trailer = new VehicleHandler(player.socialClubName, VehicleHash.TrailerLogs, truck.vehicle.position.Forward(truck.vehicle.rotation.Z, -15f), truck.vehicle.rotation, primaryColor: 0, secondaryColor: 0, locked: false, engineStatus: true, spawnVeh: true, inventorySize: 80);

            farmColShape.onEntityEnterColShape += (shape, entity) =>
            {
                Client client = API.getPlayerFromHandle(entity);
                if (client == null || client != player.client)
                    return;

                if (!client.isInVehicle || client.vehicle != truck.vehicle) { client.sendNotificationError("Vous devez être dans votre camion."); return; }
                canFarm = true;
                if (notification == false)
                {
                    player.client.sendNotification("Server", "Utiliser votre hâche pour commencer à couper du bois.");
                    notification = true;
                }
                client.setSyncedData("LumberZone", true);
            };

            farmColShape.onEntityExitColShape += (shape, entity) =>
            {
                Client client = API.getPlayerFromHandle(entity);
                if (client == null || client != player.client)
                    return;
                canFarm = false;
                client.setSyncedData("LumberZone", false);
            };

            if (player.character.Gender == 0)
            {
                API.setPlayerClothes(player.client, 11, 14, 9);
                API.setPlayerClothes(player.client, 3, 12, 0);
                API.setPlayerClothes(player.client, 8, 57, 0);
            }
            else
            {
                //API.setPlayerClothes(player.client, 11, 190, 0);
            }

            this.player = player;
            this.spawn = spawn;
            this.farmSpawn = farmSpawn;
            this.canFarm = false;
        }
    }
}
