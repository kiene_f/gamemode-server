﻿using Enums;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using ResurrectionRP.Player.Job;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ResurrectionRP
{
    public class PlayerHandler
    {
        private Client _client;
        private string _socialClubName;
        private string _firstName;
        private string _lastName;
        private string _ip;
        private bool _jailed;
        private int _money;
        private int _bank;
        private Inventory _inventory;
        private AdminRank _staffRank;
        public Location location;
        private int _hunger;
        private int _thirst;
        private Gender _gender;
        public Job job { get; set; }
        public PlayerCustomization character { get; set; }
        private bool _isOnProgress = false;
        private Timer HungerLoop;
        private Timer ThirstLoop;
        private Timer SaveLoop;
        private List<FactionRang> _faction = new List<FactionRang>();

        public PlayerHandler(Client client)
        {
            _client = client;
            _socialClubName = client.socialClubName;

            Task.Run(async () => {
                await loadPlayer();
                new Teamspeak(_client).Connect("");
            });

            HungerLoop = API.shared.delay(170000, false, () =>
            {
                if (!client.dead)
                {
                    hunger--;
                    if (hunger <= 0)
                    {
                        client.sendNotification("", "Vous êtes mort de faim.");
                        hunger = 100;
                        client.health = -1;
                    }
                }

            });
            ThirstLoop = API.shared.delay(105000, false, () =>
            {
                if (client.dead)
                {
                    thirst--;
                    if (thirst <= 0)
                    {
                        client.sendNotification("", "Vous êtes mort de soif.");
                        thirst = 100;
                        client.health = -1;
                    }
                }
            });
            SaveLoop = API.shared.delay(60000 * 5, false, async () =>
            {
                client.sendNotification("", "[INFO] ~b~Sauvegarde automatique ...");
                await updatePlayerInfo();
            });
            client.triggerEvent(Events.startstatut);

        }

        public Client client { get => _client; }
        public string socialClubName { get => _socialClubName; }
        public string firstName { get => _firstName; }
        public string lastName { get => _lastName; }
        public string ip { get => _ip; }
        public bool jailed { get => _jailed; }
        public AdminRank staffRank { get => _staffRank; set => _staffRank = value; }

        public string name
        {
            set
            {
                if (value != "" && value != _firstName + " " + _lastName)
                {
                    string[] name = value.Split(' ');
                    _firstName = name[0];
                    _lastName = name[1];
                }
            }
            get => _firstName + " " + _lastName;
        }

        public int money
        {
            set
            {
                if (value != _money)
                {
                    _client.triggerEvent(Events.UpdateMoneyHUD, value, (value - _money));
                    Task.Run(async () => { await updateMoneyDatabase(); }); 
                    _money = value;
                }
            }
            get => _money;
        }

        public int bank
        {
            set
            {
                if (value != _bank)
                {
                    Task.Run(async () => { await updateMoneyDatabase(); });
                    _bank = value;
                }
            }
            get => _bank;
        }

        public int hunger
        {
            get => _hunger;
            set
            {
                _hunger = value;
                client.setSyncedData("hunger", value);
            }
        }

        public int thirst
        {
            get => _thirst;
            set
            {
                _thirst = value;
                client.setSyncedData("thirst", value);
            }
        }
        public Inventory inventory { get => _inventory; }
        public bool isOnProgress { get => _isOnProgress; set => _isOnProgress = value; }
        // public List<Tuple<FactionType, int>> faction { get => _faction; set => _faction = value; }

        #region MethodRegion
        private async Task<bool> existPlayerInfo()
        {
            DataTable result = await Database.getQuery($"SELECT * FROM user WHERE id='{_client.socialClubName}' LIMIT 1");
            if (result.Rows.Count > 0)
                return true;
            else
                return false;
        }

        private async Task<DataTable> getPlayerInfo() =>
            await Database.getQuery($"SELECT money, ip, bank, location, jailed, inventory, staffRank , characters, hunger, thirst, health, name, faction FROM user WHERE id='{_client.socialClubName}' LIMIT 1");

        public async Task updatePlayerInfo(Location Location = null)
        {
            
            string location = "";
            if (Location == null)
                location = Serialize.toJson(Location.fromVector3(API.shared.getEntityPosition(this.client), API.shared.getEntityRotation(this.client)));
            else
                location = Serialize.toJson(Location);

            if (_client.hasData("InsideHouse_ID"))
            {
                House house = HouseMain.Houses.FirstOrDefault(h => h.ID == _client.getData("InsideHouse_ID"));
                if (house != null)
                {
                    location = Serialize.toJson(Location.fromVector3(house.Position, API.shared.getEntityRotation(this.client)));
                }
            }
            API.shared.consoleOutput(location);
            string factionjson = JsonConvert.SerializeObject(_faction, Converter.settings);
            string inventory = this.inventory.toJson();
            int health = API.shared.getPlayerHealth(this.client);
            await Database.insertQuery(string.Format("UPDATE user SET location='{0}', inventory='{1}', hunger='{2}', thirst='{3}', health='{4}', faction='{5}', money='{6}', bank='{7}' WHERE id='{8}'", location, inventory, this.hunger, this.thirst, health, factionjson, money, bank, this.socialClubName));
        }

        public async Task updateMoneyDatabase()=>
                await Database.insertQuery(string.Format("UPDATE user SET money='{0}', bank='{1}' WHERE id='{8}'", money, bank, this.socialClubName));

        public async Task insertPlayerDatabase() =>
            await Database.insertQuery($"INSERT INTO user (id, ip, money, bank, name, characters) VALUES ('{ _client.socialClubName}','{_client.address}','{_money}','{_bank}','{name}','{API.shared.toJson(character)}')");

        public async Task updateCharacter()
        {
            character = CharacterCreator.CustomPlayerData[this.client.handle];          
            await Database.insertQuery(string.Format("UPDATE user SET characters='{0}' WHERE id='{1}'", API.shared.toJson(character), this.socialClubName));
        }

        public void addfactionLicence(FactionType factiontype, int rang) => _faction.Add(new FactionRang(factiontype, rang));

        public int getRangFaction(FactionType faction)
        {
            if (_faction.Exists(x => x.FactionType == faction))
                return _faction.Find(x => x.FactionType == faction).Rang;
            return 0;
        }

        public bool isinfaction(FactionType faction) => _faction.Exists(x => x.FactionType == faction);

        public void destroy()
        {
            HungerLoop.Dispose();
            ThirstLoop.Dispose();
            SaveLoop.Dispose();
        }
        #endregion
        #region loadSection
        public async Task loadPlayer(bool firstspawn = false)
        {
            if (!await existPlayerInfo())
            {
                _firstName = "";
                _lastName = "";
                _jailed = false;

                _bank = 20000;
                this.money = 0;
                _staffRank = 0;
                _client.setSyncedData("adminrank", _staffRank);
                _gender = Gender.Man;

                this.hunger = 100;
                client.setSyncedData("hunger", hunger);
                this.thirst = 100;
                client.setSyncedData("thirst", thirst);
                _inventory = new Inventory(25);
                CharacterCreator.SendToCreator(_client);
            }  else  {      
                DataRow info = (await getPlayerInfo())?.Rows?[0];
                string[] name = Convert.ToString(info["name"]).Split(' ');
                _firstName = name[0];
                _lastName = name[1];
                _ip = Convert.ToString(info["ip"]);
                _jailed = Convert.ToBoolean(info["jailed"]);
                _bank = Convert.ToInt32(info["bank"]);
                _staffRank = (AdminRank)Convert.ToInt32(info["staffRank"]);
                _client.setSyncedData("adminrank", Convert.ToInt32(info["staffRank"]));
                _gender = Gender.Man;

                this.money = Convert.ToInt32(info["money"]);
                this.hunger = Convert.ToInt32(info["hunger"]);
                client.setSyncedData("hunger", hunger);
                this.thirst = Convert.ToInt32(info["thirst"]);
                client.setSyncedData("thirst", thirst);
                
                this.character = JsonConvert.DeserializeObject<PlayerCustomization>(info["characters"].ToString());
                CharacterCreator.CustomPlayerData.Add(_client.handle, this.character);
                CharacterCreator.ApplyCharacter(_client);

                if (info["faction"] is DBNull || String.IsNullOrEmpty(info["faction"].ToString()) || info["faction"] == null)
                    _faction = new List<FactionRang>();
                else
                    _faction = JsonConvert.DeserializeObject<List<FactionRang>>(info["faction"].ToString());
                
                _inventory = Inventory.fromJson(Convert.ToString(info["inventory"]));
                if (_inventory == null)
                    _inventory = new Inventory(25);

                if (firstspawn)
                    _client.triggerEvent(Events.startIntroduction);
                else
                {
                    if (info["location"] is DBNull || String.IsNullOrEmpty(info["location"].ToString()) || info["location"] == null)
                        this.location = Location.fromVector3(new Vector3(-1033.028f, -2732.261f, 14f), new Vector3(0f, 0f, -37.83434f));
                    else
                        this.location = Location.fromJson(Convert.ToString(info["location"])); 
                    _client.position = this.location.pos.toVector3();
                }
                _client.triggerEvent(Events.UpdateMoneyHUD, _money);
                _client.triggerEvent(Events.PlayerInitialised);
            }

            //_client.healthbarVisible = false;
            _client.nametagVisible = false;
            _client.dimension = 0;
            await Task.FromResult(0);
        }
        #endregion
    }
}