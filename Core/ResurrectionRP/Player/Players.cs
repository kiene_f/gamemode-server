﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System.Collections.Generic;
using System.Data;

namespace ResurrectionRP
{
    class Players
    {
        public static List<PlayerHandler> players = new List<PlayerHandler>();

        public static PlayerHandler add(Client client)
        {
            PlayerHandler player = new PlayerHandler(client);
            players.Add(player);

            return player;
        }

        public static void delete(Client client)
        {
            PlayerHandler player = players.Find(x => x.client == client) ?? null;
            if (player != null)
                delete(player);
        }

        public static void delete(string socialClubName)
        {
            PlayerHandler player = players.Find(x => x.socialClubName == socialClubName) ?? null;
            if (player != null)
                delete(player);
        }

        public static void delete(PlayerHandler player)
        {
            if (players.Contains(player))
            {
                player.destroy();
                players.Remove(player);
            }
                
        }

        public static PlayerHandler getPlayerBySCN(string socialClubName) => players.Find(x => x.socialClubName == socialClubName) ?? null;
        public static PlayerHandler getPlayerByName(string name) => players.Find(x => x.name == name) ?? null;
        public static PlayerHandler getPlayerByClient(Client player) => players.Find(x => x.client == player) ?? null;

        public static List<PlayerHandler> getNearestPlayer(PlayerHandler sender, float distance = 3.0f) => getNearestPlayer(sender.client, distance);
        public static List<PlayerHandler> getNearestPlayer(Client sender, float distance = 3.0f)
        {
            List<PlayerHandler> phList = new List<PlayerHandler>();
            foreach (PlayerHandler ph in players)
            {
                Vector3 phPos = API.shared.getEntityPosition(ph.client.handle);
                float distanceToPlayer = sender.position.DistanceTo(phPos);
                if (distanceToPlayer < distance)
                {
                    distance = distanceToPlayer;
                    phList.Add(ph);
                }
            }
            return phList;
        }
    }
}
