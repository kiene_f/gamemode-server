﻿using Discord;
using Discord.Net;
using Discord.Commands;
using Discord.WebSocket;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    public static class DiscordBot
    {
        private static DiscordSocketClient client = new DiscordSocketClient();
        private static CommandService commands = new CommandService();
        private static IServiceProvider services = new ServiceCollection().BuildServiceProvider();

        private static ulong channelToken = 0; // In-Game Channel
        private static ulong logchannel = 0; // Log Channel

        public async static Task startBot()
        {
            string token = "";

            try
            {
                await client.LoginAsync(TokenType.Bot, token);
                await client.StartAsync();
                Console.WriteLine("Discord Bot connected!");
            }
            catch
            {
                Console.WriteLine("Could not connect properly.");
                return;
            }
        }

        public static void sendMessageToServer(string message)
        {
            SocketTextChannel channel = client.GetChannel(channelToken) as SocketTextChannel;
            try
            {
                channel.SendMessageAsync(message);
            }
            catch (Exception ex) { Console.Write(ex.ToString()); }

        }

        public static void sendMessageToLog(string message)
        {
            SocketTextChannel channel = client.GetChannel(logchannel) as SocketTextChannel;
            try
            {
                channel.SendMessageAsync(message);
            }
            catch (Exception ex) { Console.Write(ex.ToString()); }

        }
    }
}