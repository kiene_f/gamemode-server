﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Businesses
{
    class Parkings : Businesses
    {
        [JsonIgnore]
        private static List<Parkings> _parkings = new List<Parkings>();

        public Vector3 enterpos;
        public Vector3 spawn1;
        public float spawn1rot;   
        public Vector3 spawn2;
        public float spawn2rot;

        private CylinderColShape colexit1;
        private CylinderColShape colexit2;

        public Parkings(Vector3 enterpos, Vector3 spawn1, float spawn1rot, Vector3 spawn2, float spawn2rot, string owner = null) : base(owner, "parkings")
        {
            this.enterpos = enterpos;
            this.spawn1 = spawn1;
            this.spawn1rot = spawn1rot;
            this.spawn2 = spawn2;
            this.spawn2rot = spawn2rot;
            //load();
        }

        public void load()
        {
            CylinderColShape colenter = API.shared.createCylinderColShape(enterpos, 3f, 3f);
            colenter.onEntityExitColShape += Colenter_onEntityExitColShape;
            colexit1 = API.shared.createCylinderColShape(spawn2, 5f, 3f);
            colexit2 = API.shared.createCylinderColShape(spawn2, 5f, 3f);

            API.shared.createMarker(1, colenter.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(3, 3, 1), 100, 255, 255, 255);
            Blip blip = API.shared.createBlip(enterpos);
            blip.sprite = 50;
            blip.shortRange = true;
            colenter.onEntityEnterColShape += (async (shape, entity) =>
            {

                if (API.shared.getEntityType(entity) == EntityType.Player)
                {
                    PlayerHandler ph = Players.getPlayerByClient(API.shared.getPlayerFromHandle(entity));

                    if (ph.client.isInVehicle)
                    {
                        Vehicle veh = ph.client.vehicle;
                        if (veh == null) return;
                        Menu menu = new Menu("parking", "Parking", $"Ranger le véhicule: ~r~" + veh.numberPlate, 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                        menu.BannerSprite = Banner.Garage;

                        MenuItem accepter = new MenuItem("Accepter")
                        {
                            RightLabel = "$" + Economy.Price_Parking.ToString(),
                            ExecuteCallback = true,
                            ExecuteCallbackIndexChange = false
                        };

                        MenuItem refuser = new MenuItem("Refuser")
                        {
                            ExecuteCallback = true,
                            ExecuteCallbackIndexChange = false
                        };
                        menu.Add(accepter);
                        menu.Add(refuser);

                        menu.Callback = (async (_client, _menu, _menuItem, _itemIndex, _forced, _data) =>
                        {
                            if (_menuItem == accepter)
                            {
                                VehicleHandler vehicle = Vehicles.getVehicleByVehicle(veh);
                                if (vehicle == null)
                                {
                                    ph.client.sendNotificationError("[ERREUR] Voiture impossible à ranger dans le garage.");
                                    MenuManager.CloseMenu(ph.client);
                                    return;
                                }
                                await VehicleParkingStatut(vehicle, false);
                                vehicle.delete();
                                ph.money -= Economy.Price_Parking;
                                MenuManager.CloseMenu(ph.client);
                            }
                            else
                                MenuManager.CloseMenu(ph.client);
                        });
                        MenuManager.OpenMenu(ph.client, menu);
                    }
                    else
                    {
                        Menu menu = new Menu("parking", "Parking", "", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                        menu.BannerSprite = Banner.Garage;

                        DataTable vehnamelist = await GetAllVehicleInParking();
                        foreach (DataRow vehname in vehnamelist.Rows)
                        {
                            VehicleHash hash = API.shared.vehicleNameToModel(Convert.ToString(vehname["name"]));
                            MenuItem pplate = new MenuItem(API.shared.getVehicleDisplayName(hash));
                            pplate.Description = $"Marque: {API.shared.getVehicleManufacturerName(hash)} \nPlaque: {Convert.ToString(vehname["plate"])}";
                            pplate.ExecuteCallback = true;
                            menu.Add(pplate);
                        }

                        menu.Callback = (async (_client, _menu, _menuItem, _itemIndex, _forced, _data) =>
                        {
                            DataRow dt = vehnamelist.Rows[_itemIndex];
                            await SpawnVehicle(await GetVehicleInParking(Convert.ToString(dt["plate"])), ph);
                        });
                        MenuManager.OpenMenu(ph.client, menu);
                    }
                }
            });
            _parkings.Add(this);
            businesses.Add(this);
        }

        private void Colenter_onEntityExitColShape(ColShape shape, NetHandle entity)
        {
            if (API.shared.getEntityType(entity) == EntityType.Player)
            {
                PlayerHandler ph = Players.getPlayerByClient(API.shared.getPlayerFromHandle(entity));
                if (MenuManager.HasOpenMenu(ph.client))
                    MenuManager.CloseMenu(ph.client);
            }
        }
        private async Task VehicleParkingStatut(VehicleHandler vehicle, bool statut)
        {
            await Database.insertQuery(string.Format("UPDATE vehicles SET active='{1}', parkingid='{2}' WHERE plate='{0}'",
                vehicle.plate,
                Database.boolToInt(statut),
                this.id
                ));
        }

        private async Task<DataTable> GetVehicleInParking(string plate)
        {
            DataTable result = await Database.getQuery($"SELECT * FROM vehicles WHERE plate='{plate}' LIMIT 1");
            return result;
        }

        private async Task<DataTable> GetAllVehicleInParking()
        {
            DataTable result = await Database.getQuery($"SELECT plate, name FROM vehicles WHERE active='0' && parkingid='{id}'");
            return result;
        }

        private async Task SpawnVehicle(DataTable datatable, PlayerHandler ph)
        {
            DataRow dt = datatable.Rows[0];
            Vector3 _spawnpos = new Vector3();
            float _spawnrot = 0f;
            if (colexit1.getAllEntities().Count() <= 0)
            {
                _spawnpos = colexit1.Center;
                _spawnrot = spawn1rot;
            }
            else if (colexit1.getAllEntities().Count() > 0 && colexit2.getAllEntities().Count() <= 0)
            {
                _spawnpos = colexit2.Center;
                _spawnrot = spawn2rot;
            }     
            else
            {
                ph.client.sendNotificationError("Aucune place n'est disponible.");
                return;
            }

            Inventory inventory = Inventory.fromJson(Convert.ToString(dt["inventory"]));
            VehicleHash model = API.shared.vehicleNameToModel(Convert.ToString(dt["name"]));
            Dictionary<int, int> mods = JsonConvert.DeserializeObject<Dictionary<int, int>>(Convert.ToString(dt["mods"]));
            Vector3 neon = JsonConvert.DeserializeObject<Vector3>(Convert.ToString(dt["neon"]));

            VehicleHandler veh = new VehicleHandler(Convert.ToString(dt["pid"]), model, _spawnpos, new Vector3(0, 0, _spawnrot),
            plate: Convert.ToString(dt["plate"]), primaryColor: Convert.ToInt32(dt["primaryColor"]),
            secondaryColor: Convert.ToInt32(dt["secondaryColor"]), inventory: inventory,
            mods: mods, neon: neon);
            MenuManager.CloseMenu(ph.client);
            await VehicleParkingStatut(veh, true);
            ph.client.sendNotificationSuccess($"[Parking] Vous avez sorti votre véhicule: {veh.plate}");
        }

    }
}
