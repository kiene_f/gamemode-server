﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Businesses
{
    class Ammunation : Businesses
    {
        [JsonIgnore]
        private static List<Ammunation> _ammunations = new List<Ammunation>();

        public Inventory inventory { get; set; }
        public Location npc { get; set; }
        public Tuple<int, Vector3> door;

        public Ammunation(){}

        public Ammunation(Location npc, Tuple<int, Vector3> door, string owner = null) : base(owner, "ammunation")
        {
            if (inventory == null)
                inventory = new Inventory(200);
            this.npc = npc;
            this.door = door;
            load();
        }

        public void load()
        {
            int door = API.shared.exported.doormanager.registerDoor(this.door.Item1, this.door.Item2);
            API.shared.exported.doormanager.setDoorState(door, false, 0);

            NPC vendor = new NPC((PedHash)(-1643617475), "", npc.pos.toVector3(), (int)npc.rot.z, 0);
            vendor.setData("Interaction", "Ammunation");

            blip = API.shared.createBlip(npc.pos.toVector3());
            API.shared.setBlipSprite(blip, 567);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, "Ammunation");
            if (owner == null)
                API.shared.setBlipColor(blip, color);

            _ammunations.Add(this);
            businesses.Add(this);
        }

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static Ammunation geAmmunationByPed(NPC ped) => _ammunations.Find(x => x.npc.pos.toVector3() == ped.position);
    }
}
