﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using System.Data;
using System.Threading.Tasks;

namespace ResurrectionRP.Businesses
{
    class CarDealer : Businesses
    {
        [JsonIgnore]
        private static List<CarDealer> _cardealer = new List<CarDealer>();

        public Location npc { get; set; }
        public string name { get; set; }
        public PedHash pedHash { get; set; }
        public int vehicletype { get; set; }
        public Vector3 spawn1;
        public Vector3 spawn2;
        public List<Tuple<VehicleHash,int,int>> vehiclelist { get; set; }
        public CylinderColShape cylcol1 { get; private set; }
        public CylinderColShape cylcol2 { get; private set; }

        public CarDealer() { }

        public CarDealer(string name, Location npc, PedHash pedHash, int vehicletype, Vector3 spawn1, Vector3 spawn2, string owner = null) : base(owner, "cardealer")
        {
            this.npc = npc;
            this.pedHash = pedHash;
            this.name = name;
            this.vehicletype = vehicletype;
            this.spawn1 = spawn1;
            this.spawn2 = spawn2;
            Task.Run(async () => { await load(); }); 
            
        }

        public async Task load()
        {
            
            vehiclelist = new List<Tuple<VehicleHash, int, int>>();
            string sql = $"SELECT vehiclehash, name, price, poids FROM cardealer WHERE nameconcess='{name}'";
            DataTable rows = await Database.getQuery(sql);
            if (rows != null)
            {
                foreach (DataRow row in rows.Rows)
                {
                    VehicleHash hash = (VehicleHash)Convert.ToInt32(Convert.ToString(row["vehiclehash"]));
                    int price = Convert.ToInt32(row["price"]);
                    int poids = Convert.ToInt32(row["poids"]);
                    vehiclelist.Add(new Tuple<VehicleHash, int, int>(hash, price, poids));
                }
            }
            

            NPC vendor = new NPC(pedHash, name, npc.pos.toVector3(), npc.rot.z, 0);
            vendor.setData("Interaction", "CarDealer");

            blip = API.shared.createBlip(npc.pos.toVector3());
            API.shared.setBlipSprite(blip, 225);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, name);

            cylcol1 = API.shared.createCylinderColShape(spawn1, 3f, 3f);
            cylcol2 = API.shared.createCylinderColShape(spawn2, 3f, 3f);

            _cardealer.Add(this);
            businesses.Add(this);
        }

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static CarDealer getCarDealerByPed(NPC ped) => _cardealer.Find(x => x.npc.pos.toVector3() == ped.position);
    }
}
