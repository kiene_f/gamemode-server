﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Businesses
{
    class Tatoo : Businesses
    {
        [JsonIgnore]
        private static List<Tatoo> _tatoos = new List<Tatoo>();

        public Location npc { get; set; }
        public Tuple<int, Vector3> door;

        public Tatoo(){}

        public Tatoo(Location npc, Tuple<int, Vector3> door, string owner = null) : base(owner, "tatoo")
        {
            this.npc = npc;
            this.door = door;
            load();
        }

        public void load()
        {
            int door = API.shared.exported.doormanager.registerDoor(this.door.Item1, this.door.Item2);
            API.shared.exported.doormanager.setDoorState(door, false, 0);

            NPC vendor = new NPC((PedHash)(-1105135100), "", npc.pos.toVector3(), (int)npc.rot.z, 0);
            vendor.setData("Interaction", "Tatoo");

            blip = API.shared.createBlip(npc.pos.toVector3());
            API.shared.setBlipSprite(blip, 71);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, "Tatoueur");
            if (owner == null)
                API.shared.setBlipColor(blip, color);

            _tatoos.Add(this);
            businesses.Add(this);
        }

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static Tatoo getAmmunationByPed(NPC ped) => _tatoos.Find(x => x.npc.pos.toVector3() == ped.position);
    }
}
