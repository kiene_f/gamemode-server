﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using Newtonsoft.Json;
using ResurrectionRP.Menus;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Businesses
{
    class ClothingStore : Businesses
    {
        [JsonIgnore]
        private static List<ClothingStore> _clothingStores = new List<ClothingStore>();

        public Location location { get; set; }
        public Banner banner;
        public CylinderColShape colshape;

        public ClothingStore(){}

        public ClothingStore(Location location, Banner banner, string owner = null) : base(owner, "clothingStore")
        {
            this.location = location;
            this.banner = banner;
            load();
        }

        public void load()
        {
            blip = API.shared.createBlip(this.location.pos.toVector3());
            API.shared.setBlipSprite(blip, 73);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, "Magasin de vêtements");
            /*
            if (owner == null)
                API.shared.setBlipColor(blip, color);
            */

            colshape = API.shared.createCylinderColShape(location.pos.toVector3(), 3f, 1f);
            API.shared.createMarker(1, colshape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(3, 3, 1), 100, 255, 255, 255);

            colshape.onEntityEnterColShape += ((_shape, _entity) =>
            {
                if (API.shared.getEntityType(_entity) != EntityType.Player) return;
                new ClothingStoreMenu(API.shared.getPlayerFromHandle(_entity), colshape, Banner.LowFashion);
            });

            colshape.onEntityExitColShape += ((_shape, _entity) =>
            {
                if (API.shared.getEntityType(_entity) != EntityType.Player) return;
                    MenuManager.CloseMenu(API.shared.getPlayerFromHandle(_entity));

            });

            _clothingStores.Add(this);
            businesses.Add(this);
        }

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static ClothingStore getClothingStoreByPed(NPC ped) => _clothingStores.Find(x => x.location.pos.toVector3() == ped.position);
        public static ClothingStore getClothingStoreByColshape(CylinderColShape colshape) => _clothingStores.Find(x => x.location.pos.toVector3() == colshape.Center);

    }
}
