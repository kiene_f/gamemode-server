﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Businesses
{
    class LosSantosCustom : Businesses
    {
        [JsonIgnore]
        private static List<LosSantosCustom> _lscustom = new List<LosSantosCustom>();
        private CylinderColShape reparcol;
        private CylinderColShape gerantcol;
        private CylinderColShape spawncol;

        public Tuple<int, Vector3> door;
        private MenuItem vehicleStatut;
        private MenuItem PService;
        private MenuItem reparcar;

        public Location position { get; set; }
        public Location gerantPos { get; set; }
        public Location spawn { get; set; }

        //Price 
        private int bodyReparPrice = 200;
        private MenuItem OutService;

        public LosSantosCustom(Location position, Location gerantPos, Location spawn, Tuple<int, Vector3> door, string owner = null) : base(owner, "lossantoscustom")
        {
            this.position = position;
            this.gerantPos = gerantPos;
            this.spawn = spawn;
            this.door = door;
            load();
        }

        public void load()
        {
            int door = API.shared.exported.doormanager.registerDoor(this.door.Item1, this.door.Item2);
            API.shared.exported.doormanager.setDoorState(door, false, 0);

            #region Reparation Colshape

            reparcol = API.shared.createCylinderColShape(position.pos.toVector3(), 3f, 3f);
            API.shared.createMarker(1, reparcol.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(3, 3, 1), 100, 255, 255, 255);

            reparcol.onEntityEnterColShape += ((shape, entity) =>
            {
                if (API.shared.getEntityType(entity) == EntityType.Player) {
                    PlayerHandler ph = Players.getPlayerByClient(API.shared.getPlayerFromHandle(entity));
                    if (ph.client.isInVehicle)
                    {
                        Vehicle veh = ph.client.vehicle;
                        if (veh == null) return;
                        Menu menu = new Menu("LosSantoCustom", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                        menu.Callback = LosSantosCustomReparManager;
                        menu.BannerSprite = Banner.CarMod;

                        vehicleStatut = new MenuItem("Véhicule Check-UP", "Vérifier les niveaux du véhicules");
                        vehicleStatut.ExecuteCallback = true;
                        menu.Add(vehicleStatut);

                        reparcar = new MenuItem("Réparer la carrosserie", "Réparer la carrosserie");
                        reparcar.ExecuteCallback = true;
                        reparcar.RightLabel = "$" + bodyReparPrice.ToString();
                        menu.Add(reparcar);

                        MenuManager.OpenMenu(ph.client, menu);
                    }
                }
            });
            reparcol.onEntityExitColShape += ((shape, entity) =>
            {
                if (API.shared.getEntityType(entity) == EntityType.Player)
                {
                    Client client = API.shared.getPlayerFromHandle(entity);
                    MenuManager.CloseMenu(client);
                    client.triggerEvent("closecheckvehicle");
                }
            });
            #endregion

            Blip blip = API.shared.createBlip(reparcol.Center);
            blip.sprite = 446;
            blip.shortRange = true;
            blip.color = 4;

            #region Gerant Colshape

            gerantcol = API.shared.createCylinderColShape(gerantPos.pos.toVector3(), 3f, 3f);
            API.shared.createMarker(1, gerantcol.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 100, 255, 255, 255);

            gerantcol.onEntityEnterColShape += ((shape, entity) =>
            {
                if (API.shared.getEntityType(entity) == EntityType.Player)
                {
                    PlayerHandler ph = Players.getPlayerByClient(API.shared.getPlayerFromHandle(entity));
                    Menu menu = new Menu("LosSantoCustom", "", "", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                    menu.Callback = LosSantosCustomGestionManager;
                    menu.BannerSprite = Banner.CarMod;

                    if (ph.job != null && ph.job.GetType() != typeof(Player.Job.ServiceMan))
                    {
                        OutService = new MenuItem("Quitter sont service", "");
                        OutService.ExecuteCallback = true;
                        menu.Add(OutService);
                    }
                    else
                    {
                        PService = new MenuItem("Prendre sont service", "");
                        PService.ExecuteCallback = true;
                        menu.Add(PService);
                    }
                    MenuManager.OpenMenu(ph.client, menu);
                }
            });
            gerantcol.onEntityExitColShape += ((shape, entity) =>
            {
                if (API.shared.getEntityType(entity) == EntityType.Player) {
                    Client client = API.shared.getPlayerFromHandle(entity);
                    MenuManager.CloseMenu(client);
                }
            });
            #endregion

            spawncol = API.shared.createCylinderColShape(spawn.pos.toVector3(), 3f, 3f);

            _lscustom.Add(this);
            businesses.Add(this);
        }

        private void LosSantosCustomGestionManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (menuItem == PService)
            {
                PlayerHandler ph = Players.getPlayerByClient(client);
                if (ph.job != null)
                    ph.client.sendNotificationError("Vous êtes déjà en service!");

                if (spawncol.getAllEntities().Count() >= 1)
                    ph.client.sendNotificationError("Un véhicule est sur le point d'apparition.");

                menu = new Menu("LosSantosCustom", "", "Quel véhicule voulez-vous sortir?", 0, 0, Menu.MenuAnchor.MiddleLeft, false, true, true);
                menu.BannerSprite = Banner.CarMod;
                menu.Callback = ((_client, _menu, _menuItem, _itemIndex, _forced, _data) => {
                    switch (_itemIndex)
                    {
                        case 0:
                            ph.job = new Player.Job.ServiceMan(ph, spawn, VehicleHash.TowTruck);
                            break;
                        case 1:
                            ph.job = new Player.Job.ServiceMan(ph, spawn, VehicleHash.TowTruck2);
                            break;
                        default:
                            ph.client.sendNotificationError("Inconnu");
                            break;
                    }
                });
                
                MenuItem item = new MenuItem("Dépanneuse n°1");
                item.ExecuteCallback = true;
                menu.Add(item);

                item = new MenuItem("Dépanneuse n°2");
                item.ExecuteCallback = true;
                menu.Add(item);
                MenuManager.OpenMenu(ph.client, menu);
            }
            else if (menuItem == OutService)
            {
                Players.getPlayerByClient(client).job.delete();
                client.sendNotificationSuccess("Vous avez quitter votre service");
            }
        }

        private void LosSantosCustomReparManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            if (!client.isInVehicle) return;
            PlayerHandler ph = Players.getPlayerByClient(client);
            Vehicle veh = client.vehicle;
            VehicleHandler VH = Vehicles.getVehicleByVehicle(veh);

            if (menuItem == vehicleStatut)
                client.triggerEvent("checkvehicle", "Informations: " + veh.displayName, Players.getPlayerBySCN(VH.pid).name, VH.plate, veh.health / 10, veh.engineHealth / 10, veh.oilLevel);
            else if (menuItem == reparcar)
            {
                if (ph.money >= bodyReparPrice)
                {
                    ph.money =- bodyReparPrice;
                    cashBox =+ bodyReparPrice;
                    float vehoil = veh.oilLevel;
                    float vehengine = veh.engineHealth;
                    
                    veh.health = 100;
                    veh.oilLevel = vehoil;
                    veh.engineHealth = vehengine;
                    client.sendNotificationSuccess("Vous avez réparer la carrosserie du véhicule: " + veh.numberPlate);
                }
                else
                    client.sendNotificationError("Vous n'avez pas assez d'argent sur vous pour réparer la carrosserie du véhicule: " + veh.numberPlate);
            }
        }
    }
}
