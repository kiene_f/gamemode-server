﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Businesses
{
    class Barber : Businesses
    {
        [JsonIgnore]
        private static List<Barber> _barbers = new List<Barber>();

        public Tuple<PedHash, Location> npc { get; set; }
        public Tuple<int, Vector3> door;
        public int hairprice = 0;
        public int beardprice = 0;

        public Barber(){}

        public Barber(Tuple<PedHash, Location> npc, Tuple<int, Vector3> door, string owner = null) : base(owner, "barber")
        {
            this.npc = npc;
            this.door = door;
            load();
        }

        public void load()
        {
            this.canEmploy = true;
            int door = API.shared.exported.doormanager.registerDoor(this.door.Item1, this.door.Item2);
            API.shared.exported.doormanager.setDoorState(door, false, 0);

            NPC vendor = new NPC(npc.Item1, "", npc.Item2.pos.toVector3(), (int)npc.Item2.rot.z, 0);
            vendor.setData("Interaction", "Barber");

            blip = API.shared.createBlip(npc.Item2.pos.toVector3());
            API.shared.setBlipSprite(blip, 71);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, "Coiffeur");
            if (owner == null)
                API.shared.setBlipColor(blip, color);

            _barbers.Add(this);
            businesses.Add(this);
        }

        public static Barber getBarberByPed(NPC ped) => _barbers.Find(x => x.npc.Item2.pos.toVector3() == ped.position);
    }
}
