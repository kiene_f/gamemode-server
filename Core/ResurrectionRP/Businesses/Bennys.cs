﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using MenuManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionRP.Businesses
{
    class Bennys : Businesses
    {
        [JsonIgnore]
        private static List<Bennys> _bennys = new List<Bennys>();

        public static CylinderColShape colshape;
        public Tuple<PedHash, Location> npc { get; set; }
        public Tuple<int, Vector3> door;
        public Location positionZone;

        public Bennys(Tuple<PedHash, Location> npc, Location positionZone, Tuple<int, Vector3> door, string owner = null) : base(owner, type: "bennys")
        {
            this.npc = npc;
            this.door = door;
            this.positionZone = positionZone;
            //load();
        }

        public void load()
        {
            int door = API.shared.exported.doormanager.registerDoor(this.door.Item1, this.door.Item2);
            API.shared.exported.doormanager.setDoorState(door, false, 0);

            NPC npcvendor = new NPC(this.npc.Item1, "", this.npc.Item2.pos.toVector3(), this.npc.Item2.rot.z, 0);
            npcvendor.setData("Interaction", "Bennys");

            colshape = API.shared.createCylinderColShape(positionZone.pos.toVector3(), 3f, 3f);
            API.shared.createMarker(1, colshape.Center - new Vector3(0.0f, 0.0f, 1f), new Vector3(), new Vector3(), new Vector3(4, 4, 0.5), 100, 255, 255, 255);

            Blip blip = API.shared.createBlip(colshape.Center);
            blip.sprite = 488;
            blip.shortRange = true;
            blip.color = 4;
            blip.name = "Benny's Custom";

            _bennys.Add(this);
            businesses.Add(this);
            Console.WriteLine("Benny's loaded");
        }

        public static Bennys getBennysByPed(NPC ped) => _bennys.Find(x => x.npc.Item2.pos.toVector3() == ped.position);

    }
}
